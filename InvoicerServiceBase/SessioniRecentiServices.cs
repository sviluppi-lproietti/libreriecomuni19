﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.LibrerieComuni.InvoicerDomainBase.Entity;

namespace ISC.LibrerieComuni.InvoicerServiceBase
{
    public static class SessioniRecentiServices
    {
        public static string FileDatiFullPath
        {
            get { return FolderDati + @"SessioniRecenti.xml"; }
        }

        public static string FolderDati { get; set; }

        public static Dictionary<int, SessioniRecentiEntity> GetAllSessioniRecenti()
        {
            Dictionary<int, SessioniRecentiEntity> dict;
            Boolean lRemoved;
            int i;

            dict = new Dictionary<int, SessioniRecentiEntity>();
            lRemoved = false;
            foreach (SessioniRecentiEntity ser in LoadSessioniRecenti().Values)
            {
                if (System.IO.Directory.Exists(ser.SER_percorso))
                    dict.Add(ser.SER_codice, ser);
                else
                    lRemoved = true;
            }

            if (lRemoved)
            {
                i = 1;
                foreach (SessioniRecentiEntity ser in dict.Values)
                {
                    ser.SER_codice = i;
                    i++;
                }
                SaveSessioniRecenti(dict);
            }
            return dict;
        }

        public static SessioniRecentiEntity GetBySessRecentId(int nKeyValue)
        {
            return LoadSessioniRecenti()[nKeyValue];
        }

        public static void SetSessioniRecenti(string cNomeSessione, string cSessioneFolder, int nValue2Remove)
        {
            Dictionary<int, SessioniRecentiEntity> dictA;
            SessioniRecentiEntity serA;
            int i;

            dictA = LoadSessioniRecenti();

            //
            // Rimuovi la sessione indicata
            //
            if (dictA.ContainsKey(nValue2Remove))
                dictA.Remove(nValue2Remove);
            serA = new SessioniRecentiEntity(0, cNomeSessione, cSessioneFolder);

            //
            // Ricerco se esiste un record con lo stesso percorso all'interno del dizionario e lo rimuovo
            //
            dictA = dictA.Where(kvp => kvp.Value.SER_percorso.ToLower() != cSessioneFolder.ToLower()).ToDictionary(k => k.Value.SER_codice, v => v.Value);
            dictA.Add(serA.SER_codice, serA);
            dictA = dictA.OrderBy(kvp => kvp.Key).ToDictionary(k => k.Value.SER_codice, v => v.Value);

            //
            // Rinumero le sessioni 
            //
            i = 0;
            foreach (SessioniRecentiEntity serB in dictA.Values)
            {
                i++;
                serB.SER_codice = i;
            }
            dictA = dictA.ToDictionary(k => k.Value.SER_codice, v => v.Value);
            while (dictA.Count > 10)
            {
                dictA.Remove(i);
                i--;
            }
            SaveSessioniRecenti(dictA);
        }

        public static void RimuoviSessioneRecente(int nKeyValue)
        {
            Dictionary<int, SessioniRecentiEntity> dictA;
            int i;

            dictA = LoadSessioniRecenti();
            dictA.Remove(nKeyValue);
            i = 0;
            foreach (SessioniRecentiEntity serB in dictA.Values)
            {
                i++;
                serB.SER_codice = i;
            }
            dictA = dictA.ToDictionary(k => k.Value.SER_codice, v => v.Value);
            while (dictA.Count > 10)
            {
                dictA.Remove(i);
                i--;
            }
            SaveSessioniRecenti(dictA);
        }

        public static void ClearSessioniRecenti()
        {
            SaveSessioniRecenti(new Dictionary<int, SessioniRecentiEntity>());
        }

        private static Dictionary<int, SessioniRecentiEntity> LoadSessioniRecenti()
        {
            Dictionary<int, SessioniRecentiEntity> dict;

            var reader = new System.Xml.Serialization.XmlSerializer(typeof(List<SessioniRecentiEntity>));
            System.IO.StreamReader sr = new System.IO.StreamReader(FileDatiFullPath);
            dict = new Dictionary<int, SessioniRecentiEntity>();
            foreach (SessioniRecentiEntity ser in (List<SessioniRecentiEntity>)reader.Deserialize(sr))
            {
                dict.Add(ser.SER_codice, ser);
            }
            sr.Close();
            return dict;
        }

        private static void SaveSessioniRecenti(Dictionary<int, SessioniRecentiEntity> dictSeR)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(FileDatiFullPath);
            System.IO.StreamWriter sw = file.CreateText();
            var writer = new System.Xml.Serialization.XmlSerializer(typeof(List<SessioniRecentiEntity>));
            writer.Serialize(sw, dictSeR.Values.ToList());
            sw.Close();
        }

    }
}
