﻿'
' In questa classe vengono dichiarati tutti gli enumeratori utilizzati in questo progetto
'

Namespace Modulo

    Public Enum eCodiceModuloFix
        MOD_SendEmail = 999
        MOD_ArchOtt = 998
    End Enum


    Public Enum eTipoBarcode
        BC_2of5 = 1
        BC_128c = 2
        BC_DataMa = 3
        BC_GS1_128 = 4
        BC_QRCode = 5
    End Enum

    Public Enum eTipoAllineamento
        NonGestito = -1
        Sinistra = 1
        Centro = 2
        Destra = 3
        Giustificato = 4
        F24 = 5
        Centro2 = 6
    End Enum

    Public Enum eDirezione
        Orizzontale = 1
        Verticale = 2
        Verticale_BassoAlto = 3
    End Enum

End Namespace