Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_barcode
        Inherits CLS_elemento_base

        Private cFilterRow As String
        Private _ValoreS As String
        Private _ValoreA As ArrayList
        Public Property BarCodeValue As ArrayList

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Private ReadOnly Property StampaInTesto As Boolean

            Get
                Return RawPrintItem.EndsWith("_tx")
            End Get

        End Property

        Public Overrides Sub Getvalue()

        End Sub

        Public ReadOnly Property AltezzaBarCode() As Integer

            Get
                Return GetValueFromXML(XMLItem, "Altezza")
            End Get

        End Property

        Public ReadOnly Property TipoBarcode() As eTipoBarcode

            Get
                Select Case GetValueFromXML(XMLItem, "PrintItemType", "text").ToLower
                    Case Is = "bc_2of5", "bc_2of5_codimb", "bc_2of5_tx"
                        Return eTipoBarcode.BC_2of5
                    Case Is = "bc_128c_bc", "bc_128c_tx"
                        Return eTipoBarcode.BC_128c
                    Case Is = "bc_datamatrix_bc", "bc_datamatrix_tx"
                        Return eTipoBarcode.BC_DataMa
                    Case Is = "bc_gs1-128_bc", "bc_gs1-128_tx"
                        Return eTipoBarcode.BC_GS1_128
                    Case Is = "bc_qrcode_bc"
                        Return eTipoBarcode.BC_QRCode
                    Case Else
                        Throw New Exception("PI-0002: Indicazione del tipo di barcode non corretta")
                End Select
            End Get

        End Property

        Public ReadOnly Property Direzione() As eDirezione

            Get
                Select Case GetValueFromXML(XMLItem, "Direzione", "o").ToLower
                    Case Is = "o"
                        Return eDirezione.Orizzontale
                    Case Is = "v", "v-ba"
                        Return eDirezione.Verticale
                    Case Else
                        Throw New Exception("PI-0001: Indicazione della direzione di stampa non corretta")
                End Select
            End Get

        End Property

        Public ReadOnly Property Valori_codice_DataMatrix As ArrayList

            Get
                Dim aValue As ArrayList

                aValue = New ArrayList
                If Me.XMLItem.SelectNodes("values/valuesItem/value/BC_value").Count > 0 Then
                    For Each xnItem_2 As Xml.XmlNode In Me.XMLItem.SelectNodes("values/valuesItem/value/BC_value")
                        aValue.Add(xnItem_2.InnerText)
                    Next
                ElseIf Me.XMLItem.SelectNodes("value/BC_value").Count > 0 Then
                    For Each xnItem_2 As Xml.XmlNode In Me.XMLItem.SelectNodes("value/BC_value")
                        aValue.Add(xnItem_2.InnerText)
                    Next
                Else
                    Throw New Exception("Errore recuperando i valori del barcode")
                End If
                Return aValue
            End Get

        End Property

        Public ReadOnly Property SpessoreBarCode() As Decimal

            Get
                Return CDec(GetValueFromXML(XMLItem, "Larghezza", "0,2"))
            End Get

        End Property

        Public ReadOnly Property Datamatrix_Tipo() As String

            Get
                Return GetValueFromXML(XMLItem, "tipologia", "16x48")
            End Get

        End Property

        Public ReadOnly Property Datamatrix_DimensioniModulo() As Integer

            Get
                Return GetValueFromXML(XMLItem, "dimensione", 2)
            End Get

        End Property

        Public ReadOnly Property Datamatrix_DimensioniMargini() As Integer

            Get
                Return GetValueFromXML(XMLItem, "dimensioneMargini", 1)
            End Get

        End Property

        Public ReadOnly Property LarghezzaRiga As Decimal

            Get
                Return GetValueFromXML(XMLItem, "dimensioni/larghezza", 1)
            End Get

        End Property

        Public Overrides Property ValoreS As String

            Get
                Return _ValoreS
            End Get
            Set(ByVal value As String)
                _ValoreS = value
                _BarCodeValue = New ArrayList
                If (value.Length Mod 2) = 1 Then
                    _BarCodeValue.Add("0")
                End If
                For i As Integer = 0 To value.Length - 1
                    _BarCodeValue.Add(value.Chars(i).ToString)
                Next
            End Set

        End Property

        Public Overrides WriteOnly Property ValoreA() As ArrayList

            Set(ByVal value As ArrayList)
                _BarCodeValue = value
            End Set

        End Property

        Public ReadOnly Property ECCLevel As String

            Get
                Return GetValueFromXML(XMLItem, "ECCLevel", "L")
            End Get

        End Property

        Public ReadOnly Property NeedRotation As Boolean

            Get
                Return NodeNotNull(Me.RotationNode)
            End Get

        End Property

        Public ReadOnly Property RotationType As System.Drawing.RotateFlipType

            Get
                Return RotationNode.InnerText
            End Get

        End Property

        Private ReadOnly Property RotationNode As Xml.XmlNode

            Get
                Return XMLItem.SelectSingleNode("RotationType")
            End Get

        End Property

    End Class

End Namespace