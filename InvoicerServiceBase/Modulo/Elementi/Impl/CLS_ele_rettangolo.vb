Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_rettangolo
        Inherits CLS_elemento_base

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property BordiRotondi As Boolean

            Get
                Return RawPrintItem = "rectangler"
            End Get

        End Property

        Public ReadOnly Property BordiSquadrati As Boolean

            Get
                Return RawPrintItem = "rectangle"
            End Get

        End Property

        Public ReadOnly Property RettangoloPieno As Boolean

            Get
                Return RawPrintItem = "rectanglef"
            End Get

        End Property

        Public ReadOnly Property RettangoloSoloBordi As Boolean

            Get
                Return RawPrintItem <> "rectanglef"
            End Get

        End Property

        Public ReadOnly Property Larghezza As Decimal

            Get
                Return GetValueFromXML(XMLItem, "Dimensioni/X")
            End Get

        End Property

        Public ReadOnly Property Altezza As Decimal

            Get
                Return GetValueFromXML(XMLItem, "Dimensioni/Y")
            End Get

        End Property

        Public ReadOnly Property DimensionePenna As Decimal

            Get
                Return GetValueFromXML(XMLItem, "penna/dimensione", "0,5")
            End Get

        End Property

        Public ReadOnly Property ColorePenna As System.Drawing.Color

            Get
                Dim cColor As String
                Dim nRed As Integer
                Dim nGre As Integer
                Dim nBlu As Integer

                If XMLItem.SelectSingleNode("penna/colore") Is Nothing Then
                    cColor = GetValueFromXML(XMLItem, "Color", "black")
                Else
                    cColor = GetValueFromXML(XMLItem, "penna/colore", "black")
                End If
                If (cColor.Contains(",")) Then
                    nRed = Integer.Parse(cColor.Split(",")(0))
                    nGre = Integer.Parse(cColor.Split(",")(1))
                    nBlu = Integer.Parse(cColor.Split(",")(2))
                    Return System.Drawing.Color.FromArgb(nRed, nGre, nBlu)
                Else
                    Return System.Drawing.Color.FromName(cColor)
                End If
            End Get

        End Property

    End Class

End Namespace