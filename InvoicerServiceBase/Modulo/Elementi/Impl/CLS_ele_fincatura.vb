Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_fincatura
        Inherits CLS_elemento_base

        Public Property ImageFLD As String

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

            Me.Posizione("X") = 0
            Me.Posizione("Y") = 0

        End Sub

        '
        ' Indica se la fincatura deve essere stampata ogni volta che il modulo viene prodotto indipendentemente dal
        ' numero di pagina e dal fronte retro.
        '
        Public ReadOnly Property StampaSempre As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "PrintAlways", 0) = 1
            End Get

        End Property

        Public ReadOnly Property FincaturaFile As String

            Get
                Return ConcatenaFolderFileValue(ImageFLD, Me.ValoreS)
            End Get

        End Property

    End Class

End Namespace