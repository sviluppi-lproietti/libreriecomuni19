﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Drawing;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.LiberieComuni.InvoicerService.Modulo.Elementi;
using ISC.LibrerieComuni.InvoicerService.Modulo.Elementi.Base;

namespace ISC.InvoicerService.Modulo.Elementi.Impl
{
    public class CLS_ele_linea : CLS_elemento_base
    {
        private XmlNode _BoxItem;
        private int _Lunghezza;
        private int _Altezza;

        public CLS_ele_linea(CLS_elemento_base eb, CLS_posizioni posi, Brush upColor, Font upFont, Dictionary<String, Decimal> upPos)
            : base(eb.XMLItem, posi, upColor, upFont, upPos)
        {
            _Lunghezza = 0;
            _Altezza = 0;
            if (XMLItem.SelectSingleNode("Dimensioni") == null)
            {
                if (XMLItem.SelectSingleNode("lunghezza") != null)
                    _Lunghezza = int.Parse(XMLItem.SelectSingleNode("lunghezza").InnerText);
                if (XMLItem.SelectSingleNode("altezza") != null)
                    _Altezza = int.Parse(XMLItem.SelectSingleNode("altezza").InnerText);
            }
            else
            {
                if (XMLItem.SelectSingleNode("Dimensioni/X") != null)
                    _Lunghezza = int.Parse(XMLItem.SelectSingleNode("Dimensioni/X").InnerText);
                if (XMLItem.SelectSingleNode("Dimensioni/Y") != null)
                    _Altezza = int.Parse(XMLItem.SelectSingleNode("Dimensioni/Y").InnerText);
            }
        }

        public int Lunghezza
        {
            get
            {
                return _Lunghezza;
            }

        }

        public int Altezza
        {
            get
            {
                return _Altezza;
            }
        }


        public Decimal DimensionePenna
        {
            get
            {
                return decimal.Parse(OGC_utilitaXML.GetValueFromXML(XMLItem, "penna/dimensione"));
            }
        }

        public Color ColorePenna
        {
            get
            {
                String cColor;
                int nRed;
                int nGre;
                int nBlu;

                if (XMLItem.SelectSingleNode("penna/colore") == null)
                    cColor = OGC_utilitaXML.GetValueFromXML(XMLItem, "Color", "black");
                else
                    cColor = OGC_utilitaXML.GetValueFromXML(XMLItem, "penna/colore", "black");

                if (cColor.Contains(","))
                {
                    nRed = int.Parse(cColor.Split(',')[0]);
                    nGre = int.Parse(cColor.Split(',')[1]);
                    nBlu = int.Parse(cColor.Split(',')[2]);
                    return Color.FromArgb(nRed, nGre, nBlu);
                }
                else
                    return Color.FromName(cColor);
            }
        }
    }
}
