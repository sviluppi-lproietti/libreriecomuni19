﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Drawing;
using ISC.LibrerieComuni.InvoicerService.Modulo.Elementi;
using ISC.LibrerieComuni.InvoicerService.Modulo.Elementi.Base;

namespace ISC.LibrerieComuni.InvoicerService.Modulo.Elementi.Impl
{
    public class CLS_ele_defaults : CLS_elemento_base
    {
        public CLS_ele_defaults(CLS_elemento_base eb, CLS_posizioni posi, Brush upColor, Font upFont, Dictionary<String, Decimal> upPos)
            : base(eb.XMLItem, posi, upColor, upFont, upPos)
        {

        }

        public CLS_ele_defaults(XmlNode xi) :
            base(xi)
        {

        }
    }
}
