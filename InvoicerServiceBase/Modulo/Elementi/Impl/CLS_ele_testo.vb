Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_testo
        Inherits CLS_elemento_base

        Public Sub New(ByVal eb As CLS_elemento_base)
            MyBase.New(eb.XMLItem)

        End Sub

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property Direzione() As eDirezione

            Get
                Select Case GetValueFromXML(XMLItem, "Direzione", "o").ToLower
                    Case Is = "o"
                        Return eDirezione.Orizzontale
                    Case Is = "v"
                        Return eDirezione.Verticale
                    Case Is = "v-ba"
                        Return eDirezione.Verticale_BassoAlto
                    Case Else
                        Throw New Exception("PI-0001: Indicazione della direzione di stampa non corretta")
                End Select
            End Get

        End Property

        '
        ' Contiene l'allineamento del testoda stampare
        '
        Public ReadOnly Property Allineamento As eTipoAllineamento

            Get
                Dim cTmp As String

                cTmp = GetValueFromXML(XMLItem, "align", "l").ToLower
                If (cTmp = "l") Then
                    Return eTipoAllineamento.Sinistra
                ElseIf (cTmp = "c") Then
                    Return eTipoAllineamento.Centro
                ElseIf (cTmp = "cc") Then
                    Return eTipoAllineamento.Centro2
                ElseIf (cTmp = "r") Then
                    Return eTipoAllineamento.Destra
                ElseIf (cTmp = "g") Or (cTmp = "gg") Or (cTmp = "j") Or (cTmp = "jj") Then
                    Return eTipoAllineamento.Giustificato
                ElseIf (cTmp = "f24") Then
                    Return eTipoAllineamento.F24
                Else
                    Return eTipoAllineamento.NonGestito
                End If
            End Get

        End Property

        Public ReadOnly Property LarghezzaRiga As Decimal

            Get
                Return GetValueFromXML(XMLItem, "dimensioni/larghezza", 1)
            End Get

        End Property

        Public ReadOnly Property BackGroundColor As System.Drawing.Color

            Get
                Dim cColor As String
                Dim nRed As Integer
                Dim nGre As Integer
                Dim nBlu As Integer

                cColor = GetValueFromXML(XMLItem, "BackColor", "black")
                If (cColor.Contains(",")) Then
                    nRed = Integer.Parse(cColor.Split(",")(0))
                    nGre = Integer.Parse(cColor.Split(",")(1))
                    nBlu = Integer.Parse(cColor.Split(",")(2))
                    Return System.Drawing.Color.FromArgb(nRed, nGre, nBlu)
                Else
                    Return System.Drawing.Color.FromName(cColor)
                End If
            End Get

        End Property

    End Class

End Namespace