Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_pagina_prestampato
        Inherits CLS_elemento_base

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New(xi)

            '
            ' Rimuove i commenti dalla pagina. I Commenti non sono necesari.
            '
            PulisciDaiCommenti(Me.XMLItem)
            Me.IsPrestampato = False

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode, ByVal lClean As Boolean)
            MyBase.New(xi)

            If lClean Then
                '
                ' Rimuove i commenti dalla pagina. I Commenti non sono necesari.
                '
                PulisciDaiCommenti(Me.XMLItem)
            End If

        End Sub

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property PosizioneBaseXML As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("PosizioneBase").SelectSingleNode("Posizione").Clone
            End Get

        End Property

        ''' <summary>
        ''' Rimuove i commenti dalla pagina che sista stampando.
        ''' </summary>
        ''' <param name="xnItem">Contiene l'elemento dal quale rimuovere i commenti</param>
        ''' <remarks></remarks>
        Private Sub PulisciDaiCommenti(ByVal xnItem As Xml.XmlNode)
            'Dim i As Integer
            Dim allComments As Xml.XmlNodeList
            'i = 0
            'While i < xnItem.ChildNodes.Count - 1
            '    If xnItem.ChildNodes(i).NodeType = Xml.XmlNodeType.Comment Then
            '        xnItem.RemoveChild(xnItem.ChildNodes(i))
            '        i -= 1
            '    Else
            '        PulisciDaiCommenti(xnItem.ChildNodes(i))
            '    End If
            '    i += 1
            'End While
            allComments = xnItem.SelectNodes("//comment()")

            For Each n As Xml.XmlNode In allComments
                n.ParentNode.RemoveChild(n)
            Next

        End Sub

        ''' <summary>
        ''' Restituisce il cassetto dal quale prelevare la carta. Utile in fase di stampa del cartaceo.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Integer</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CassettoCarta As Integer

            Get
                Dim nTrayOfPage As Integer
                Dim cTrayOfPage As String

                cTrayOfPage = Me.XMLItem.SelectSingleNode("tray").InnerText.Replace("*", "")
                If IsNumeric(cTrayOfPage) Then
                    nTrayOfPage = cTrayOfPage
                Else
                    nTrayOfPage = cTrayOfPage
                End If
                Return nTrayOfPage
            End Get

        End Property

        ''' <summary>
        ''' Indica se la pagina in questione � generata come vuota o meno.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property PaginaVuota As Boolean

            Get
                Return GetAttrFromXML(Me.XMLItem, "PaginaVuota", 0) = 1
            End Get

        End Property

        ''' <summary>
        ''' Indica se la pagina in questione deve essere stampata obbligatoriamente su pagina Dispari.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ForzaSuPaginaDispari As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "tray", "0").Contains("*")
            End Get

        End Property

        Public ReadOnly Property RiportaElementiTuttePagine As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "NoAllPageItems", "0") = 0
            End Get

        End Property

        Public Sub ImpostaMargineBasso(ByVal nMargineBassoDef As Decimal)

            MargineBasso = GetValueFromXML(Me.XMLItem, "MaxPageLenght", nMargineBassoDef)

        End Sub

        ''' <summary>
        ''' Indca il valore del margine basso della pagina.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MargineBasso As Decimal

        Public Sub AddSwitchToDettaglio(ByVal nSwitch As Integer)
            Dim xnItem As Xml.XmlNode

            xnItem = Me.XMLItem.OwnerDocument.CreateNode(Xml.XmlNodeType.Element, "SwitchToDettaglio", "") 'Me.XMLItem.OwnerDocument.CreateNode("")
            xnItem.InnerText = nSwitch
            Me.XMLItem.InsertAfter(xnItem, Me.XMLItem.ChildNodes(0))

        End Sub

        Public ReadOnly Property SwitchStampaDettaglioExist As Boolean

            Get
                Return NodeExist(Me.XMLItem, "SwitchToDettaglio")

            End Get

        End Property

        Public ReadOnly Property SwitchStampaDettaglio As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "SwitchToDettaglio", 0) = 1

            End Get

        End Property

        Public Sub AddStampaSoloSePari()
            Dim xnConditions As Xml.XmlNode
            Dim xnCondition As Xml.XmlNode
            Dim xaWork As Xml.XmlAttribute

            xnCondition = Me.XMLItem.OwnerDocument.CreateNode(Xml.XmlNodeType.Element, "Condition_row", "")
            xnCondition.InnerText = ";;page;PARI;;;n;;"
            xnConditions = Me.XMLItem.OwnerDocument.CreateNode(Xml.XmlNodeType.Element, "Conditions", "") 'Me.XMLItem.OwnerDocument.CreateNode("")
            xnConditions.AppendChild(xnCondition)
            Me.XMLItem.InsertAfter(xnConditions, Me.XMLItem.ChildNodes(0))

            xaWork = Me.XMLItem.OwnerDocument.CreateAttribute("StampaSoloSePari")
            xaWork.Value = 1
            Me.XMLItem.Attributes.Append(xaWork)

        End Sub

        ReadOnly Property StampaSoloSePari As Boolean

            Get
                Return GetAttrFromXML(Me.XMLItem, "StampaSoloSePari", "0") = 1
            End Get

        End Property

        'Public Property PaginaPreStampato As Integer

        '    Get
        '        Return GetValueFromXML(Me.XMLItem, "PaginaPreStampato", "-1")
        '    End Get
        '    Set(ByVal value As Integer)
        '        Dim xnWork As Xml.XmlNode

        '        xnWork = Me.XMLItem.SelectSingleNode("PaginaPreStampato")
        '        If xnWork Is Nothing Then
        '            xnWork = Me.XMLItem.OwnerDocument.CreateElement("PaginaPreStampato")
        '            xnWork.InnerText = value
        '            Me.XMLItem.AppendChild(xnWork)
        '        Else
        '            xnWork.InnerText = value
        '        End If
        '    End Set

        'End Property

        Public Property IsPrestampato As Boolean

            Get
                Return GetAttrFromXML(Me.XMLItem, "IsPrestampato", "0") = 1
            End Get
            Set(ByVal value As Boolean)
                Dim nTmp As Integer

                If value Then
                    nTmp = 1
                Else
                    nTmp = 0

                End If
                SetAttrToXML(Me.XMLItem, "IsPrestampato", nTmp)
                Return
            End Set

        End Property

        Public Property PaginePreStampato As Integer

            Get
                Return GetValueFromXML(Me.XMLItem, "PaginePreStampato", "-1")
            End Get
            Set(ByVal value As Integer)
                Dim xnWork As Xml.XmlNode

                xnWork = Me.XMLItem.SelectSingleNode("PaginePreStampato")
                If xnWork Is Nothing Then
                    xnWork = Me.XMLItem.OwnerDocument.CreateElement("PaginePreStampato")
                    xnWork.InnerText = value
                    Me.XMLItem.AppendChild(xnWork)
                Else
                    xnWork.InnerText = value
                End If
            End Set

        End Property

    End Class

End Namespace