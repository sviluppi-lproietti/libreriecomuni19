﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.LiberieComuni.InvoicerService.Modulo.Elementi;

namespace ISC.LibrerieComuni.InvoicerService.Modulo.Elementi.Base
{

    public enum eTipoElemento
    {
        TEL_nonspecificato,
        TEL_sconosciuto,
        TEL_pagina,
        TEL_sezione,
        TEL_blocco,
        TEL_item,
        TEL_fincatura,
        TEL_corpo,
        TEL_modulo
    }

    public enum eSottoTipoItem
    {
        NonImpostato = -1,
        TipoNonGestito = 0,
        BarCode = 1,
        Fincatura = 2,
        Immagine = 3,
        Testo = 4,
        TestoInvertito = 5,
        Grafico1 = 6,
        Grafico2 = 7,
        Rettangolo = 8,
        Linea = 9,
        PreStampato = 10
    }

    public enum eTipoSpecialeItem
    {
        NonImpostato = -1,
        TipoNonGestito = 0,
        Titolo = 1
    }

    public class CLS_elemento_base
    {
        private Decimal _MaxPosizioneY;
        private Boolean _NeedValue;
        private String _RawPrintItem;
        private eSottoTipoItem _TipoPrintItem;

        public int Codice
        {

            get
            {
                return int.Parse(OGC_utilitaXML.GetValueFromXML(XMLItem, "CodiceItem", "-999"));
            }

        }

        public Color Colore { get; set; }

        public Brush ColoreBrush
        {
            get
            {
                return new SolidBrush(this.Colore);
            }
        }

        public XmlNode GetValueTAG
        {
            get
            {
                XmlNode xnWork;
                XmlNode xnTmp;

                if (this.XMLItem.SelectSingleNode("value") != null)
                {
                    xnTmp = this.XMLItem.SelectSingleNode("value").Clone();
                    this.XMLItem.RemoveChild(this.XMLItem.SelectSingleNode("value"));
                    xnWork = this.XMLItem.OwnerDocument.CreateElement("values");
                    xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp.OuterXml, "</valuesItem>");
                    this.XMLItem.AppendChild(xnWork);
                }
                return this.XMLItem;
            }
        }

        /// <summary>
        /// Indica se l'elemento in questione è un elemento di stampa: pagina, blocco, sezione 
        /// </summary>
        /// <param name="FixedType"></param>
        /// <value></value>
        /// <returns>True: elemento di stampa, False: Altro</returns>
        /// <remarks></remarks>
        public Boolean IsElementoStampa(eTipoElemento FixedType = eTipoElemento.TEL_nonspecificato)
        {
            Boolean lRtn;

            lRtn = (TipoElemento == eTipoElemento.TEL_blocco) || (TipoElemento == eTipoElemento.TEL_item) ||
                   (TipoElemento == eTipoElemento.TEL_sezione) || (TipoElemento == eTipoElemento.TEL_corpo);
            if (FixedType != eTipoElemento.TEL_nonspecificato)
                lRtn = (TipoElemento == FixedType);
            return lRtn;
        }

        public Decimal MaxPosizioneY
        {
            get
            {
                return _MaxPosizioneY;
            }
            set
            {
                if ((value == Decimal.MinValue) && (this.Posizione.Count > 0))
                    _MaxPosizioneY = Math.Max(_MaxPosizioneY, Posizione["Y"]);
                else
                    _MaxPosizioneY = Math.Max(_MaxPosizioneY, value);
            }
        }

        //
        // Indica che l'elemento necessita diun valore per la stampa.
        //
        public Boolean NeedValue
        {
            get
            {
                return _NeedValue;
            }

        }

        //
        // Nome dell'elemento.
        //
        public String Nome
        {
            get
            {
                return OGC_utilitaXML.GetValueFromXML(XMLItem, "name");
            }

        }

        //
        // Indica la posizione nella quale occorre stampare l'elemento.
        //
        public Dictionary<String, Decimal> Posizione { get; set; }

        public String RawPrintItem
        {
            get
            {
                if (_RawPrintItem == "")
                    _RawPrintItem = OGC_utilitaXML.GetValueFromXML(XMLItem, "PrintItemType", "text").ToLower();
                return _RawPrintItem;
            }
        }

        public eSottoTipoItem SottoTipoElemento
        {
            get
            {
                if (TipoElemento == eTipoElemento.TEL_item)
                {
                    if (RawPrintItem.StartsWith("bc_"))
                        return eSottoTipoItem.BarCode;
                    else if (RawPrintItem == "form")
                        return eSottoTipoItem.Fincatura;
                    else if (RawPrintItem == "image")
                        return eSottoTipoItem.Immagine;
                    else if (RawPrintItem == "text")
                        return eSottoTipoItem.Testo;
                    else if (RawPrintItem == "testoinvertito")
                        return eSottoTipoItem.TestoInvertito;
                    else if (RawPrintItem == "grafico1")
                        return eSottoTipoItem.Grafico1;
                    else if (RawPrintItem == "grafico2")
                        return eSottoTipoItem.Grafico2;
                    else if (RawPrintItem == "line")
                        return eSottoTipoItem.Linea;
                    else if (RawPrintItem == "prestampato")
                        return eSottoTipoItem.PreStampato;
                    else if (RawPrintItem.StartsWith("rectangle"))
                        return eSottoTipoItem.Rettangolo;
                    else
                        return eSottoTipoItem.TipoNonGestito;
                }
                else
                    return eSottoTipoItem.TipoNonGestito;
            }
        }

        public eTipoElemento TipoElemento
        {
            get
            {
                if ((XMLItem.Name.ToLower() == "modulo") || (XMLItem.Name.ToLower() == "#document"))
                    return eTipoElemento.TEL_modulo;
                else if (XMLItem.Name.ToLower() == "blocco")
                    return eTipoElemento.TEL_blocco;
                else if (XMLItem.Name.ToLower() == "corpo")
                    return eTipoElemento.TEL_corpo;
                else if (XMLItem.Name.ToLower() == "item")
                    return eTipoElemento.TEL_item;
                else if (XMLItem.Name.ToLower() == "intestazione")
                    return eTipoElemento.TEL_sezione;
                else if (XMLItem.Name.ToLower() == "page")
                    return eTipoElemento.TEL_pagina;
                else if (XMLItem.Name.ToLower() == "piediblocco")
                    return eTipoElemento.TEL_sezione;
                else if (XMLItem.Name.ToLower() == "sezione")
                    return eTipoElemento.TEL_sezione;
                else
                    return eTipoElemento.TEL_sconosciuto;
            }
        }

        public eTipoSpecialeItem TipoSpeciale
        {
            get
            {
                String cTipo;

                cTipo = "";
                if (XMLItem.SelectSingleNode("SpecialType") != null)
                    cTipo = XMLItem.SelectSingleNode("SpecialType").InnerText;
                if (cTipo.ToLower() == "titolo")
                    return eTipoSpecialeItem.Titolo;
                else
                    return eTipoSpecialeItem.NonImpostato;
            }
        }

        public Font TipoCarattere { get; set; }

        public XmlNode XMLItem { get; set; }

        public Boolean DaStampare
        {
            get
            {
                return (int.Parse(OGC_utilitaXML.GetAttrFromXML(this.XMLItem, "toprint", "1")) == 1) && (int.Parse(OGC_utilitaXML.GetAttrFromXML(this.XMLItem, "printonce", "0")) <= 1);
            }
        }

        public Boolean GiaStampato { get; set; }

        private XmlNode _PosizioneItmXML
        {
            get
            {
                return XMLItem.SelectSingleNode("Posizione");
            }
        }

        public CLS_posizioni _PosizioniFisse { get; set; }

        public Boolean _PosItemExist
        {
            get
            {
                return _PosizioneItmXML != null;
            }
        }

        //Public Sub New()

        //End Sub

        public CLS_elemento_base(XmlNode xi)
        {

            XMLItem = xi;
            Posizione = new Dictionary<String, Decimal>();
            _NeedValue = false;
        }

        public CLS_elemento_base(XmlNode xi, CLS_posizioni posi, Brush upColor, Font upFont, Dictionary<String, Decimal> uppos)
            : this(xi)
        {
            _PosizioniFisse = posi;
            _MaxPosizioneY = Decimal.MinValue;

            //
            // Alcuni valori hanno bisogno di recuperare il proprio valore dal DB o dall'elemento.
            //
            _NeedValue = (this.SottoTipoElemento == eSottoTipoItem.BarCode) || (this.SottoTipoElemento == eSottoTipoItem.Testo) || (this.SottoTipoElemento == eSottoTipoItem.TestoInvertito) || (this.SottoTipoElemento == eSottoTipoItem.Fincatura) || (this.SottoTipoElemento == eSottoTipoItem.PreStampato);

            //
            // Recupera la posizione dell'elemento
            //
            if (this.SottoTipoElemento != eSottoTipoItem.Fincatura)
            {
                if (this.TipoElemento != eTipoElemento.TEL_pagina)
                    GetPosizione(uppos);

                GetColor(this.XMLItem, upColor);
                GetFont(this.XMLItem, upFont);
            }

        }

        public CLS_elemento_base(CLS_elemento_base eb, CLS_posizioni posi, Brush upColor, Font upFont, Dictionary<String, Decimal> uppos)
            : this(eb.XMLItem)
        {
            CheckCondizioni = eb.CheckCondizioni;
            _PosizioniFisse = posi;
            _MaxPosizioneY = Decimal.MinValue;

            //
            // Alcuni valori hanno bisogno di recuperare il proprio valore dal DB o dall'elemento.
            //
            _NeedValue = (this.SottoTipoElemento == eSottoTipoItem.BarCode) || (this.SottoTipoElemento == eSottoTipoItem.Testo) || (this.SottoTipoElemento == eSottoTipoItem.TestoInvertito) || (this.SottoTipoElemento == eSottoTipoItem.Fincatura) || (this.SottoTipoElemento == eSottoTipoItem.PreStampato);

            //
            // Recupera la posizione dell'elemento
            //
            if (this.SottoTipoElemento != eSottoTipoItem.Fincatura)
            {
                if (this.TipoElemento == eTipoElemento.TEL_pagina)
                    GetPosizione(uppos);

                GetColor(this.XMLItem, upColor);
                GetFont(this.XMLItem, upFont);
            }
        }

        //Public Overridable Sub GetValue()

        //End Sub

        public void SettaMassimaPosizione(Decimal nValueX, Decimal nValueY)
        {
            if (this.Nome != "")
            {
                if (this._PosizioniFisse.NamedItem.ContainsKey(this.Nome))
                    _PosizioniFisse.NamedItem[this.Nome] = new Decimal[] { nValueX, nValueY };
                else
                    _PosizioniFisse.NamedItem.Add(this.Nome, new Decimal[] { nValueX, nValueY });
            }
        }

        //'
        //' Gestione della posizione dell'elemento
        //'
        public void GetPosizione(Dictionary<String, Decimal> upPos)
        {
            try
            {   //
                // Istanzia l'oggetto Posizione
                //
                Posizione.Add("X", 0);
                Posizione.Add("Y", 0);
                if ((upPos != null) & (upPos.Count > 0))
                {
                    Posizione["X"] = upPos["X"];
                    Posizione["Y"] = upPos["Y"];
                }
                if (_PosItemExist)
                {
                    GetPosizioneAsse("X");
                    GetPosizioneAsse("Y");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetPosizioneAsse(string cAsse)
        {
            Decimal[] nRelativePos;
            int nForceOffset;
            Decimal nTmp;

            try
            {
                if (!(_PosizioneItmXML.SelectSingleNode(cAsse) == null))
                {
                    if (decimal.TryParse((_PosizioneItmXML.SelectSingleNode(cAsse).InnerText), out nTmp))
                    {
                        Posizione[cAsse] = decimal.Parse(_PosizioneItmXML.SelectSingleNode(cAsse).InnerText);
                    }
                    else if ((cAsse == "X"))
                    {
                        Posizione[cAsse] = _PosizioniFisse.Fisse[_PosizioneItmXML.SelectSingleNode(cAsse).InnerText][0];
                    }
                    else
                    {
                        Posizione[cAsse] = _PosizioniFisse.Fisse[_PosizioneItmXML.SelectSingleNode(cAsse).InnerText][1];
                    }

                }
                else if (!(_PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse)) == null))
                {
                    if (!(_PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse, "/LinkedItem")) == null))
                    {
                        // nRelativePos = _PosizioniFisse.NamedItem(String.Concat("LI_", _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/LinkedItem")).InnerText))
                        if (_PosizioniFisse.NamedItem.ContainsKey(_PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse, "/LinkedItem")).InnerText))
                        {
                            nRelativePos = _PosizioniFisse.NamedItem[_PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse, "/LinkedItem")).InnerText];
                            nForceOffset = int.Parse(OGC_utilitaXML.GetValueFromXML(_PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse)), "ForceOffSet", "0"));
                            Posizione[cAsse] = nRelativePos[1];
                            if (((nRelativePos[2] == 1)
                                        || (nForceOffset == 1)))
                            {
                                Posizione[cAsse] = decimal.Parse((Posizione[cAsse] + _PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse, "/OffSet")).InnerText));
                            }

                        }

                    }
                    else
                    {
                        Posizione[cAsse] = decimal.Parse((Posizione[cAsse] + _PosizioneItmXML.SelectSingleNode(string.Concat("Relative", cAsse, "/OffSet")).InnerText));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nella funzione GetSubPos", ex);
            }
        }

        public void ChangePrintItemTo(eSottoTipoItem nuovoTipo)
        {
            _TipoPrintItem = nuovoTipo;
            if (_TipoPrintItem == eSottoTipoItem.Testo)
                _RawPrintItem = "text";
        }

        /// <summary>
        /// Indica il valore dell'indice di riutilizzo se è da riutilizzare
        /// </summary>
        ///<remarks>-1 significa che l'elemento non è da riutilizzare. 0 è il primo elemento della lista.</remarks>
        ///<value>Intero</value>
        ///<returns></returns>
        public int IndiceRiutilizzo
        {
            get
            {
                return int.Parse(OGC_utilitaXML.GetAttrFromXML(this.XMLItem, "IndiceRiutilizzo", "-1"));
            }
            set
            {
                OGC_utilitaXML.SetAttrToXML(this.XMLItem, "IndiceRiutilizzo", value.ToString());
            }
        }

        //Shadows Function GetXmlNodeFromXML(ByVal xnTmp As Xml.XmlNode, ByVal cPath As String) As Xml.XmlNode

        //    Return xnTmp.SelectSingleNode(cPath)

        //End Function

        public Boolean IsDaStampare()
        {
            Boolean lStampabile;

            lStampabile = false;
            if (XMLItem.GetType() == typeof(System.Xml.XmlElement))
                lStampabile = (int.Parse(OGC_utilitaXML.GetAttrFromXML(XMLItem, "toprint", "1")) == 1) && (int.Parse(OGC_utilitaXML.GetAttrFromXML(XMLItem, "printonce", "0")) <= 1);
            return lStampabile;
        }

        private void GetColor(XmlNode xmlItem, Brush _UpColor)
        {
            String cColor;
            int nRed;
            int nGre;
            int nBlu;

            if (xmlItem.SelectSingleNode("Color") != null)
                cColor = OGC_utilitaXML.GetValueFromXML(xmlItem, "Color", "black");
            else if (xmlItem.SelectSingleNode("ForeColor") != null)
                cColor = OGC_utilitaXML.GetValueFromXML(xmlItem, "ForeColor", "black");
            else
                cColor = "black";


            if (cColor.Contains(","))
            {
                nRed = int.Parse(cColor.Split(',')[0]);
                nGre = int.Parse(cColor.Split(',')[1]);
                nBlu = int.Parse(cColor.Split(',')[2]);
                this.Colore = System.Drawing.Color.FromArgb(nRed, nGre, nBlu);
            }
            else
                this.Colore = System.Drawing.Color.FromName(cColor);
        }

        private void GetFont(XmlNode xmlItem, Font _UpFont)
        {
            XmlNode xmlFont;
            String cFont;
            float nSize;
            FontStyle FntStyle = new FontStyle();
            Boolean lBold = false;
            Boolean lItalic = false;
            Boolean lStrikeout = false;
            Boolean lUnderline = false;

            if (xmlItem.SelectSingleNode("Font") == null)
                this.TipoCarattere = _UpFont;
            else
            {
                xmlFont = xmlItem.SelectSingleNode("Font");
                if (_UpFont != null)
                {
                    cFont = _UpFont.FontFamily.Name;
                    nSize = _UpFont.Size;
                    lBold = _UpFont.Bold;
                    lItalic = _UpFont.Italic;
                    lStrikeout = _UpFont.Strikeout;
                    lUnderline = _UpFont.Underline;
                }
                else
                {
                    cFont = "";
                    nSize = 0;
                }
                foreach (XmlNode xmlItemTMP in xmlFont.ChildNodes)
                {
                    if (xmlItemTMP.Name.ToLower() == "nome")
                        cFont = xmlItemTMP.InnerText;
                    else if (xmlItemTMP.Name.ToLower() == "size")
                        nSize = float.Parse(xmlItemTMP.InnerText);
                    else if (xmlItemTMP.Name.ToLower() == "bold")
                        lBold = int.Parse(xmlItemTMP.InnerText) == 1;
                    else if (xmlItemTMP.Name.ToLower() == "italic")
                        lItalic = int.Parse(xmlItemTMP.InnerText) == 1;
                    else if (xmlItemTMP.Name.ToLower() == "strikeout")
                        lStrikeout = int.Parse(xmlItemTMP.InnerText) == 1;
                    else if (xmlItemTMP.Name.ToLower() == "underline")
                        lUnderline = int.Parse(xmlItemTMP.InnerText) == 1;
                }

                //
                // Imposta lo stile del fnt a normale.
                //
                FntStyle = FontStyle.Regular;
                if (lBold)
                    FntStyle = FntStyle | FontStyle.Bold;
                if (lItalic)
                    FntStyle = FntStyle | FontStyle.Italic;
                if (lStrikeout)
                    FntStyle = FntStyle | FontStyle.Strikeout;
                if (lUnderline)
                    FntStyle = FntStyle | FontStyle.Underline;

                this.TipoCarattere = new Font(cFont, nSize, FntStyle);
            }

        }

        public virtual String ValoreS { get; set; }

        public virtual ArrayList ValoreA { get; set; }

        /// <summary>
        /// Indica se un elemento da stampare deve proseguire a stampare i dati dal punto dove era stato lasciato 
        /// </summary>
        /// <value></value>
        /// <returns>Boolean</returns>
        /// <remarks></remarks>
        public Boolean ContinuaDati
        {
            get
            {
                return int.Parse(OGC_utilitaXML.GetAttrFromXML(XMLItem, "continuaDati", "0")) == 1;
            }
        }

        public void RimuoviAttributoElemento(String cAttrName)
        {
            this.XMLItem.Attributes.RemoveNamedItem(cAttrName);
        }

        public void ImpostaAttributoElemento(String cAttrName, int cValue)
        {
            XMLItem.Attributes.Append(XMLItem.OwnerDocument.CreateAttribute(cAttrName));
            XMLItem.Attributes[cAttrName].Value = cValue.ToString();
        }

        public void SegnaDaRiusare(int nIndiceRiutilizzo)
        {
            this.ImpostaAttributoElemento("IndiceRiutilizzo", nIndiceRiutilizzo + 1);
        }

        delegate Boolean DelControllaCondizioni(XmlNode xnItem, String cRelNameMaster, String cRelNameSlave);
        public DelControllaCondizioni CheckCondizioni { get; set; }

        // <summary>
        // Indica se un elemento è stato inserito dinamicamente ad esempio: elementi per tutte le pagine o codice di imbustamento.
        // </summary>
        // <value></value>
        // <returns>Boolean</returns>
        // <remarks></remarks>
        public Boolean IsTemporaneo
        {
            get
            {
                return int.Parse(OGC_utilitaXML.GetAttrFromXML(XMLItem, "temporaneo", "0")) == 1;
            }
        }

        public Boolean JumpPrint
        {
            get
            {
                return int.Parse(OGC_utilitaXML.GetAttrFromXML(XMLItem, "JumpPrint", "0")) == 1;
            }
        }

        public int PaginaPreStampato
        {
            get
            {
                return int.Parse(OGC_utilitaXML.GetValueFromXML(this.XMLItem, "PaginaPreStampato", "-1"));
            }
            set
            {
                XmlNode xnWork;

                xnWork = this.XMLItem.SelectSingleNode("PaginaPreStampato");
                if (xnWork == null)
                {
                    xnWork = this.XMLItem.OwnerDocument.CreateElement("PaginaPreStampato");
                    xnWork.InnerText = value.ToString();
                    this.XMLItem.AppendChild(xnWork);
                }
                else
                    xnWork.InnerText = value.ToString();
            }

        }
    }
}
