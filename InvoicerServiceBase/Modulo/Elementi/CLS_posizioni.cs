﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LiberieComuni.InvoicerService.Modulo.Elementi
{
    public class CLS_posizioni
    {
        public Dictionary<String, Decimal[]> Fisse { get; set; }
        public Dictionary<String, Decimal[]> NamedItem { get; set; }

        public CLS_posizioni()
        {
            Fisse = new Dictionary<String, Decimal[]>();
            NamedItem = new Dictionary<String, Decimal[]>();

        }
    }
}
