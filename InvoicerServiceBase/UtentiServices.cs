﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.LibrerieComuni.InvoicerDomainBase.Entity;

namespace ISC.LibrerieComuni.InvoicerServiceBase
{
    public static class UtentiServices
    {
        public static string FileDatiFullPath
        {
            get { return FolderDati + @"DatiUtenti.xml"; }
        }

        public static string FolderDati { get; set; }

        public static UtentiEntity GetUserByUserName(string cUsername, string cPassword)
        {
            UtentiEntity ute = null;

            Dictionary<int, UtentiEntity> _dictUtenti = new Dictionary<int, UtentiEntity>();
            var reader = new System.Xml.Serialization.XmlSerializer(typeof(List<UtentiEntity>));
            System.IO.StreamReader sr = new System.IO.StreamReader(FileDatiFullPath);

            foreach (UtentiEntity u in (List<UtentiEntity>)reader.Deserialize(sr))
            {
                if ((u.UTE_username == cUsername) && (u.UTE_password == cPassword))
                    ute = u.Clone();
            }
            sr.Close();

            return ute;
        }

        public static Dictionary<int, UtentiEntity> LoadAllUsers()
        {
            Dictionary<int, UtentiEntity> _dictUtenti;
            var reader = new System.Xml.Serialization.XmlSerializer(typeof(List<UtentiEntity>));
            System.IO.StreamReader sr = new System.IO.StreamReader(FileDatiFullPath);

            _dictUtenti = new Dictionary<int, UtentiEntity>();
            foreach (UtentiEntity ute in (List<UtentiEntity>)reader.Deserialize(sr))
            {
                _dictUtenti.Add(ute.UTE_codice, ute);
            }
            sr.Close();
            return _dictUtenti;
        }

        public static void SaveUsers(Dictionary<int, UtentiEntity> dictUte)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(FileDatiFullPath);
            System.IO.StreamWriter sw = file.CreateText();
            //System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Doctor));
            var writer = new System.Xml.Serialization.XmlSerializer(typeof(List<UtentiEntity>));
            writer.Serialize(sw, dictUte.Values.ToList());
            sw.Close();
        }

        public static void AggiornaPassword(UtentiEntity utente)
        {
            Dictionary<int, UtentiEntity> _dictUtenti = new Dictionary<int, UtentiEntity>();
            var reader = new System.Xml.Serialization.XmlSerializer(typeof(List<UtentiEntity>));
            System.IO.StreamReader sr = new System.IO.StreamReader(FileDatiFullPath);

            foreach (UtentiEntity ute in (List<UtentiEntity>)reader.Deserialize(sr))
            {
                _dictUtenti.Add(ute.UTE_codice, ute);
            }
            sr.Close();

            _dictUtenti[utente.UTE_codice] = utente;
            SaveUsers(_dictUtenti);
        }

        public static void GeneraUsername(UtentiEntity ute)
        {
            ute.UTE_username = "";
            if (!String.IsNullOrEmpty(ute.UTE_nome))
                ute.UTE_username = ute.UTE_nome[0].ToString();
            if (!String.IsNullOrEmpty(ute.UTE_cognome))
                ute.UTE_username += ute.UTE_cognome.Replace(" ", "");
            if (ute.tiebreak != 0)
                ute.UTE_username += ute.tiebreak.ToString();
            ute.UTE_username = ute.UTE_username.ToLower();
        }
    }
}
