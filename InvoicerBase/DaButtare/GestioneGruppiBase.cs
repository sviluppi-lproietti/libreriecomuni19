﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using ISC.LibrerieComuni.InvoicerDomain.Impl;

namespace ISC.Invoicer40.Gruppi.Base
{
    public class GestioneGruppiBase
    {
        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_Globale { get; set; }
        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_Creati
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_creato).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_Caricati 
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_caricato).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_Terminati
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_fine).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public int MaxGruppiGestiti { get { return 10; } }

        private System.Object lockGestioneGruppi = new System.Object();


        public GestioneGruppiBase()
        {
            lisgrp_Globale = new Dictionary<decimal, StrutturaGruppoBase>();
        }
        
        public void AggiungiStrutturaAttivita(StrutturaGruppoBase sg)
        {
            lock (lockGestioneGruppi)
            {
                lisgrp_Globale.Add(sg.ChiaveStrutturaGruppo, sg);
                lisgrp_Globale = lisgrp_Globale.OrderBy(k => k.Key).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public void RimuoviStrutturaAttivita(decimal nkey)
        {
            lock (lockGestioneGruppi)
            {
                lisgrp_Globale.Remove(nkey);
            }
        }

    }
}
