﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using ISC.LibrerieComuni.InvoicerDomain.Impl;
using ISC.Invoicer40.Gruppi.Base;

namespace ISC.Invoicer40.Gruppi.Impl
{
    public class GestioneGruppiStampa : GestioneGruppiBase
    {
        public GestioneGruppiStampa()
            : base()
        {

        }

        public int RecordStampare { get; set; }

        public Boolean FastPrintEnabled { get; set; }

        public Boolean THRCaricaDatabaseRunning
        {
            get { return RecordStampare <= 20; }
        }

        public Boolean THRGeneraAnteprimaRunning
        {
            get { return THRCaricaDatabaseRunning || lisgrp_AttesaAnteprima.Count > 0 || lisgrp_GeneraAnteprima.Count > 0; }
        }

        public Boolean THRGeneraStampaRunning
        {
            get { return THRGeneraAnteprimaRunning || lisgrp_AttesaStampa.Count > 0 || lisgrp_GeneraStampa.Count > 0; }
        }

        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_AttesaAnteprima
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_attesa_anteprima).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_GeneraAnteprima
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_genera_anteprima).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_AttesaStampa
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_attesa_stampa).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, StrutturaGruppoBase> lisgrp_GeneraStampa
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.StatoGruppo == eStatoGruppo.STR_genera_stampa).ToDictionary(k => k.Key, v => v.Value);
            }
        }


        public StrutturaGruppoStampa saTHRGA
        {
            get
            {
                return (StrutturaGruppoStampa)lisgrp_GeneraAnteprima.First().Value;
            }
        }

        public StrutturaGruppoStampa saTHRGS
        {
            get
            {
                return (StrutturaGruppoStampa)lisgrp_GeneraStampa.First().Value;
            }
        }

        public Boolean Need_CaricaBaseDati
        {
            get
            {
                return base.lisgrp_Globale.Count < this.MaxGruppiGestiti;
            }
        }

        public void AvanzaRecordDaCaricare()
        {

        }

        public string CheckSumPlugIn { get; set; }
        public string ModuloCheckSum { get; set; }
        public string CheckSumDatiFissi { get; set; }

        public void Carica_FileDatiAnteprima(string cFileName)
        {
            _DatiAnteprimaFN = cFileName;
            try
            {
                _xdFastPrint = new System.Xml.XmlDocument();
                if (System.IO.File.Exists(_DatiAnteprimaFN))
                    _xdFastPrint.Load(_DatiAnteprimaFN);
            }
            catch (Exception ex)
            {
                _xdFastPrint = null;
                throw ex;
            }
            finally
            {
                if (!_xdFastPrint.HasChildNodes)
                {
                    _xdFastPrint.LoadXml("<fastprint></fastprint>");
                    FastPrintEnabled = false;
                }
            }
        }

        public Boolean Recupera_DatiAnteprima()
        {
            Boolean lRtn;
            ContatoriPagineEntity c;
            XmlNode xnGruppo;
            int nKey;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            lRtn = false;
            try
            {
                if (this.saTHRGA.DatiStampa.DBStampa.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(this.saTHRGA.DatiStampa.DBStampa.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", CheckSumPlugIn, "' and @modulo='", ModuloCheckSum, "' and @datifissi='", CheckSumDatiFissi, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo != null)
                {
                    foreach (DataRow dr in this.saTHRGA.DatiStampa.DBStampa.MainTable.Rows)
                    {
                        nKey = int.Parse(dr[this.saTHRGA.DatiStampa.DBStampa.MainLinkToQD].ToString());
                        xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", nKey, "']"));
                        if (xnDocumento != null)
                        {
                            c = this.saTHRGA.DatiStampa.ElencoPagineDocumenti[nKey];
                            if (!c.GiaEseguitaAnteprima)
                            {
                                foreach (string cValue in xnDocumento.SelectSingleNode("parzia").InnerText.Split(';'))
                                {
                                    c.PagineDocumento += int.Parse(cValue);
                                    c.FineStampaSubDocumento(int.Parse(cValue));
                                }
                                c.GiaEseguitaAnteprima = true;
                                this.saTHRGA.DatiStampa.ProgressivoPagine += c.PagineDocumento;
                                this.saTHRGA.DatiStampa.FineStampaDocumento(nKey);
                            }
                            lRtn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lRtn = false;
                throw ex;
            }
            return lRtn;
        }

        public void Salva_DatiAnteprima()
        {
            XmlNode xnGruppo;
            string cPagineparziali;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            try
            {
                if (this.saTHRGA.DatiStampa.DBStampa.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(this.saTHRGA.DatiStampa.DBStampa.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", CheckSumPlugIn, "' and @modulo='", ModuloCheckSum, "' and @datifissi='", CheckSumDatiFissi, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo == null)
                {
                    xnGruppo = _xdFastPrint.CreateNode(XmlNodeType.Element, "anteprima", "");
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("nomemodulo", ""));
                    xnGruppo.Attributes["nomemodulo"].Value = this.saTHRGA.NomeModulo;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("plugin", ""));
                    xnGruppo.Attributes["plugin"].Value = CheckSumPlugIn;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("modulo", ""));
                    xnGruppo.Attributes["modulo"].Value = ModuloCheckSum;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("datifissi", ""));
                    xnGruppo.Attributes["datifissi"].Value = CheckSumDatiFissi;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("dettaglio", ""));
                    xnGruppo.Attributes["dettaglio"].Value = lDettaglio.ToString();
                    _xdFastPrint.SelectSingleNode("fastprint").AppendChild(xnGruppo);
                }
                //
                // All'interno del gruppo ricerca la presenza del documento di cui devo salvare i dati di anteprima.
                //
                foreach (KeyValuePair<int, ContatoriPagineEntity> k in saTHRGA.DatiStampa.ElencoPagineDocumenti)
                {
                    xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", k.Key, "']"));
                    if (xnDocumento == null)
                    {
                        xnDocumento = _xdFastPrint.CreateNode(XmlNodeType.Element, "documento", "");
                        xnDocumento.Attributes.Append(_xdFastPrint.CreateAttribute("codice", ""));
                        xnDocumento.Attributes["codice"].Value = k.Key.ToString();
                        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "totali", ""));
                        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "parzia", ""));
                        xnGruppo.AppendChild(xnDocumento);
                    }

                    //
                    // Aggiorna i dati delle pagine.
                    //
                    xnDocumento.SelectSingleNode("totali").InnerText = k.Value.PagineDocumento.ToString();
                    cPagineparziali = "";
                    foreach (int nPagine in k.Value.PagineSubDocumenti)
                        cPagineparziali = String.Concat(cPagineparziali, nPagine, ";");
                    k.Value.GiaEseguitaAnteprima = true;
                    xnDocumento.SelectSingleNode("parzia").InnerText = cPagineparziali.Substring(0, cPagineparziali.Length - 1);
                }
                _xdFastPrint.Save(_DatiAnteprimaFN);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string _DatiAnteprimaFN { get; set; }
        private System.Xml.XmlDocument _xdFastPrint { get; set; }
    }
}
