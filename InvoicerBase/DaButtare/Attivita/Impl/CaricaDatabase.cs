﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.Invoicer40.Attivita.Base;
using ISC.Invoicer40.Attivita.Interfaccia;
using System.Data;
using log4net;

namespace ISC.Invoicer40.Attivita.Impl
{
    public class CaricaDatabase : AttivitaBase , IAttivita
    {
        protected log4net.ILog loggerTHRLoadDB = LogManager.GetLogger("THRLoadDBLog");

        // ********************************************************************* '
        // Caricamento dei dati che andranno stampati.                           '
        // ********************************************************************* '
        private void CaricaDatabase1()
        {
            DataRow dr_mai_qd;

            loggerTHRLoadDB.Debug("  > INIZIO - Caricamento database");
            try
            {
                loggerTHRLoadDB.Debug("      > Attivazione del modulo: " + _saTHRLD.NomeModulo + " (" + _saTHRLD.CodiceModulo.ToString() + ")");
                GSS.GoToRecordNumber(_saTHRLD.DatiGenerali.RecordDaStampare_Primo);
                loggerTHRLoadDB.Debug("      > Primo record da caricare.: " + _saTHRLD.DatiGenerali.RecordDaStampare_Primo.ToString());
                loggerTHRLoadDB.Debug("      > Numero record da caricare: " + this.DatiMaster.ElementiPerGruppo_Numero.ToString());
                //                this.RecordDaStampare_Primo = GSS.CurrentRecordNumber;
                GSS.Carica_FullDataset(int.Parse(this.DatiMaster.ElementiPerGruppo_Numero.ToString()));
                _saTHRLD.DatiGruppo.AssegnaDatabase(GSS.FullDataset);
                //_saTHRLD.DatiGruppo.DBStampa.DBDatiPlugIn = GSS.FullDataset;

                loggerTHRLoadDB.Debug("      > Numero Record caricati: " + _saTHRLD.RecordFullDatasetMainTable.ToString());
                if (!_saTHRLD.IsEmptyBaseDati)
                {
                    loggerTHRLoadDB.Debug("      > Verifico se i record che ho caricato sono da stampare");

                    //
                    // Se stiamo stampando i docuementi cartacei e non abbiamo impostato la forzatura della stampa cartacea allora procedo a cancellare i dati dei record interessati.
                    //
                    if (!this.Stampa_PerArchiviazioneOttica && !this.Forza_StampaCartacea && !_saTHRLD.DatiGruppo.GMS.ModuloSenzaSelezioneNecessaria)
                        foreach (DataRow dr_fud in _saTHRLD.MainTable.Rows)
                        {
                            // ********************************************************************* //
                            // Per alcuni plug-in il valore della colonna MAINTABLEKEY della tabella //
                            // QUICKDATASET è di tipo 'string' e pertanto va effettuata una select   //
                            // con tipologia stringa quindi con apici prima e dopo il valore ricerca //
                            // to.                                                                   //
                            // ********************************************************************* //
                            if (GSS.QuickDataTable.Columns[this.GSS.QuickDataset_MainTableKey.ToString()].DataType == typeof(System.String))
                                dr_mai_qd = GSS.QuickDataTable.Select(String.Concat(GSS.QuickDataset_MainTableKey, " = '", dr_fud[GSS.LinkField_QuickDS_FullDS], "'"))[0];
                            else
                                dr_mai_qd = GSS.QuickDataTable.Rows.Find(dr_fud[GSS.LinkField_QuickDS_FullDS]);   //' .Select(String.Concat(GSS.QuickDataset_MainTableKey, " = ", dr_fud.Item(GSS.LinkField_QuickDS_FullDS)))(0)

                            if (((Boolean)dr_mai_qd["DEF_onlyarchott"]) && ((Boolean)dr_mai_qd["DEF_sendbymail"]))
                            {
                                loggerTHRLoadDB.Debug("      > Record rimosso.: " + dr_fud[GSS.LinkField_QuickDS_FullDS].ToString());
                                dr_fud.Delete();
                            }
                            _saTHRLD.MainTable.AcceptChanges();
                        }
                }
                this.DatiMaster.RecordDaStampare_Ultimo = GSS.CurrentRecordNumber;
            }
            catch (Exception ex)
            {
                loggerTHRLoadDB.Error("      > ERRORE - Caricamento database", ex);
            }
            loggerTHRLoadDB.Debug("  > FINE - Caricamento database");
        }

        public void EseguiAttivita()
        {
            decimal loopModulo;
            string cItemToken;

            loggerTHRLoadDB.Info("INIZIO - Avvio del THREAD per il caricamento dei dati.");
            this.RecordStampare = 0;
            while (this.THRCaricaDatabaseRunning)
            {
                try
                {
                    if (this.Need_CaricaBaseDati)
                    {
                        loggerTHRLoadDB.Debug("  > Caricamento necessario. procedo");
                        loopModulo = 0;
                        cItemToken = DateTime.Now.ToString("yyyyMMddhhmmss");
                        foreach (CLS_GestioneModuloStampare m in ModuliDaStampare.Values)
                        {
                            loopModulo = loopModulo + 1;
                            //
                            // Attivazione modulo per il recupero dei parametri delle tabelle di servizio.
                            // Deve essere attivata prima.
                            // 
                            this.GSS.AttivaModulo(m.CodiceModulo);
                            _saTHRLD = new StrutturaGruppoStampa(this.GSS.FullDataset_MainTableName, this.GSS.Files["PlugInDatiFissiFIL"], GSS.LinkField_QuickDS_FullDS, m.StampaForzataDettaglio);
                            _saTHRLD.ItemToken = cItemToken;
                            _saTHRLD.Raccolta = ModuliDaStampare.Values.Count == 1 ? 9 : 0;
                            if (_saTHRLD.Raccolta == 0)
                            {
                                if (loopModulo == 1)
                                    _saTHRLD.Raccolta = 1;
                                else if (loopModulo == ModuliDaStampare.Values.Count)
                                    _saTHRLD.Raccolta = 3;
                                else
                                    _saTHRLD.Raccolta = 2;
                            }
                            _saTHRLD.DatiGenerali = (DatiStampaGenerale)this.DatiMaster.Clone();
                            _saTHRLD.DatiGruppo.GMS = m.Clone();

                            _saTHRLD.DatiGruppo.ArchiviazioneOtticaFNTipo = m.Modulo.NomeFileArchivioOtticoSelect.AOF_filename;
                            _saTHRLD.DatiGruppo.FusioneFNTipo = GSS.ModuloUnificato.NomeFileArchivioOtticoSelect.AOF_filename;
                            _saTHRLD.DatiGruppo.InserisciFincatura = m.InserisciFincatura;
                            //_saTHRLD.DatiGruppo.Folders = m.Folders;

                            _saTHRLD.DatiGruppo.DatiCore = _saTHRLD.DatiGenerali.DatiBaseStampa;
                            _saTHRLD.DatiGruppo.FronteRetro = _saTHRLD.DatiGenerali.FronteRetro;
                            //_saTHRLD.DatiGruppo.Stampa_PerArchiviazioneOttica = _saTHRLD.DatiGenerali.Stampa_PerArchiviazioneOttica;
                            //_saTHRLD.DatiGruppo.ListaCassettiUtilizzati = m.DatiPerStampa.ListaCassettiUtilizzati;
                            //_saTHRLD.DatiStampa.SuffissoParte = m.SuffissoParte;
                            loggerTHRLoadDB.Info("  > Caricamento per il record: " + _saTHRLD.DatiGenerali.RecordDaStampare_Primo.ToString() + " Modulo: " + _saTHRLD.CodiceModulo.ToString());
                            CaricaDatabase1();
                            lock (lockAttivita)
                            {
                                if (_saTHRLD.IsEmptyBaseDati)
                                {
                                    loggerTHRLoadDB.Debug("  > Il repository è vuoto lo sposto in stato PreAttesaFusione o lo metto in stato terminato.");
                                    if (this.RichiestaFusioneModuli)
                                        _saTHRLD.StatoGruppo = eStatoGruppo.STR_pre_attesa_fusione;
                                    else
                                        _saTHRLD.StatoGruppo = eStatoGruppo.STR_fine;
                                }
                                else
                                {
                                    loggerTHRLoadDB.Debug("  > Il repository è pieno lo aggiungo a quelli da stampare.");
                                    _saTHRLD.StatoGruppo = eStatoGruppo.STR_attesa_anteprima;
                                }
                                AggiungiStrutturaAttivita(_saTHRLD);
                                loggerTHRLoadDB.Debug("  > Aggiunta effettuata con successo.");
                            }
                        }
                        this.DatiMaster.RecordDaStampare_Primo = this.DatiMaster.RecordDaStampare_Ultimo;
                        this.RecordStampare += 1;
                    }
                    else
                    {
                        loggerTHRLoadDB.Debug("  > Caricamento non necessario. Attendo.");
                        Thread.Sleep(1000);
                    }
                }
                catch (Exception ex)
                {
                    loggerTHRLoadDB.Error("ERRORE - THRCaricaDatabase - " + ex.Message, ex);
                }
            }
            loggerTHRLoadDB.Info("FINE - Termine del THREAD per il caricamento dei dati.");
        }
    }
}
