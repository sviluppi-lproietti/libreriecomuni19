﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.LibrerieComuni.InvoicerDomain.Entity;
//using ISC.LibrerieComuni.ManageAccessoDati;
//using ISC.LibrerieComuni.ManageSessione.Modulo;
using ISC.LibrerieComuni.ManageAccessoDati;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.Invoicer40.Dati.Base;
using ISC.Invoicer40.Varie;

namespace ISC.Invoicer40.Dati.Impl
{

    public class DatiStampaDettaglio : DatiBase
    {
        public Boolean Abilitata_FastPrint { get; set; }
        public Boolean Forza_StampaCartacea { get; set; }
        //public Boolean Fusione_Moduli { get; set; }
        //public Boolean Pulisci_FolderElaborati { get; set; }
        public Boolean Stampa_PerArchiviazioneOttica { get; set; }
        public int DocumentiDaStampare_Numero { get; set; }

        public CLS_GestioneModuloStampare Modulo { get; set; }
        //
        // L'impostazione dell'anteprima di stampa condiziona anche i comportamenti dei contatori delle pagine.
        //
        public Boolean Anteprima
        {
            get
            {
                return _Anteprima;
            }
            set
            {
                _Anteprima = value;
                ////               if (this.ElencoPagineDocumenti != null)
                ////{
                ////    if (_Anteprima)
                ////    {
                ////        this.DocumentoTroppiFogli = false;
                ////        this.TotalePagine = -1;
                ////        this.ElencoPagineDocumenti = new Dictionary<int, ContatoriPagineEntity>();
                ////    }
                ////    else
                ////    {
                ////        this.TotalePagine = this.ProgressivoPagine;
                ////        foreach (KeyValuePair<int, ContatoriPagineEntity> k in this.ElencoPagineDocumenti)
                ////        {
                ////            k.Value.PagineDocumento = 0;
                ////            k.Value.PagineSubDocumento = 0;
                ////            k.Value.PaginaUltimaParte = 0;
                ////        }
                ////    }
                ////    this.ProgressivoPagine = 0;
                ////    this.DocumentoInStampa = 0;
                ////}
            }
        }
                   
        public Boolean Ristampa
        {
            get
            {
                return LoopSinteticoDettaglio > 1;
            }
        }

        public Boolean RistampaDettaglio
        {
            get
            {
                return LoopSinteticoDettaglio > 2;
            }
        }

        public Boolean SwitchStampaDettaglio { get; set; }

        public String ArchiviazioneOtticaFN
        {
            get
            {
                if (_ArchiviazioneOtticaFN == null)
                {
                    System.Text.RegularExpressions.Regex re;
                    //String cFileName;

                    try
                    {
                        _ArchiviazioneOtticaFN = AccessiDati.ExtractValue(ArchiviazioneOtticaFNTipo, "", 0, "", 0);
                        if (this.ContatoriPagine().PartiDocumento > 1)
                        {
                            _ArchiviazioneOtticaFN = String.Concat(_ArchiviazioneOtticaFN, "_");
                            //if (this.SuffissoParte != "")
                            //    _ArchiviazioneOtticaFN = String.Concat(_ArchiviazioneOtticaFN, this.SuffissoParte, "_");
                            _ArchiviazioneOtticaFN = String.Concat(_ArchiviazioneOtticaFN, this.ContatoriPagine().ParteInStampa.ToString().PadLeft(3, '0'));
                        }

                        //
                        // Compongo il nome file.
                        //
                        if (!Ristampa)
                            _ArchiviazioneOtticaFN = String.Concat(this.Folders["ArchivioOtticoFLD"], _ArchiviazioneOtticaFN, ".pdf");
                        else
                        {
                            if (!RistampaDettaglio)
                                _ArchiviazioneOtticaFN = String.Concat(this.Folders["RistampeFLD"], _ArchiviazioneOtticaFN, ".pdf");
                            else
                                _ArchiviazioneOtticaFN = String.Concat(this.Folders["RistampeFLD"], _ArchiviazioneOtticaFN, "_ALIAS.pdf");
                        }

                        // ********************************************************************* //
                        // Se il nome è stato generato provvedo a verificarne la correttezza.    //
                        // ********************************************************************* //
                        re = new System.Text.RegularExpressions.Regex("^(([a-zA-Z]:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>\"|]*))+)$");
                        //if (!re.IsMatch(cFileName))
                        //    throw new Exception(String.Concat("Nome del file non corretto: \"", cFileName, "\".\nNon è possibile generare il file per l'archiviazione ottica."));
                    }
                    catch (Exception ex)
                    {
                        _ArchiviazioneOtticaFN = "";
                        throw ex;
                    }
                }
                return _ArchiviazioneOtticaFN;
            }

        }

        public string ArchiviazioneOtticaFNTipo { get; set; }

        public ContatoriPagineEntity ContatoriPagine(int nDocId = -1)
        {
            if (nDocId == -1)
            {
                nDocId = int.Parse(this.DBStampa.get_RecordMasterRow()[this.DBStampa.MainLinkToQD].ToString());
                return this.ElencoPagineDocumenti[nDocId];
            }
            else
                return this.ElencoPagineDocumenti[nDocId];
        }

        //
        // Database da stampare
        //
        public CLS_repositorydati DBStampa
        {
            get
            {
                return AccessiDati.RepositoryDati;
            }
            set
            {
                AccessiDati.RepositoryDati = value;
                if (value.DBDatiPlugIn != null)
                {
                    foreach (System.Data.DataRow dr in DBStampa.MainTable.Rows)
                        this.AggiungiContatoriPagineVuoto(int.Parse(dr[DBStampa.MainLinkToQD].ToString()));
                }
            }

        }

        public CLS_accessodati AccessiDati { get; set; }

        //
        // Indica quanti sono i documenti da stampare
        //
        public int DocumentiDaStampare { get; set; }

        //
        // Indica il progressivo dei documenti in stampa
        //
        public int DocumentoInStampa { get; set; }
        public Boolean DocumentoTroppiFogli { get; set; }
        public Boolean StampaDocumentoSinteticoDettaglio { get; set; }
        public int LoopSinteticoDettaglio { get; set; }

        public Boolean StampaForzataDettaglio { get; set; }

        public Dictionary<int, ContatoriPagineEntity> ElencoPagineDocumenti
        {
            get
            {
                return AccessiDati.RepositoryDati.ElencoPagineDocumenti;
            }
            set
            {
                AccessiDati.RepositoryDati.ElencoPagineDocumenti = value;
            }
        }

        public FattoreCorrezioneEntity FattoreCorrezione { get; set; }

        //
        // Indica i fogli utilizzati in caso di stampa cartacea
        //
        public Dictionary<int, int> FogliStampati()
        {
            Dictionary<int, int> f;
            int nFogli;

            f = new Dictionary<int, int>();
            foreach (int nKey in ElencoPagineDocumenti.Keys)
            {
                nFogli = ElencoPagineDocumenti[nKey].NumeroFogliDocumento;

                if (f.ContainsKey(nFogli))
                    f[nFogli] = f[nFogli] + 1;
                else
                    f.Add(nFogli, 1);
            }
            return f;
        }

        public CLS_FileFolderDic Folders { get; set; }
        public Boolean FronteRetro { get; set; }

        public Boolean InserisciFincatura
        {
            get
            {
                return _InserisciFincatura && !Anteprima;
            }
            set
            {
                _InserisciFincatura = value;
            }

        }

        public Dictionary<int, CassettoEntity> ListaCassettiUtilizzati { get; set; }
        public int MaxNumeroFogli { get; set; }
        public int PaginePerParte { get; set; }

      //  public GestioneModulo Modulo { get; set; }

        //
        // Indica il numero di pagine che sono state stampate per questo gruppo.
        //
        public int ProgressivoPagine { get; set; }
        public Boolean RendiPaginePari { get; set; }
        public Boolean StampaEseguita { get; set; }
        
        //
        // Indica il numero di pagine stampate per questo gruppo
        //
        public int TotalePagine { get; set; }
        public Decimal WrapRound
        {
            get
            {
                return _WrapRound;
            }
            set
            {
                _WrapRound = value % 10;
                if (_WrapRound == 0)
                    _WrapRound = 1;
            }
        }


        //
        // Dichiarazione delle variabili private
        //
        private Boolean _Anteprima { get; set; }
        private Boolean _InserisciFincatura { get; set; }
        private Boolean _StampaDaSplittare { get; set; }

        private Decimal _WrapRound { get; set; }

        public DatiStampaDettaglio() : base()
        {
            TotalePagine = -1;
            WrapRound = 1;
            Folders = new CLS_FileFolderDic();
            AccessiDati = new CLS_accessodati();
            FattoreCorrezione = new FattoreCorrezioneEntity();
        }

        //
        // Avanza i contatori parziali e globali delle pagine.
        //
        public void AvanzaPaginaStampata()
        {
            this.ProgressivoPagine += 1;
            this.ContatoriPagine().PagineDocumento += 1;
            this.ContatoriPagine().PagineSubDocumento += 1;
            //Me.AccessiDati.ProgressivoPagineDocumento = Me.ContatoriPagine.PagineDocumento + 1
            //            // Me.AccessiDati.ProgressivoPagineSubDocumento = Me.ContatoriPagine.PagineSubDocumento + 1
            if (!this.Anteprima)
                this.AvanzaWrapRound();
        }

        public void AvanzaWrapRound()
        {
            if (this.FronteRetro)
                this.WrapRound += 1 / 2;
            else
                this.WrapRound += 1;
        }

        //
        // Procedura per la gestione delle attività di fine stampa documento 
        //
        public void FineStampaDocumento(int nDocId = -1)
        {
            if (this.Anteprima)
                ContatoriPagine(nDocId).PagineTotaliDocumento = ContatoriPagine(nDocId).PagineDocumento;
            DocumentoInStampa += 1;
        }

        //
        // Aggiungi elementi dei contatori di pagina
        //
        public void AggiungiContatoriPagineVuoto(int k)
        {
            //if (!ElencoPagineDocumenti.ContainsKey(k))
            //    ElencoPagineDocumenti.Add(k, new ContatoriPagineEntity(this.ArchiviazioneOttica, this.FronteRetro, this.PaginePerParte, MaxNumeroFogli));
        }

        public DatiStampaDettaglio Clone()
        {
            return (DatiStampaDettaglio)this.MemberwiseClone();
        }

        private string _ArchiviazioneOtticaFN { get; set; }
    }

}

