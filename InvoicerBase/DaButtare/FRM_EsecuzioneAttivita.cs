﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;
using ISC.Invoicer40.Processi.Impl;

namespace ISC.Invoicer40.FormApplicazione
{
    public partial class EsecuzioneAttivita : Form
    {
        // 
        // Dichiarazione di proprietà ed enumeratori pubblici.
        //
        public enum eTipoAttivita
        {
            Stampa = 1,
            InvioMail = 2
        }

        private eTipoAttivita _TipoAttivita
        {
            get
            { return __TipoAttivita; }
            set
            {
                __TipoAttivita = value;
                if (__TipoAttivita == eTipoAttivita.Stampa)
                    this.Text = "Attività di stampa";
                else if (__TipoAttivita == eTipoAttivita.Stampa)
                    this.Text = "Invio email";
            }
        }

        protected log4net.ILog logger = LogManager.GetLogger("AppLog");
        protected log4net.ILog loggerTHRLoadDB = LogManager.GetLogger("THRLoadDBLog");
        protected log4net.ILog loggerTHRGenAnt = LogManager.GetLogger("THRGenAntLog");
        protected log4net.ILog loggerTHRGenSta = LogManager.GetLogger("THRGenStaLog");

        public ProcessoDiStampa ProcessoDiStampa
        {
            get { return _ProcessoDiStampa; }
            set
            {
                _ProcessoDiStampa = value;
                _TipoAttivita = EsecuzioneAttivita.eTipoAttivita.Stampa;
            }
        }

        private ProcessoDiStampa _ProcessoDiStampa { get; set; }
        private System.Threading.Thread _thrPS;
        // 
        // Dichiarazione di proprietà ed enumeratori privati
        //

        private eTipoAttivita __TipoAttivita { get; set; }

        //private eStatoCiclo _StatoCiclo { get; set; }

        public EsecuzioneAttivita()
        {
            InitializeComponent();
        }

        private void FRM_EsecuzioneAttivita_Load(object sender, EventArgs e)
        {
            ProcessoDiStampa.PreparaProcessoStampa();
            _thrPS = new System.Threading.Thread(ProcessoDiStampa.ProcessoStampa);
        }

        private void EsecuzioneAttivita_Shown(object sender, EventArgs e)
        {
            PGB_stato_gruppi.Maximum = ProcessoDiStampa.MaxGruppiGestiti;
            TMR_esegui.Start();
        }

        //
        // Procedura per la gestione del ciclo di stampa.
        //
        private void TMR_esegui_Tick(object sender, EventArgs e)
        {
            if (_thrPS.ThreadState == System.Threading.ThreadState.Unstarted)
                _thrPS.Start();
            SetUpMaschera();
            //System.Threading.Thread.Sleep(5000);
        }

        private void SetUpMaschera()
        {
            label2.Text = ProcessoDiStampa.StatoProcesso.ToString();

            LBL_grp_totali.Text = ProcessoDiStampa.MaxGruppiGestiti.ToString("#,##0") + "/" + ProcessoDiStampa.GRP_globale_Numero.ToString("#,##0");
            PGB_stato_gruppi.Value = ProcessoDiStampa.GRP_globale_Numero > 10 ? ProcessoDiStampa.MaxGruppiGestiti : ProcessoDiStampa.GRP_globale_Numero;

            //
            // Informazioni del Thread di Caricamento dei dati.
            //
            IMG_carica_dati.Image = GetImage(ProcessoDiStampa.THRCaricamento_Stato);

            LBL_grp_caricati.Text = ProcessoDiStampa.GRP_caricati_Numero.ToString("#,##0");
            LBL_grp_anteprima.Text = ProcessoDiStampa.GRP_AttesaAnteprima_Numero.ToString("#,##0") + "/" + ProcessoDiStampa.GRP_GeneraAnteprima_Numero.ToString("#,##0");
            LBL_grp_stampa.Text = ProcessoDiStampa.GRP_AttesaStampa_Numero.ToString("#,##0") + "/" + ProcessoDiStampa.GRP_GeneraStampa_Numero.ToString("#,##0");
            LBL_grp_fusione.Text = ProcessoDiStampa.GRP_PreAttesaFusione_Numero.ToString("#,##0") + "/" + ProcessoDiStampa.GRP_AttesaFusione_Numero.ToString("#,##0") + "/" + ProcessoDiStampa.GRP_GeneraFusione_Numero.ToString("#,##0");

            //
            // Informazioni del Thread di generazione dell'anteprima
            //
            IMG_prepara_anteprima.Image = GetImage(ProcessoDiStampa.THRPreparazAnteprima_Stato);

            //
            // Informazioni del Thread di generazione dell'anteprima
            //
            IMG_prepara_stampa.Image = GetImage(ProcessoDiStampa.THRPreparazStampa_Stato);

            //
            // Informazioni del thread di fusione dei documenti
            //
            IMG_fusione_documenti.Image = GetImage(ProcessoDiStampa.ThreadFondiDocumenti_Stato);
        }

        private Image GetImage(System.Threading.ThreadState s)
        {
            if (s == System.Threading.ThreadState.Unstarted)
                return IML_listaImmagini.Images[0];
            else if (s == System.Threading.ThreadState.WaitSleepJoin)
                return IML_listaImmagini.Images[1];
            else if (s == System.Threading.ThreadState.Running)
                return IML_listaImmagini.Images[2];
            else
                return IML_listaImmagini.Images[0];
        }

        private void EsecuzioneAttivita_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_thrPS != null)
            {
                _thrPS.Abort();
                _thrPS = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProcessoDiStampa.TerminaStampa = true;
        }
    }
}
