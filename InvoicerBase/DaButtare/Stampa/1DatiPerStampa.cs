﻿using System.Linq;
//using ISC.Invoicer.InvoicerDomain.Impl;
//using ISC.LibrerieComuni.ManageSessione.Modulo;
//using ISC.LibrerieComuni.ManageAccessoDati;
//using ISC.LibrerieComuni.OggettiComuni;

Namespace ISC.LibrerieComuni.GestioneAttivita
{

    Public Class DatiPerStampa
    {
        //
        // L'impostazione dell'anteprima di stampa condiziona anche i comportamenti dei contatori delle pagine.
        //
        //        Public Property Anteprima As Boolean

        //            Get
        //                Return _Anteprima
        //            End Get
        //            Set(ByVal value As Boolean)
        //                _Anteprima = value
        //                If _Anteprima Then
        //                    Me.DocumentoTroppiFogli = False
        //                    Me.TotalePagine = -1
        //                    Me.ElencoPagineDocumenti = New Dictionary(Of Integer, ContatoriPagineEntity)
        //                Else
        //                    Me.TotalePagine = Me.ProgressivoPagine
        //                    For Each k As KeyValuePair(Of Integer, ContatoriPagineEntity) In ElencoPagineDocumenti
        //                        k.Value.PagineDocumento = 0
        //                        k.Value.PagineSubDocumento = 0
        //                        k.Value.PaginaUltimaParte = 0
        //                    Next
        //                End If
        //                Me.ProgressivoPagine = 0
        //                Me.DocumentoInStampa = 0
        //            End Set

        //        End Property

        //        Public Property ArchiviazioneOttica As Boolean

        //        Public ReadOnly Property Ristampa As Boolean

        //            Get
        //                Return LoopSinteticoDettaglio > 1
        //            End Get

        //        End Property

        //        Public ReadOnly Property RistampaDettaglio As Boolean

        //            Get
        //                Return LoopSinteticoDettaglio > 2
        //            End Get

        //        End Property

        //        Public Property SwitchStampaDettaglio As Boolean

        //        Public ReadOnly Property ArchiviazioneOtticaFN() As String

        //            Get
        //                Dim re As System.Text.RegularExpressions.Regex
        //                Dim cFileName As String

        //                Try
        //                    cFileName = AccessiDati.ExtractValue(ArchiviazioneOtticaFNTipo, "", 0, "", 0)
        //                    If Me.ContatoriPagine.PartiDocumento > 1 Then
        //                        cFileName = String.Concat(cFileName, "_")
        //                        If Me.SuffissoParte > "" Then
        //                            cFileName = String.Concat(cFileName, Me.SuffissoParte, "_")
        //                        End If
        //                        cFileName = String.Concat(cFileName, Me.ContatoriPagine.ParteInStampa.ToString.PadLeft(3, "0"))
        //                    End If

        //                    //
        //                    // Compongo il nome file.
        ////
        //                    If Not Ristampa Then
        //                        cFileName = String.Concat(Me.Folders("ArchivioOtticoFLD"), cFileName, ".pdf")
        //                    Else
        //                        If Not RistampaDettaglio Then
        //                            cFileName = String.Concat(Me.Folders("RistampeFLD"), cFileName, ".pdf")
        //                        Else
        //                            cFileName = String.Concat(Me.Folders("RistampeFLD"), cFileName, "_ALIAS.pdf")
        //                        End If
        //                    End If


        //                    // ********************************************************************* //
        //                    // Se il nome è stato generato provvedo a verificarne la correttezza.    //
        //                    // ********************************************************************* //
        //                    re = New System.Text.RegularExpressions.Regex("^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$")
        //                    If Not re.IsMatch(cFileName) Then
        //                        Throw New Exception(String.Concat("Nome del file non corretto: """, cFileName, """.", vbLf, "Non è possibile generare il file per l'archiviazione ottica."))
        //                    End If
        //                Catch ex As Exception
        //                    cFileName = ""
        //                    Throw ex
        //                End Try
        //                Return cFileName
        //            End Get

        //        End Property

        //        Public Property ArchiviazioneOtticaFNTipo As String

        //        Public ReadOnly Property ContatoriPagine(Optional ByVal nDocId As Integer = -1) As ContatoriPagineEntity

        //            Get
        //                If nDocId = -1 Then
        //                    Return Me.ElencoPagineDocumenti(Me.DBStampa.RecordMasterRow(0).Item(DBStampa.MainLinkToQD))
        //                Else
        //                    Return Me.ElencoPagineDocumenti(nDocId)
        //                End If
        //            End Get

        //        End Property

        ////
        //        // Database da stampare
        ////
        //        Public Property DBStampa As CLS_repositorydati

        //            Get
        //                Return AccessiDati.RepositoryDati
        //            End Get
        //            Set(ByVal value As CLS_repositorydati)
        //                AccessiDati.RepositoryDati = value
        //            End Set

        //        End Property

        //        Public Property AccessiDati As CLS_accessodati

        //        //
        //        // Indica quanti sono i documenti da stampare
        //        //
        //        Public Property DocumentiDaStampare As Integer

        ////
        //        // Indica il progressivo dei documenti in stampa
        //        //
        //        Public Property DocumentoInStampa As Integer
        //        Public Property DocumentoTroppiFogli As Boolean

        //        Public Property StampaDocumentoSinteticoDettaglio As Boolean
        //        Public Property LoopSinteticoDettaglio As Integer
        //        Public Property StampaForzata As Boolean

        //        Public Property ElencoPagineDocumenti As Dictionary(Of Integer, ContatoriPagineEntity)

        //            Get
        //                Return AccessiDati.RepositoryDati.ElencoPagineDocumenti
        //            End Get
        //            Set(ByVal value As Dictionary(Of Integer, ContatoriPagineEntity))
        //                AccessiDati.RepositoryDati.ElencoPagineDocumenti = value
        //            End Set

        //        End Property

        //        Public Property FattoreCorrezione As FattoreCorrezioneEntity

        //        //
        //        // Indica i fogli utilizzati in caso di stampa cartacea
        //        //
        //        Public ReadOnly Property FogliStampati As Dictionary(Of Integer, Integer)

        //            Get
        //                Dim f As Dictionary(Of Integer, Integer)
        //                Dim nFogli As Integer

        //                f = New Dictionary(Of Integer, Integer)
        //                For Each nKey As Integer In ElencoPagineDocumenti.Keys
        //                    nFogli = ElencoPagineDocumenti(nKey).NumeroFogliDocumento

        //                    If f.ContainsKey(nFogli) Then
        //                        f(nFogli) = f(nFogli) + 1
        //                    Else
        //                        f.Add(nFogli, 1)
        //                    End If
        //                Next
        //                Return f
        //            End Get

        //        End Property

        //        Public Property Folders As CLS_FileFolderDic
        //        Public Property FronteRetro As Boolean

        //        Public Property InserisciFincatura As Boolean

        //            Get
        //                Return _InserisciFincatura And Not Anteprima
        //            End Get
        //            Set(ByVal value As Boolean)
        //                _InserisciFincatura = value
        //            End Set

        //        End Property

        //        Public Property ListaCassettiUtilizzati As Dictionary(Of Integer, CassettoEntity)
        //        Public Property MaxNumeroFogli As Integer
        //        Public Property PaginePerParte As Integer

        ////
        //        // Indica il numero di pagine che sono state stampate per questo gruppo.
        ////
        //        Public Property ProgressivoPagine As Integer
        //        Public Property RendiPaginePari As Boolean

        //        Public Property StampaDaSplittare1() As Boolean

        //            Get
        //                Dim lStampaDaSplittare As Boolean

        //                If Me.ArchiviazioneOttica And Not Me.Anteprima Then
        //                    lStampaDaSplittare = Me.ContatoriPagine.PagineDocumento > Me.PaginePerParte
        //                Else
        //                    lStampaDaSplittare = False
        //                End If
        //                Return lStampaDaSplittare
        //            End Get
        //            Set(ByVal value As Boolean)
        //                _StampaDaSplittare = value
        //            End Set

        //        End Property

        //        Public Property StampaEseguita As Boolean
        //        Public Property SuffissoParte As String

        //        //
        //        // Indica il numero di pagine stampate per questo gruppo
        ////
        //        Public Property TotalePagine As Integer

        //        Public Property WrapRound As Decimal

        //            Get
        //                Return _WrapRound
        //            End Get
        //            Set(ByVal value As Decimal)
        //                _WrapRound = value Mod 10
        //                If _WrapRound = 0 Then
        //                    _WrapRound = 1
        //                End If
        //            End Set

        //        End Property

        //        Private _Anteprima As Boolean
        //        //
        //        // Dichiarazione delle variabili private
        //        //
        //        Private _InserisciFincatura As Boolean
        //        Private _StampaDaSplittare As Boolean

        //        Private _WrapRound As Decimal

        //        Public Sub New()

        //            TotalePagine = -1
        //            WrapRound = 1
        //            Folders = New CLS_FileFolderDic
        //            AccessiDati = New CLS_accessodati()
        //            FattoreCorrezione = New FattoreCorrezioneEntity()

        //        End Sub

        ////
        //        // Avanza i contatori parziali e globali delle pagine.
        ////
        //        Public Sub AvanzaPaginaStampata()

        //            Me.ProgressivoPagine += 1
        //            Me.ContatoriPagine.PagineDocumento += 1
        //            Me.ContatoriPagine.PagineSubDocumento += 1
        //            //Me.AccessiDati.ProgressivoPagineDocumento = Me.ContatoriPagine.PagineDocumento + 1
        //            // Me.AccessiDati.ProgressivoPagineSubDocumento = Me.ContatoriPagine.PagineSubDocumento + 1
        //            If Not Me.Anteprima Then
        //                Me.AvanzaWrapRound()
        //            End If

        //        End Sub

        //        Public Sub AvanzaWrapRound()

        //            If Me.FronteRetro Then
        //                Me.WrapRound += 0.5
        //            Else
        //                Me.WrapRound += 1
        //            End If

        //        End Sub

        ////
        //        // Procedura per la gestione delle attività di fine stampa documento 
        //        //
        //        Public Sub FineStampaDocumento(Optional ByVal nDocId As Integer = -1)

        //            If Me.Anteprima Then
        //                ContatoriPagine(nDocId).PagineTotaliDocumento = ContatoriPagine(nDocId).PagineDocumento
        //            End If
        //            DocumentoInStampa += 1

        //        End Sub

        //        //
        //        // Aggiungi elementi dei contatori di pagina
        //        //
        //        Public Sub AggiungiContatoriPagineVuoto(ByVal k As Integer)

        //            If Not ElencoPagineDocumenti.ContainsKey(k) Then
        //                ElencoPagineDocumenti.Add(k, New ContatoriPagineEntity(Me.ArchiviazioneOttica, Me.FronteRetro, PaginePerParte, MaxNumeroFogli))
        //            End If

        //        End Sub

    }


}

