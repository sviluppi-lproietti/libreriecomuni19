﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.Invoicer40.ProcessiOLD
{
    public class ManageThread
    {
        public System.Threading.Thread ThreadCaricaDatabase { get; set; }
        public System.Threading.Thread ThreadGeneraAnteprima { get; set; }
        public System.Threading.Thread ThreadGeneraStampa { get; set; }
        public System.Threading.Thread ThreadFondiDocumenti { get; set; }

        public Boolean IsRunningCaricaDatabase
        {
            get
            {
                return (Stato_THR_CaricaDatabase == System.Threading.ThreadState.Running) || (Stato_THR_CaricaDatabase == System.Threading.ThreadState.WaitSleepJoin);
            }
        }
        public Boolean IsRunningGeneraAnteprima
        {
            get
            {
                return (Stato_THR_GeneraAnteprima == System.Threading.ThreadState.Running) || (Stato_THR_GeneraAnteprima == System.Threading.ThreadState.WaitSleepJoin);
            }
        }
        public Boolean IsRunningGeneraStampa
        {
            get
            {
                return (Stato_THR_GeneraStampa == System.Threading.ThreadState.Running) || (Stato_THR_GeneraStampa == System.Threading.ThreadState.WaitSleepJoin);
            }
        }
        public Boolean IsRunningFondiDocumenti
        {
            get
            {
                return (Stato_THR_FondiDocumenti == System.Threading.ThreadState.Running) || (Stato_THR_FondiDocumenti == System.Threading.ThreadState.WaitSleepJoin);
            }
        }

        public System.Threading.ThreadState Stato_THR_CaricaDatabase
        {
            get
            {
                if (ThreadCaricaDatabase == null)
                    return System.Threading.ThreadState.Unstarted;
                else if (ThreadCaricaDatabase.ThreadState == System.Threading.ThreadState.Aborted)
                    return System.Threading.ThreadState.Stopped;
                else
                    return ThreadCaricaDatabase.ThreadState;
            }
        }
        public System.Threading.ThreadState Stato_THR_GeneraAnteprima
        {
            get
            {
                if (ThreadGeneraAnteprima == null)
                    return System.Threading.ThreadState.Unstarted;
                else if (ThreadGeneraAnteprima.ThreadState == System.Threading.ThreadState.Aborted)
                    return System.Threading.ThreadState.Stopped;
                else
                    return ThreadGeneraAnteprima.ThreadState;
            }
        }
        public System.Threading.ThreadState Stato_THR_GeneraStampa
        {
            get
            {
                if (ThreadGeneraStampa == null)
                    return System.Threading.ThreadState.Unstarted;
                else if (ThreadGeneraStampa.ThreadState == System.Threading.ThreadState.Aborted)
                    return System.Threading.ThreadState.Stopped;
                else
                    return ThreadGeneraStampa.ThreadState;
            }
        }
        public System.Threading.ThreadState Stato_THR_FondiDocumenti
        {
            get
            {
                if (ThreadFondiDocumenti == null)
                    return System.Threading.ThreadState.Unstarted;
                else if (ThreadFondiDocumenti.ThreadState == System.Threading.ThreadState.Aborted)
                    return System.Threading.ThreadState.Stopped;
                else
                    return ThreadFondiDocumenti.ThreadState;
            }
        }

    }
}
