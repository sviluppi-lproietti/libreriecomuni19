﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Threading;
using System.Xml;
using System.Data;
using ISC.Invoicer.ManageFusione;
using ISC.Invoicer.ManageStampa;
using ISC.LibrerieComuni.ManageSessione;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.Invoicer40.ProcessiOLD.Base;
using ISC.LibrerieComuni.InvoicerDomain.Entity;
using ISC.LibrerieComuni.InvoicerService;
using ISC.LibrerieComuni.InvoicerService.Dati.Impl;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Base;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Impl;

namespace ISC.Invoicer40.ProcessiOLD.Impl
{
    public enum eStatiProcessoStampa
    {
        SPS_PredisposizionePerStampa = 1,
        SPS_AvvioCicloStampa = 2,
        SPS_CicloStampa = 3,
        SPS_TermineStampa = 4
    }

    public class ProcessoDiStampa : ProcessoBase
    {
        public DatiStampaGenerale DatiMaster { get; set; }
        public ManageThread gestThread { get; set; }
        public decimal GruppiDaStampare { get; set; }
        public decimal GruppoInStampa { get; set; }
        public Boolean TerminaStampa { get; set; }

        //protected log4net.ILog logger = LogManager.GetLogger("AppLog");
        //protected log4net.ILog loggerTHRLoadDB = LogManager.GetLogger("THRLoadDBLog");
        //protected log4net.ILog loggerTHRGenAnt = LogManager.GetLogger("THRGenAntLog");
        //protected log4net.ILog loggerTHRGenSta = LogManager.GetLogger("THRGenStaLog");
        //protected log4net.ILog loggerTHRFonDoc = LogManager.GetLogger("THRFonDocLog");

        protected Object lockAttivita;
        protected Object lockStampante;
        private CLS_PDFCreator _prnPDF;

        //private void AggiornaGruppiDiStampa()
        //{
        //    GruppoInStampa = 0;
        //    if (GruppiDaStampare > -1 && DatiMaster.ElementiPerGruppo_Numero > -1)
        //    {
        //        if ((GSS.RecordSelezionati % DatiMaster.ElementiPerGruppo_Numero) == 0)
        //            GruppiDaStampare = Math.Truncate(GSS.RecordSelezionati / DatiMaster.ElementiPerGruppo_Numero);
        //        else
        //            GruppiDaStampare = Math.Truncate(GSS.RecordSelezionati / DatiMaster.ElementiPerGruppo_Numero) + 1;
        //    }
        //}

        public Boolean RichiestaFusioneModuli { get; set; }
        public Boolean Pulisci_FolderElaborati { get; set; }

        public CLS_FileFolderDic Folders { get; set; }
        public int TipologiaStampa { get; set; }
        public int UltimoRecordDaStampare1 { get; set; }

        public ArrayList ModuliDaStampare_Codici1 { get; set; }

        //public int ModuliDaStampare_Numero1
        //{
        //    get
        //    {
        //        return ModuliDaStampare_Codici.Count;
        //    }
        //}

        private System.Xml.XmlDocument _xdFastPrint { get; set; }

        public int GRP_globale_Numero
        {
            get
            {
                return lisgrp_Globale.Count;
            }
        }

        public int GRP_creati_Numero
        {
            get
            {
                return lisgrp_Creati.Count;
            }
        }

        public int GRP_caricati_Numero
        {
            get
            {
                return lisgrp_Caricati.Count;
            }
        }

        public int GRP_AttesaAnteprima_Numero
        {
            get
            {
                return lisgrp_AttesaAnteprima.Count;
            }
        }

        public int GRP_GeneraAnteprima_Numero
        {
            get
            {
                return lisgrp_GeneraAnteprima.Count;
            }
        }

        public int GRP_AttesaStampa_Numero
        {
            get
            {
                return lisgrp_AttesaStampa.Count;
            }
        }

        public int GRP_GeneraStampa_Numero
        {
            get
            {
                return lisgrp_GeneraStampa.Count;
            }
        }

        public int GRP_PreAttesaFusione_Numero
        {
            get
            {
                return lisgrp_PreAttesaFusione.Count;
            }
        }

        public int GRP_AttesaFusione_Numero
        {
            get
            {
                return lisgrp_AttesaFusione.Count;
            }
        }

        public int GRP_GeneraFusione_Numero
        {
            get
            {
                return lisgrp_GeneraFusione.Count;
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_AttesaAnteprima
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_attesa_anteprima).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_GeneraAnteprima
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_genera_anteprima).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_AttesaStampa
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_attesa_stampa).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_GeneraStampa
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_genera_stampa).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_PreAttesaFusione
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_pre_attesa_fusione).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_AttesaFusione
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_attesa_fusione).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public Dictionary<decimal, GruppoBase> lisgrp_GeneraFusione
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_genera_fusione).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public int RecordStampare { get; set; }

        public Boolean THRCaricaDatabaseRunning
        {
            get { return RecordStampare <= GSS.RecordSelezionati; }
        }

        public Boolean THRGeneraAnteprimaRunning
        {
            get { return THRCaricaDatabaseRunning || lisgrp_AttesaAnteprima.Count > 0 || lisgrp_GeneraAnteprima.Count > 0; }
        }

        public Boolean THRGeneraStampaRunning
        {
            get { return THRGeneraAnteprimaRunning || lisgrp_AttesaStampa.Count > 0 || lisgrp_GeneraStampa.Count > 0; }
        }

        public Boolean THRFondiDocumentiRunning
        {
            get { return THRGeneraStampaRunning || lisgrp_PreAttesaFusione.Count > 0 || lisgrp_AttesaFusione.Count > 0 || lisgrp_GeneraFusione.Count > 0; }
        }

        public GruppoPerStampa saTHRLD { get; set; }

        public GruppoPerStampa saTHRGA
        {
            get
            {
                return null; // (StrutturaGruppoStampa)lisgrp_GeneraAnteprima.First().Value;
            }
        }

        public GruppoPerStampa saTHRGS
        {
            get
            {
                return null; // (StrutturaGruppoStampa)lisgrp_GeneraStampa.First().Value;
            }
        }

        public GruppoPerStampa saTHRFD
        {
            get
            {
                return null; // (StrutturaGruppoStampa)lisgrp_GeneraFusione.First().Value;
            }
        }

        public Boolean Need_CaricaBaseDati
        {
            get
            {
                return base.lisgrp_Globale.Count < this.MaxGruppiGestiti;
            }
        }

        public GestioneSessione GSS { get; set; }

        public Dictionary<int, CLS_GestioneModuloStampare> ModuliDaStampare { get; set; }

        public ProcessoDiStampa() : base()
        {
            gestThread = new ManageThread();
            //ModuliDaStampare_Codici = new ArrayList();
            ModuliDaStampare = new Dictionary<int, CLS_GestioneModuloStampare>();
            DatiMaster = new DatiStampaGenerale();
            lockAttivita = new Object();
            FileDeletingLocked = false;
        }

        public eStatiProcessoStampa StatoProcesso { get; set; }

        public void PreparaProcessoStampa()
        {
            TerminaStampa = false;
            StatoProcesso = eStatiProcessoStampa.SPS_PredisposizionePerStampa;
        }

        public void ProcessoStampa()
        {
            //
            // Valorizza le variabili per il lock delle risorse.
            //
            lockAttivita = new Object();
            lockStampante = new Object();
            while (StatoProcesso != eStatiProcessoStampa.SPS_TermineStampa)
            {
                if (StatoProcesso == eStatiProcessoStampa.SPS_PredisposizionePerStampa)
                {
                    OggettiGlobali.logger.Info("INIZIO - Processo di Stampa - Predisposizione per stampa");
                    lisgrp_Globale.Clear();
                    gestThread.ThreadCaricaDatabase = new System.Threading.Thread(new System.Threading.ThreadStart(THRCaricaDatabase));
                    gestThread.ThreadGeneraAnteprima = new System.Threading.Thread(new System.Threading.ThreadStart(THRGeneraAnteprima));
                    gestThread.ThreadGeneraStampa = new System.Threading.Thread(new System.Threading.ThreadStart(THRGeneraStampa));
                    if (this.RichiestaFusioneModuli)
                        gestThread.ThreadFondiDocumenti = new System.Threading.Thread(new System.Threading.ThreadStart(THRFondiDocumenti));

                    //
                    // Attiva stamapnte ottica
                    //
                    Carica_FileDatiAnteprima();

                    StatoProcesso = eStatiProcessoStampa.SPS_AvvioCicloStampa;
                    OggettiGlobali.logger.Info("INIZIO - Processo di Stampa - Ciclo di stampa");
                }
                else if (StatoProcesso == eStatiProcessoStampa.SPS_AvvioCicloStampa)
                {
                    //logger.Debug("  > Strutture di stampa attive: " + GestAttStampa.GAS_Complessivo.Count.ToString());
                    if (gestThread.ThreadCaricaDatabase.ThreadState == ThreadState.Unstarted)
                    {
                        OggettiGlobali.logger.Info("  > Avvio thread caricamento dati dal database");
                        gestThread.ThreadCaricaDatabase.Start();
                    }

                    if (gestThread.ThreadGeneraAnteprima.ThreadState == ThreadState.Unstarted)
                    {
                        OggettiGlobali.logger.Info("  > Avvio thread generazione anteprima");
                        gestThread.ThreadGeneraAnteprima.Start();
                    }

                    if (gestThread.ThreadGeneraStampa.ThreadState == ThreadState.Unstarted)
                    {
                        OggettiGlobali.logger.Info("  > Avvio thread generazione stampa");
                        gestThread.ThreadGeneraStampa.Start();
                    }

                    if (this.RichiestaFusioneModuli && gestThread.ThreadFondiDocumenti.ThreadState == ThreadState.Unstarted)
                    {
                        OggettiGlobali.logger.Info("  > Avvio thread generazione stampa");
                        gestThread.ThreadFondiDocumenti.Start();
                    }
                    StatoProcesso = eStatiProcessoStampa.SPS_CicloStampa;
                    OggettiGlobali.logger.Info("FINE - Avvio ciclo di stampa");
                }
                else if (StatoProcesso == eStatiProcessoStampa.SPS_CicloStampa)
                {
                    OggettiGlobali.logger.Info("  > Strutture caricate: " + lisgrp_Globale.Count.ToString());
                    lock (lockAttivita)
                    {
                        //foreach (StrutturaGruppoBase sa in lisgrp_Globale.Values)
                        //    OggettiGlobali.logger.Debug("Chiave: " + sa.Chiave.ToString() + " Stato: " + sa.Stato.ToString());
                    }
                    if (((gestThread.ThreadCaricaDatabase.ThreadState == System.Threading.ThreadState.Stopped) && (this.lisgrp_Terminati.Count > 0)) ||
                            (this.lisgrp_Terminati.Count >= this.MaxGruppiGestiti / 2))
                    {
                        OggettiGlobali.logger.Debug("  > INIZIO - Esistono strutture terminate da rimuovere.");
                        lock (lockAttivita)
                        {
                            foreach (Decimal nKey in this.lisgrp_Terminati.Keys)
                            {
                                OggettiGlobali.logger.Debug("    > Rimuovo la struttura con chiave: " + nKey.ToString());
                                this.RimuoviStrutturaAttivita(nKey);
                            }
                        }
                        OggettiGlobali.logger.Debug("  > FINE - Esistono strutture terminate da rimuovere.");
                    }
                    else
                        OggettiGlobali.logger.Debug("  > Non esistono struttura terminate da rimuovere");

                    //
                    // Se ho ucciso la stampa allora devo uccidere anche i thread figli.
                    //
                    if (TerminaStampa)
                    {
                        gestThread.ThreadCaricaDatabase.Abort();
                        gestThread.ThreadGeneraAnteprima.Abort();
                        gestThread.ThreadGeneraStampa.Abort();
                        if (this.RichiestaFusioneModuli)
                            gestThread.ThreadFondiDocumenti.Abort();
                        TerminaStampa = false;
                    }

                    //
                    // Se i thread sono tutti terminati allora procedo con il termine del ciclo.
                    //
                    if ((!gestThread.IsRunningCaricaDatabase && (!gestThread.IsRunningGeneraAnteprima) &&
                        (!gestThread.IsRunningGeneraStampa) && (!gestThread.IsRunningFondiDocumenti)))
                    {
                        lock (lockAttivita)
                        {
                            foreach (Decimal nKey in this.lisgrp_Terminati.Keys)
                            {
                                OggettiGlobali.logger.Debug("    > Rimuovo la struttura con chiave: " + nKey.ToString());
                                this.RimuoviStrutturaAttivita(nKey);
                            }
                        }
                        StatoProcesso = eStatiProcessoStampa.SPS_TermineStampa;
                    }
                    //        If ((_ThreadCaricaDatabase.ThreadState = Threading.ThreadState.Stopped) And (_ThreadGeneraAnteprima.ThreadState = Threading.ThreadState.Stopped) And (_ThreadGeneraStampa.ThreadState = Threading.ThreadState.Stopped)) Then
                    //            _StatoCiclo = eStatoCiclo.TermineStampa
                    //        End If
                    //        Label15.Text = GestAttStampa.GAS_Complessivo.Count
                    //        Label1.Text = _ThreadCaricaDatabase.ThreadState.ToString()
                    //        Label2.Text = String.Concat(GestAttStampa.GAS_Creato.Count, " <-> ", GestAttStampa.GAS_caricato.Count)

                    //        Label5.Text = _ThreadGeneraAnteprima.ThreadState.ToString()
                    //        Label6.Text = String.Concat(GestAttStampa.GAS_AttesaAnteprima.Count, " <-> ", GestAttStampa.GAS_GeneraAnteprima.Count)
                    //        'If (GestAttStampa.GAS_GeneraAnteprima.Count > 0) Then
                    //        '    Label15.Text = GestAttStampa.saTHRGA.MainTable.Rows(0)(GestAttStampa.saTHRGA.MainLinkToQD)
                    //        'Else
                    //        '    Label15.Text = "--"
                    //        'End If
                    //        Label7.Text = _ThreadGeneraStampa.ThreadState.ToString()
                    //        Label8.Text = String.Concat(GestAttStampa.GAS_AttesaStampa.Count, " <-> ", GestAttStampa.GAS_GeneraStampa.Count)
                    //        'If (GestAttStampa.GAS_GeneraStampa.Count > 0) Then
                    //        '    Label16.Text = GestAttStampa.GAS_GeneraStampa.First().Value.MainTable.Rows(0)(GestAttStampa.GAS_GeneraStampa.First().Value.MainLinkToQD)
                    //        'Else
                    //        '    Label16.Text = "--"
                    //        'End If
                    //        '_StatoCiclo = eStatoCiclo.TermineStampa
                    OggettiGlobali.logger.Info("FINE - Ciclo di stampa");
                }
                else if (StatoProcesso == eStatiProcessoStampa.SPS_TermineStampa)
                {
                    //            Restore_PDFPrinterParam()
                    OggettiGlobali.logger.Info("FINE - Processo di Stampa - Termina processo stampa");
                }
            }
        }

        private void THRCaricaDatabase()
        {
            decimal loopModulo;
            string cItemToken;

            OggettiGlobali.loggerTHRLoadDB.Info("INIZIO - Avvio del THREAD per il caricamento dei dati.");
            this.RecordStampare = 0;
            while (this.THRCaricaDatabaseRunning)
            {
                try
                {
                    if (this.Need_CaricaBaseDati)
                    {
                        OggettiGlobali.loggerTHRLoadDB.Debug("  > Caricamento necessario. procedo");
                        loopModulo = 0;
                        cItemToken = DateTime.Now.ToString("yyyyMMddhhmmss");
                        foreach (CLS_GestioneModuloStampare m in ModuliDaStampare.Values)
                        {
                            loopModulo = loopModulo + 1;
                            //
                            // Attivazione modulo per il recupero dei parametri delle tabelle di servizio.
                            // Deve essere attivata prima.
                            // 
                            this.GSS.AttivaModulo(m.CodiceModulo);
                            saTHRLD = null; // new StrutturaGruppoStampa(this.GSS.FullDataset_MainTableName, this.GSS.Files["PlugInDatiFissiFIL"], GSS.LinkField_QuickDS_FullDS, m.StampaForzataDettaglio);
                            saTHRLD.ItemToken = cItemToken;
                            saTHRLD.Raccolta = ModuliDaStampare.Values.Count == 1 ? 9 : 0;
                            if (saTHRLD.Raccolta == 0)
                            {
                                if (loopModulo == 1)
                                    saTHRLD.Raccolta = 1;
                                else if (loopModulo == ModuliDaStampare.Values.Count)
                                    saTHRLD.Raccolta = 3;
                                else
                                    saTHRLD.Raccolta = 2;
                            }
                            saTHRLD.DatiGenerali = (DatiStampaGenerale)this.DatiMaster.Clone();
                        //    saTHRLD.DatiGruppo.GMS = m.Clone();

                            saTHRLD.DatiGruppo.ArchiviazioneOtticaFNTipo = m.Modulo.NomeFileArchivioOtticoSelect.AOF_filename;
                            saTHRLD.DatiGruppo.FusioneFNTipo = GSS.ModuloUnificato.NomeFileArchivioOtticoSelect.AOF_filename;
                            //saTHRLD.DatiGruppo.InserisciFincatura = m.InserisciFincatura;

                            //saTHRLD.DatiGruppo.DatiCore = saTHRLD.DatiGenerali.DatiBaseStampa;
                            //saTHRLD.DatiGruppo.FronteRetro = saTHRLD.DatiGenerali.FronteRetro;

                     //       OggettiGlobali.loggerTHRLoadDB.Info("  > Caricamento per il record: " + saTHRLD.DatiGenerali.RecordDaStampare_Primo.ToString() + " Modulo: " + saTHRLD.CodiceModulo.ToString());
                            CaricaDatabase();
                            lock (lockAttivita)
                            {
                                if (saTHRLD.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                                {
                                    OggettiGlobali.loggerTHRLoadDB.Debug("  > Il repository è vuoto lo sposto in stato PreAttesaFusione o lo metto in stato terminato.");
                                    //if (this.RichiestaFusioneModuli)
                                    // //   saTHRLD.Stato = eStatoGruppo.STR_pre_attesa_fusione;
                                    //else
                                    //    saTHRLD.Stato = eStatoGruppo.STR_fine;
                                }
                                else
                                {
                                    OggettiGlobali.loggerTHRLoadDB.Debug("  > Il repository è pieno lo aggiungo a quelli da stampare.");
                                    //saTHRLD.Stato = eStatoGruppo.STR_attesa_anteprima;
                                }
                                //AggiungiStrutturaAttivita(saTHRLD);
                                OggettiGlobali.loggerTHRLoadDB.Debug("  > Aggiunta effettuata con successo.");
                            }
                        }
                      //  this.DatiMaster.RecordDaStampare_Primo = this.DatiMaster.RecordDaStampare_Ultimo;
                        this.RecordStampare += 1;
                    }
                    else
                    {
                        OggettiGlobali.loggerTHRLoadDB.Debug("  > Caricamento non necessario. Attendo.");
                        Thread.Sleep(1000);
                    }
                }
                catch (Exception ex)
                {
                    OggettiGlobali.loggerTHRLoadDB.Error("ERRORE - THRCaricaDatabase - " + ex.Message, ex);
                }
            }
            OggettiGlobali.loggerTHRLoadDB.Info("FINE - Termine del THREAD per il caricamento dei dati.");
        }

        // ********************************************************************* '
        // Caricamento dei dati che andranno stampati.                           '
        // ********************************************************************* '
        private void CaricaDatabase()
        {
            DataRow dr_mai_qd;

            OggettiGlobali.loggerTHRLoadDB.Debug("  > INIZIO - Caricamento database");
            try
            {
                //OggettiGlobali.loggerTHRLoadDB.Debug("      > Attivazione del modulo: " + saTHRLD.NomeModulo + " (" + saTHRLD.CodiceModulo.ToString() + ")");
                //GSS.GoToRecordNumber(saTHRLD.DatiGenerali.RecordDaStampare_Primo);
                //OggettiGlobali.loggerTHRLoadDB.Debug("      > Primo record da caricare.: " + saTHRLD.DatiGenerali.RecordDaStampare_Primo.ToString());
                //OggettiGlobali.loggerTHRLoadDB.Debug("      > Numero record da caricare: " + this.DatiMaster.ElementiPerGruppo_Numero.ToString());
                //GSS.Carica_FullDataset(int.Parse(this.DatiMaster.ElementiPerGruppo_Numero.ToString()));
                saTHRLD.DatiGruppo.AssegnaDatabase(GSS.FullDataset);

                OggettiGlobali.loggerTHRLoadDB.Debug("      > Numero Record caricati: " + saTHRLD.RecordFullDatasetMainTable.ToString());
                if (!saTHRLD.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                {
                    OggettiGlobali.loggerTHRLoadDB.Debug("      > Verifico se i record che ho caricato sono da stampare");

                    //
                    // Se stiamo stampando i docuementi cartacei e non abbiamo impostato la forzatura della stampa cartacea allora procedo a cancellare i dati dei record interessati.
                    //
                    //if (!this.DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica && !this.DatiMaster.DatiBaseStampa.Stampa_ForzaCartacea && !saTHRLD.DatiGruppo.GMS.ModuloSenzaSelezioneNecessaria)
                    //    foreach (DataRow dr_fud in saTHRLD.DatiGruppo.AccessoDati.MainTable.Rows)
                    //    {
                    //        // ********************************************************************* //
                    //        // Per alcuni plug-in il valore della colonna MAINTABLEKEY della tabella //
                    //        // QUICKDATASET è di tipo 'string' e pertanto va effettuata una select   //
                    //        // con tipologia stringa quindi con apici prima e dopo il valore ricerca //
                    //        // to.                                                                   //
                    //        // ********************************************************************* //
                    //        if (GSS.QuickDataTable.Columns[this.GSS.QuickDataset_MainTableKey.ToString()].DataType == typeof(System.String))
                    //            dr_mai_qd = GSS.QuickDataTable.Select(String.Concat(GSS.QuickDataset_MainTableKey, " = '", dr_fud[GSS.LinkField_QuickDS_FullDS], "'"))[0];
                    //        else
                    //            dr_mai_qd = GSS.QuickDataTable.Rows.Find(dr_fud[GSS.LinkField_QuickDS_FullDS]);   //' .Select(String.Concat(GSS.QuickDataset_MainTableKey, " = ", dr_fud.Item(GSS.LinkField_QuickDS_FullDS)))(0)

                    //        if (((Boolean)dr_mai_qd["DEF_onlyarchott"]) && ((Boolean)dr_mai_qd["DEF_sendbymail"]))
                    //        {
                    //            OggettiGlobali.loggerTHRLoadDB.Debug("      > Record rimosso.: " + dr_fud[GSS.LinkField_QuickDS_FullDS].ToString());
                    //            dr_fud.Delete();
                    //        }
                    //        saTHRLD.DatiGruppo.AccessoDati.MainTable.AcceptChanges();
                    //    }
                }
                //this.DatiMaster.RecordDaStampare_Ultimo = GSS.CurrentRecordNumber;
            }
            catch (Exception ex)
            {
                OggettiGlobali.loggerTHRLoadDB.Error("      > ERRORE - Caricamento database", ex);
            }
            OggettiGlobali.loggerTHRLoadDB.Debug("  > FINE - Caricamento database");
        }

        private void THRGeneraAnteprima()
        {
            OggettiGlobali.loggerTHRGenAnt.Info("INIZIO - Avvio del THREAD per la generazione delle anteprime.");
            while (this.THRGeneraAnteprimaRunning)
            {
                //loggerTHRGenAnt.Debug("Documento da stampare su stampante (2): " + _prnPDF.AutosaveFilename);
                if ((this.GRP_AttesaAnteprima_Numero > 0) && (this.GRP_GeneraAnteprima_Numero == 0))
                    this.lisgrp_AttesaAnteprima.First().Value.Stato = eStatoGruppo.STR_genera_anteprima;
                else if (this.GRP_GeneraAnteprima_Numero == 1)
                {
                    //if (!this.DatiMaster.DatiBaseStampa.Abilita_FastPrint || (this.DatiMaster.DatiBaseStampa.Abilita_FastPrint & !this.Recupera_DatiAnteprima()))
                    //{
                    //    OggettiGlobali.loggerTHRGenAnt.Debug("  > Generazione anteprima di stampa NECESSARIA per il record codice: " + this.saTHRGA.Chiave .ToString() + " modulo: " + this.saTHRGA.CodiceModulo.ToString());
                    //    AnteprimaDocumentoTHR();
                    //    this.Salva_DatiAnteprima();
                    //}
                    //else
                    //    OggettiGlobali.loggerTHRGenAnt.Debug("  > Generazione anteprima di stampa NON NECESSARIA per il record codice: " + this.saTHRGA.Chiave .ToString() + " modulo: " + this.saTHRGA.CodiceModulo.ToString());

                    lock (lockAttivita)
                    {
                       // this.saTHRGA.AvanzaStato();
                    }
                }
                else
                {
                    OggettiGlobali.loggerTHRGenAnt.Debug("  > Generazione anteprime non necessaria. Attendo.");
                    Thread.Sleep(1000);
                }
            }
            OggettiGlobali.loggerTHRGenAnt.Info("FINE - Termine del THREAD per la generazione delle anteprime.");

        }

        public void Carica_FileDatiAnteprima()
        {
            //_DatiAnteprimaFN = cFileName;
            try
            {
                _xdFastPrint = new System.Xml.XmlDocument();
                if (System.IO.File.Exists(GSS.Files["FastPrintFIL"]))
                    _xdFastPrint.Load(GSS.Files["FastPrintFIL"]);
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(GSS.Files["FastPrintFIL"]);
                _xdFastPrint = null;
                OggettiGlobali.logger.Error("Caricamento file per fast print.", ex);
                throw ex;
            }
            finally
            {
                if (!_xdFastPrint.HasChildNodes)
                {
                    _xdFastPrint.LoadXml("<fastprint></fastprint>");
                  //  this.DatiMaster.DatiBaseStampa.Abilita_FastPrint = false;
                }
            }
        }

        private Boolean AnteprimaDocumentoTHR()
        {
            CLS_motore_stampa prnprn;
            Boolean lRtn;

            lRtn = true;
            try
            {
                //if (DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                //    do
                //    {
                //        CancellaDocumentoGenerare(saTHRGA.DatiGruppo.ArchiviazioneOtticaFN);
                //        if (FileDeletingLocked)
                //            Thread.Sleep(1000);
                //    }
                //    while (FileDeletingLocked);
                lock (lockStampante)
                {
                    //if (DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                    //{
                    //    prnprn = new CLS_motore_stampa(this.saTHRGA.DatiGruppo.GMS.NomeStampanteOttica);
                    //}
                    //else
                    //    prnprn = new CLS_motore_stampa(this.saTHRGA.DatiGruppo.GMS.NomeStampanteCartacea);
                    prnprn = null;
                    prnprn.DatiGenerali = (DatiStampaGenerale)this.DatiMaster.Clone();
                    //prnprn.DatiGruppoOLD = this.saTHRGA.DatiGruppo;
                    prnprn.WorkFLD = this.Folders["PrnWrkFLD"];
                    prnprn.PrintController = new System.Drawing.Printing.StandardPrintController();
                    prnprn.PrinterSettings.PrintFileName = String.Concat(this.Folders["PrnWrkFLD"], "print_temp_", OGC_utilita.Timestamp());

                    if (prnprn != null)
                    {
                        prnprn.Print();
                        //if (this.saTHRGA.DatiGruppo.ContatoriPagine().PartiDocumento > 1)
                        //SetUpError2QuickDataset(prnprn.StatusPrn);
                        prnprn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                OggettiGlobali.loggerTHRGenAnt.Error("      > ERRORE - Stampa anteprima. ", ex);
            }
            return lRtn;
        }

        private void THRGeneraStampa()
        {
            OggettiGlobali.loggerTHRGenAnt.Info("INIZIO - Avvio del THREAD per la generazione delle anteprime.");
            while (this.THRGeneraStampaRunning)
            {
                if ((this.GRP_AttesaStampa_Numero > 0) && (this.GRP_GeneraStampa_Numero == 0))
                    this.lisgrp_AttesaStampa.First().Value.Stato = eStatoGruppo.STR_genera_stampa;
                else if (this.GRP_GeneraStampa_Numero == 1)
                {
                   // OggettiGlobali.loggerTHRGenSta.Debug("  > Generazione anteprima per base date per il record codice: " + this.saTHRGS.Chiave .ToString() + " modulo: " + this.saTHRGS.CodiceModulo.ToString());
                    StampaDocumentoTHR();
                    lock (lockAttivita)
                    {
                      //  this.saTHRGS.Stato  = this.RichiestaFusioneModuli ? eStatoGruppo.STR_pre_attesa_fusione : eStatoGruppo.STR_fine;
                    }
                }
                else
                {
                    OggettiGlobali.loggerTHRGenAnt.Debug("  > Generazione anteprime non necessaria. Attendo.");
                    Thread.Sleep(1000);
                }
            }
            OggettiGlobali.loggerTHRGenAnt.Info("FINE - Termine del THREAD per la generazione delle anteprime.");
        }

        private void StampaDocumentoTHR()
        {
            CLS_motore_stampa prnprn;
            String cTmp;

            try
            {
                OggettiGlobali.loggerTHRGenSta.Debug("INIZIO - Procedura di stampa");
                OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare: " + this.saTHRGS.DatiGruppo.ArchiviazioneOtticaFN);
                lock (lockStampante)
                {
                    //loggerTHRGenSta.Debug("Documento da stampare su stampante (1): " + _prnPDF.AutosaveFilename);
                    //if (this.DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                    //{

                    //    prnprn = new CLS_motore_stampa(this.saTHRGS.DatiGruppo.GMS.NomeStampanteOttica);
                    //}
                    //else
                    //    prnprn = new CLS_motore_stampa(this.saTHRGS.DatiGruppo.GMS.NomeStampanteCartacea);

                    //loggerTHRGenSta.Debug("Documento da stampare su stampante (2): " + _prnPDF.AutosaveFilename);
                    prnprn = null;
                    prnprn.DatiGenerali = this.DatiMaster;
                   // prnprn.DatiGruppoOLD = this.saTHRGS.DatiGruppo;
                    prnprn.PrintController = new System.Drawing.Printing.StandardPrintController();
                    //loggerTHRGenSta.Debug("Documento da stampare su stampante (2b): " + _prnPDF.AutosaveFilename);
                    if (prnprn
                        != null)
                    {
                        //loggerTHRGenSta.Debug("Documento da stampare su stampante (2c): " + _prnPDF.AutosaveFilename);
                        for (int i = 0; i <= this.saTHRGS.DatiGruppo.AccessoDati.ContatorePagineCurr().PartiDocumento; i++)
                        {
                            //if (this.DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                            //    PredisponiStampanteOttica(MST_enumeratori.ePDFCreatorFileType.FT_PDF1A);
                            OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare su stampante (2d): " + _prnPDF.AutosaveFilename);
                            prnprn.Print();
                            this.saTHRGS.DatiGruppo.AccessoDati.ContatorePagineCurr().ParteInStampa += 1;
                            OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare su stampante (2d): " + _prnPDF.AutosaveFilename);
                        }
                        //if (! this.saTHRGS.DatiGruppo.DBStampa.PartiDocumento > 1 )
                        //    SetUpError2QuickDataset(prnprn.StatusPrn);
                        OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare su stampante (2e): " + _prnPDF.AutosaveFilename);

                        prnprn.Dispose();
                        while ((!System.IO.File.Exists(this.saTHRGS.DatiGruppo.ArchiviazioneOtticaFN) && !TerminaStampa))
                        {
                            System.Threading.Thread.Sleep(500);
                            OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare su stampante (3): " + _prnPDF.AutosaveFilename);
                        }
                        while (_prnPDF.NumeroJobInStampa > 0 && !TerminaStampa)
                        {
                            Thread.Sleep(500);
                            // ********************************************************************* //
                            // L'istruzione che segue serve esclusivamente a controllare un abort    // 
                            // del processo PDFCreator.                                              //
                            // ********************************************************************* //
                            cTmp = _prnPDF.AutosaveFilename;
                            OggettiGlobali.loggerTHRGenSta.Debug("Documento da stampare su stampante: " + _prnPDF.AutosaveFilename);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OggettiGlobali.loggerTHRGenSta.Error("      > ERRORE - Stampa definitiva. ", ex);
            }
            OggettiGlobali.loggerTHRGenSta.Debug("FINE - Procedura di stampa");
        }

        public Boolean Recupera_DatiAnteprima()
        {
            Boolean lRtn;
            ContatoriPagineEntity c;
            XmlNode xnGruppo;
            int nKey;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            lRtn = false;
            try
            {
                if (this.saTHRGA.DatiGruppo.AccessoDati.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(this.saTHRGA.DatiGruppo.AccessoDati.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = null;
                //xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GSS.CheckSumPlugIn, "' and @modulo='", this.saTHRGA.DatiGruppo.GMS.Modulo.ModuloCheckSum, "' and @datifissi='", GSS.CheckSumDatiFissi, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo != null)
                {
                    foreach (DataRow dr in this.saTHRGA.DatiGruppo.AccessoDati.MainTable.Rows)
                    {
                        nKey = int.Parse(dr[this.saTHRGA.DatiGruppo.AccessoDati.MainLinkToQD].ToString());
                        xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", nKey, "']"));
                        if (xnDocumento != null)
                        {
                            c = this.saTHRGA.DatiGruppo.AccessoDati.ElencoPagineDocumenti[nKey];
                            if (!c.GiaEseguitaAnteprima)
                            {
                                foreach (string cValue in xnDocumento.SelectSingleNode("parzia").InnerText.Split(';'))
                                {
                                    //c.PagineDocumento += int.Parse(cValue);
                                    c.FineStampaSubDocumento(int.Parse(cValue));
                                }
                                c.GiaEseguitaAnteprima = true;
                                //this.saTHRGA.DatiGruppo.ProgressivoPagine += c.PagineDocumento;
                                //this.saTHRGA.DatiGruppo.FineStampaDocumento(nKey);
                            }
                            lRtn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lRtn = false;
                throw ex;
            }
            return lRtn;
        }

        public void Salva_DatiAnteprima()
        {
            XmlNode xnGruppo;
            string cPagineparziali;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            try
            {
                if (this.saTHRGA.DatiGruppo.AccessoDati.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(this.saTHRGA.DatiGruppo.AccessoDati.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = null;
                //xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GSS.CheckSumPlugIn, "' and @modulo='", this.saTHRGA.DatiGruppo.GMS.Modulo.ModuloCheckSum, "' and @datifissi='", GSS.CheckSumDatiFissi, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo == null)
                {
                    xnGruppo = _xdFastPrint.CreateNode(XmlNodeType.Element, "anteprima", "");
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("nomemodulo", ""));
                   // xnGruppo.Attributes["nomemodulo"].Value = this.saTHRGA.NomeModulo;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("plugin", ""));
                    xnGruppo.Attributes["plugin"].Value = GSS.CheckSumPlugIn;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("modulo", ""));
                    //xnGruppo.Attributes["modulo"].Value = this.saTHRGA.DatiGruppo.GMS.Modulo.ModuloCheckSum;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("datifissi", ""));
                    xnGruppo.Attributes["datifissi"].Value = GSS.CheckSumDatiFissi;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("dettaglio", ""));
                    xnGruppo.Attributes["dettaglio"].Value = lDettaglio.ToString();
                    _xdFastPrint.SelectSingleNode("fastprint").AppendChild(xnGruppo);
                }

                //
                // All'interno del gruppo ricerca la presenza del documento di cui devo salvare i dati di anteprima.
                //
                //foreach (KeyValuePair<decimal, ContatoriPagineEntity> k in saTHRGA.DatiGruppo.ElencoPagineDocumenti)
                //{
                //    xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", k.Key, "']"));
                //    if (xnDocumento == null)
                //    {
                //        xnDocumento = _xdFastPrint.CreateNode(XmlNodeType.Element, "documento", "");
                //        xnDocumento.Attributes.Append(_xdFastPrint.CreateAttribute("codice", ""));
                //        xnDocumento.Attributes["codice"].Value = k.Key.ToString();
                //        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "totali", ""));
                //        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "parzia", ""));
                //        xnGruppo.AppendChild(xnDocumento);
                //    }

                //    //
                //    // Aggiorna i dati delle pagine.
                //    //
                //    xnDocumento.SelectSingleNode("totali").InnerText = k.Value.PagineDocumento.ToString();
                //    cPagineparziali = "";
                //    foreach (int nPagine in k.Value.PagineSubDocumenti)
                //        cPagineparziali = String.Concat(cPagineparziali, nPagine, ";");
                //    k.Value.GiaEseguitaAnteprima = true;
                //    xnDocumento.SelectSingleNode("parzia").InnerText = cPagineparziali.Substring(0, cPagineparziali.Length - 1);
                //}
                _xdFastPrint.Save(GSS.Files["FastPrintFIL"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PredisponiStampanteOttica(MST_enumeratori.ePDFCreatorFileType asFormat)
        {
            // Dim cFileName As String

            //'If GPS.ArchiviazioneOttica Then
            //'cFileName = GMS.DatiPerStampa.ArchiviazioneOtticaFN
            //'cFileName = GestAttStampa.saTHRGS.DatiStampa.ArchiviazioneOtticaFN
            //'
            //' Rimuovi il documento elettronico generato in precedenza
            //'

            if (_prnPDF == null)
            {
                _prnPDF = new CLS_PDFCreator("PDFCreator");
                _prnPDF.AutosaveFormat = asFormat;
            }
            this.saTHRGS.DatiGruppo.resetNomeFileArchiviazioneOttica();
            _prnPDF.AutosaveDirectory = System.IO.Path.GetDirectoryName(this.saTHRGS.DatiGruppo.ArchiviazioneOtticaFN);

            //
            // Predisponi la stampante ottica con il nome file corretto.
            //
            _prnPDF.AutosaveFilename = System.IO.Path.GetFileName(this.saTHRGS.DatiGruppo.ArchiviazioneOtticaFN);
            _prnPDF.AttivaStampante();
            //
            // Aggiungo il nome del file all'elenco dei file da fondere eventualmente
            //
            //If (GPS.FondiModuli) Then
            //    DatiFusione.DocumentiDaFondere.Add(GestAttStampa.saTHRGS.DatiStampa.ArchiviazioneOtticaFN)
            //End If


        }

        private void THRFondiDocumenti()
        {
            OggettiGlobali.loggerTHRFonDoc.Info("INIZIO - Avvio del THREAD per la fusione dei documenti.");
            while (this.THRFondiDocumentiRunning)
            {
                if (this.GRP_PreAttesaFusione_Numero > 1)
                {
                    OggettiGlobali.loggerTHRFonDoc.Info("INIZIO - Ci sono almeno due gruppi in attesa di fusione. Verifico che siano gli X necessari per la fusione.");
                    PreAttesaFusioneDocumenti();
                }
                else if ((this.GRP_AttesaFusione_Numero > 0) && (this.GRP_GeneraFusione_Numero == 0))
                    lock (lockAttivita)
                        this.lisgrp_AttesaFusione.First().Value.Stato = eStatoGruppo.STR_genera_fusione;
                else if (this.GRP_GeneraFusione_Numero == 1)
                {
                   // OggettiGlobali.loggerTHRFonDoc.Debug("  > Generazione anteprima per base date per il record codice: " + this.saTHRFD.Chiave.ToString() + " modulo: " + this.saTHRFD.CodiceModulo.ToString());
                    FusioneDocumentiTHR();
                    lock (lockAttivita)
                    {
                      //  this.saTHRFD.Stato = eStatoGruppo.STR_fine;
                    }
                }
                else
                {
                    OggettiGlobali.loggerTHRFonDoc.Debug("  > Generazione anteprime non necessaria. Attendo.");
                    Thread.Sleep(1000);
                }
            }
            OggettiGlobali.loggerTHRFonDoc.Info("FINE - Termine del THREAD per la fusione dei documenti.");
        }

        private void PreAttesaFusioneDocumenti()
        {
            GruppoPerStampa sgb1;
            List<GruppoBase> lstGruppi;
            String cFilename;

            OggettiGlobali.loggerTHRFonDoc.Info("INIZIO - Avvio del THREAD per le attività di pre-fusione.");
            lock (lockAttivita)
            {
                //
                // Sposto il nome del file pdf generato nella lista dei documenti.
                //
                //foreach (StrutturaGruppoBase sgb2 in lisgrp_PreAttesaFusione.Values)
                //{
                //    sgb1 = (StrutturaGruppoStampa)sgb2;
                //    cFilename = "";
                //    if (!sgb1.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                //        cFilename = sgb1.DatiGruppo.ArchiviazioneOtticaFN;
                //    if (cFilename != "" && !sgb1.ListaDocumentiDaFondere.Contains(cFilename))
                //        sgb1.ListaDocumentiDaFondere.Add(cFilename);
                //}

                ////
                //// Recupero tutte le 
                ////
                //foreach (StrutturaGruppoBase sgb2 in lisgrp_PreAttesaFusione.Values.Where(v => ((StrutturaGruppoStampa)v).Raccolta == 1 || ((StrutturaGruppoStampa)v).Raccolta == 9))
                //{
                //    lstGruppi = lisgrp_PreAttesaFusione.Values.Where(v => ((StrutturaGruppoStampa)v).ItemToken == ((StrutturaGruppoStampa)sgb2).ItemToken).OrderBy(v => ((StrutturaGruppoStampa)v).Raccolta).ToList();
                //    if ((((StrutturaGruppoStampa)lstGruppi.First()).Raccolta == 1) && (((StrutturaGruppoStampa)lstGruppi.Last()).Raccolta == 3))
                //    {
                //        foreach (StrutturaGruppoBase sgb3 in lstGruppi)
                //        {
                //            if (sgb3.Chiave != sgb2.Chiave)
                //            {
                //                ((StrutturaGruppoStampa)sgb2).ListaDocumentiDaFondere = ((StrutturaGruppoStampa)sgb2).ListaDocumentiDaFondere.Concat(((StrutturaGruppoStampa)sgb3).ListaDocumentiDaFondere).ToList();
                //                sgb3.Stato = eStatoGruppo.STR_fine;
                //            }
                //        }
                //        sgb2.Stato = eStatoGruppo.STR_attesa_fusione;
                //    }
                //    else if (((StrutturaGruppoStampa)lstGruppi.First()).Raccolta == 9)
                //    {
                //        sgb2.Stato = eStatoGruppo.STR_attesa_fusione;
                //    }
                //}
            }
            OggettiGlobali.loggerTHRFonDoc.Info("FINE - Termine del THREAD per le attività di pre-fusione.");
        }

        private void FusioneDocumentiTHR()
        {
            CLS_motore_fusione Fusione;
            CLS_DatiPerFusione DatiFusione;

            try
            {
                DatiFusione = new CLS_DatiPerFusione();
                DatiFusione.SOrientamento = GSS.ModuloUnificato.Orientamento;
                DatiFusione.Rotation = GSS.ModuloUnificato.Rotazione;
                DatiFusione.PaginePariDocumentoGlobale = GSS.ModuloUnificato.PaginePariDocumentoGlobale;
                DatiFusione.PaginePariSingoloDocumento = GSS.ModuloUnificato.PaginePariSingoloDocumento;
                DatiFusione.DocumentiDaFondere = saTHRFD.ListaDocumentiDaFondere;
                DatiFusione.NomeDocumentoFuso = saTHRFD.DatiGruppo.DocumentoFusoFN;
                Fusione = new CLS_motore_fusione();
                Fusione.DatiFusione = DatiFusione;
                Fusione.FondiDocumenti();
            }
            catch (Exception ex)
            {

            }
        }

        private void CancellaDocumentoGenerare(String cFileName)
        {
            Boolean lDeleted;
            String cTmp;

            lDeleted = false;
            FileDeletingLocked = false;
            OggettiGlobali.logger.Info("INIZIO - Cancellazione file");
            OggettiGlobali.logger.Info("File da cancellare: " + cFileName);
            //cTmp = String.Concat("_", DatiMaster.SuffissoParte, "_");
            while (!lDeleted && !TerminaStampa)
            {
                try
                {
                    //
                    // Rimuove i file singoli.
                    //
                    foreach (string f in System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(cFileName), System.IO.Path.GetFileName(cFileName)))
                    {
                        OggettiGlobali.logger.Info("Cancellazione del file: " + f);
                        FileLockedFN = f;
                        System.IO.File.Delete(f);
                    }

                    //
                    // Rimuovo eventuali parti.
                    //
                    // OggettiGlobali.logger.Info("Cancellazione dei file: " + cTmp);
                    //if (cFileName.Contains(cTmp))
                    //    cFileName = cFileName.Remove(cFileName.IndexOf(cTmp), cTmp.Length + 3);

                    cFileName = ""; //cFileName.Replace(".pdf", cTmp + "*.pdf");
                    if (!cFileName.Contains("*"))
                        cFileName = cFileName.Replace(".pdf", "*.pdf");

                    OggettiGlobali.logger.Info("Cancellazione dei file: " + cFileName);
                    foreach (string f in System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(cFileName), System.IO.Path.GetFileName(cFileName)))
                    {
                        OggettiGlobali.logger.Info("Cancellazione del file: " + f);
                        FileLockedFN = f;
                        System.IO.File.Delete(f);
                    }
                    lDeleted = true;
                    FileLockedFN = "";
                }
                catch (Exception ex)
                {
                    FileDeletingLocked = true;
                    //if MessageBox.Show(String.Concat("Attenzione il file """, cFileName, """ è aperto da un'altra applicazione. Attendo la chiusura del file?"), "File bloccato", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
                    // ForcedExit = True
                    //End If
                }
            }
            OggettiGlobali.logger.Info("FINE - Cancellazionne file");
        }

        public Boolean FileDeletingLocked { get; set; }
        public String FileLockedFN { get; set; }
    }

    //public override string ChiaveGruppo
    //{
    //    get
    //    {
    //        throw new NotImplementedException();
    //    }
    //    set
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    //public decimal RecordDaStampare_Primo { get; set; }
    //protected System.Threading.Thread _ThreadCaricaDatabase;
    //protected System.Threading.Thread _ThreadGeneraAnteprima;
    //protected System.Threading.Thread _ThreadGeneraStampa;
    //protected System.Threading.Thread _ThreadFondiDocumenti;

    //public Boolean ForcedExit { get; set; }

    // probabilmente inutili
    //**********************
    // public Boolean Abilitata_FastPrint { get; set; }
    // public Boolean Forza_StampaCartacea1 { get; set; }
    //**********************
}
