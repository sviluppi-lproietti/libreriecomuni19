﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Base;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;

namespace ISC.Invoicer40.ProcessiOLD.Base
{
    public abstract class ProcessoBase
    {
        //public abstract string ChiaveGruppo { get; set; }

        public Dictionary<decimal,  GruppoBase> lisgrp_Globale { get; set; }
        public Dictionary<decimal, GruppoBase> lisgrp_Creati
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_creato).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        public Dictionary<decimal, GruppoBase> lisgrp_Caricati
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_caricato).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        public Dictionary<decimal, GruppoBase> lisgrp_Terminati
        {
            get
            {
                return lisgrp_Globale.Where(v => v.Value.Stato == eStatoGruppo.STR_fine).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public int MaxGruppiGestiti { get { return 10; } }

        private System.Object lockGestioneGruppi = new System.Object();

        public ProcessoBase()
        {
            lisgrp_Globale = new Dictionary<decimal, GruppoBase>();
        }
        
        public void AggiungiStrutturaAttivita(GruppoBase sg)
        {
            lock (lockGestioneGruppi)
            {
                lisgrp_Globale.Add(sg.Chiave, sg);
                lisgrp_Globale = lisgrp_Globale.OrderBy(k => k.Key).ToDictionary(k => k.Key, v => v.Value);
            }
        }

        public void RimuoviStrutturaAttivita(decimal nkey)
        {
            lock (lockGestioneGruppi)
            {
                lisgrp_Globale.Remove(nkey);
            }
        }
    }
}
