﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace ISC.InvoicerBase.Varie
{
    public class Utilita
    {
        internal static string HashValue(string cValue, string cHashType = "md5", string cFormat = "X2")
        {
            byte[] byteHashValue;
            string cRtn;

            cHashType = cHashType.ToLower();
            byteHashValue = null;
            if (cHashType == "md5")
            {
                MD5CryptoServiceProvider md5;
                md5 = new MD5CryptoServiceProvider();
                md5.ComputeHash((new UnicodeEncoding()).GetBytes(cValue));
                byteHashValue = md5.Hash;
            }
            else
            {
                throw new Exception("Metodo di hashing sconosciuto: " + cHashType);
            }

            if (byteHashValue != null)
            {
                if (cFormat == "")
                    cRtn = BitConverter.ToString(byteHashValue);
                else
                {
                    StringBuilder hash;
                    hash = new StringBuilder(byteHashValue.Length);
                    for (int i = 0; i < byteHashValue.Length; i++)
                        hash.Append(byteHashValue[i].ToString(cFormat));
                    cRtn = hash.ToString();
                }
            }
            else
                cRtn = "";
            
            return cRtn;
        }
    }
}
