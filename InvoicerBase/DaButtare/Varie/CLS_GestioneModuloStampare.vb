﻿Imports ISC.LibrerieComuni.GestioneAttivita
Imports ISC.LibrerieComuni.InvoicerDomain.Impl
Imports ISC.Invoicer.ManageStampa
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni
Imports System.Linq

Public Class CLS_GestioneModuloStampare

    Public Property BarcodeCodImbX As Decimal
    Public Property BarcodeCodImbY As Decimal

    ''' <summary>
    ''' Contiene l'elenco dei cassetti della stampante selezionata.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CassettiStampante As Dictionary(Of Integer, CassettoEntity)
    Public Property CassettiModuli As Dictionary(Of Integer, CassettiPagineEntity)

    '
    ' Indica se il modulo è da stampare o meno
    '
    Public Property DaStampare As Boolean
    Public Property DatiPerStampa As DatiPerStampa

    '
    '
    '
    Public Property DescrizionePossoProcedere As String

    Public ReadOnly Property ListaCassettiModuli As List(Of CassettiPagineEntity)

        Get
            Dim l As List(Of CassettiPagineEntity)

            l = New List(Of CassettiPagineEntity)
            For Each k As KeyValuePair(Of Integer, CassettiPagineEntity) In CassettiModuli
                l.Add(k.Value)
            Next
            Return l
        End Get

    End Property

    Public ReadOnly Property ListaCassetti As List(Of CassettoEntity)

        Get
            Dim l As List(Of CassettoEntity)

            l = New List(Of CassettoEntity)
            For Each k As KeyValuePair(Of Integer, CassettoEntity) In CassettiStampante
                l.Add(k.Value)
            Next
            Return l
        End Get

    End Property

    Public Property ArchiviazioneOttica() As Boolean

        Get
            Return DatiPerStampa.ArchiviazioneOttica
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.ArchiviazioneOttica = value
        End Set

    End Property

    Public ReadOnly Property ModuloSenzaSelezioneNecessaria As Boolean

        Get
            Return Modulo.ModuloSenzaSelezioneNecessaria
        End Get

    End Property

    Public ReadOnly Property CanPrintPDF As Boolean

        Get
            Return Modulo.DatiModulo.CanPrintPDF
        End Get

    End Property

    Public Property CassettoDefault As CassettoEntity

    Public ReadOnly Property StampaConDettaglio As Boolean

        Get
            Return Modulo.ConDettaglio
        End Get

    End Property


    Public ReadOnly Property EsistonoPagineGenerare As Boolean

        Get
            Return Not (Modulo.AllegatoDaDividere = "")
        End Get

    End Property

    Public Property FattoreCorrezione_Cartaceo As FattoreCorrezioneEntity

        Get
            Return Modulo.FattoreCorrezione_Cartaceo
        End Get
        Set(ByVal value As FattoreCorrezioneEntity)
            Modulo.FattoreCorrezione_Cartaceo = value
        End Set

    End Property

    Public Property FattoreCorrezione_Ottico As FattoreCorrezioneEntity

        Get
            Return Modulo.FattoreCorrezione_Ottico
        End Get
        Set(ByVal value As FattoreCorrezioneEntity)
            Modulo.FattoreCorrezione_Ottico = value
        End Set

    End Property

    Public Property RendiPaginePari As Boolean

        Get
            Return DatiPerStampa.RendiPaginePari
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.RendiPaginePari = value
        End Set

    End Property

    Public Property StampaForzataDettaglio As Boolean

    Public Property Folders As CLS_FileFolderDic

        Get
            Return DatiPerStampa.Folders
        End Get
        Set(ByVal value As CLS_FileFolderDic)
            DatiPerStampa.Folders = value
        End Set

    End Property

    Public Property FronteRetro As Boolean

        Get
            Return DatiPerStampa.FronteRetro
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.FronteRetro = value
            AssociaPagineCassetti()
        End Set

    End Property

    Public Property InserisciFincatura As Boolean

        Get
            Return DatiPerStampa.InserisciFincatura
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.InserisciFincatura = value
        End Set

    End Property

    Public Property ListaCassettiUtilizzati As Dictionary(Of Integer, CassettoEntity)

        Get
            Return DatiPerStampa.ListaCassettiUtilizzati
        End Get
        Set(ByVal value As Dictionary(Of Integer, CassettoEntity))
            DatiPerStampa.ListaCassettiUtilizzati = value
        End Set

    End Property

    Public Property Modulo As GestioneModulo
    Public Property NomeModulo As String
    Public Property CodiceModulo As Integer

    '
    ' Indica il nome della stampante con la quale si stamperà il modulo.
    '
    Public Property NomeStampanteCartacea As String
    Public Property NomeStampanteOttica As String
    Public Property NoSplitAllegati() As Boolean

    Public Property ProcedoConStampa As Boolean
    Public Property ResetWrapRound As Boolean

    Public Property StampaEseguita() As Boolean

        Get
            Return DatiPerStampa.StampaEseguita
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.StampaEseguita = value
        End Set

    End Property

    Public Property SuffissoParte As String

        Get
            Return DatiPerStampa.SuffissoParte
        End Get
        Set(ByVal value As String)
            DatiPerStampa.SuffissoParte = value
        End Set

    End Property


    Public Property WrapRound() As Decimal

        Get
            Return DatiPerStampa.WrapRound
        End Get
        Set(ByVal value As Decimal)
            DatiPerStampa.WrapRound = value
        End Set

    End Property

    Public Sub New(ByVal nCodice As Integer)

        CodiceModulo = nCodice
        DatiPerStampa = New DatiPerStampa
        ListaCassettiUtilizzati = New Dictionary(Of Integer, CassettoEntity)

    End Sub

    Public Sub GetDatiStampante()
        Dim PSource As System.Drawing.Printing.PaperSource
        Dim pd As System.Drawing.Printing.PrintDocument

        Modulo.NomeStampanteCartacea = NomeStampanteCartacea
        pd = New System.Drawing.Printing.PrintDocument
        pd.PrinterSettings.PrinterName = Me.NomeStampanteCartacea
        CassettiStampante = New Dictionary(Of Integer, CassettoEntity)
        CassettiStampante.Add(CassettiStampante.Count, New CassettoEntity(0, "0", 0, "----- Seleziona un cassetto -----"))

        For Each PSource In pd.PrinterSettings.PaperSources
            If (PSource.Kind = Drawing.Printing.PaperSourceKind.AutomaticFeed) Or (PSource.Kind = Drawing.Printing.PaperSourceKind.Middle) Or
                (PSource.Kind = Drawing.Printing.PaperSourceKind.Upper) Or (PSource.Kind = Drawing.Printing.PaperSourceKind.Lower) Or
                (PSource.Kind = Drawing.Printing.PaperSourceKind.FormSource) Or (PSource.Kind = Drawing.Printing.PaperSourceKind.Custom) Then
                CassettiStampante.Add(CassettiStampante.Count, New CassettoEntity(CassettiStampante.Count, PSource.Kind, PSource.RawKind, PSource.SourceName))
                If pd.PrinterSettings.DefaultPageSettings.PaperSource.RawKind = PSource.RawKind Then
                    _CassettoDefault = CassettiStampante(CassettiStampante.Count - 1)
                End If
            End If
        Next
        If CassettiStampante.Count = 1 Then
            CassettiStampante.Add(CassettiStampante.Count, New CassettoEntity(CassettiStampante.Count, -1, 0, "Cassetto di default"))
            _CassettoDefault = CassettiStampante(CassettiStampante.Count - 1)
        End If

    End Sub

    '
    ' Prepara la il modulo per la stampa.
    '
    Public Sub PreparaPerStampa()

        If _ResetWrapRound Then
            DatiPerStampa.WrapRound = 1
        Else
            DatiPerStampa.WrapRound = Int(DatiPerStampa.WrapRound)
        End If
        Modulo.PreparaperStampa()

    End Sub

    ' ********************************************************************* '
    ' Predisposizione del dataset per l'associazione del modulo al cassetto ' 
    ' ********************************************************************* '
    Public Sub AssociaPagineCassetti()
        Dim cas As List(Of CassettiPagineEntity)
        Dim nTray As Integer

        CassettiModuli = New Dictionary(Of Integer, CassettiPagineEntity)
        For Each aValue As String() In Me.Modulo.RecuperaModuliNecessari
            nTray = aValue(0).Replace("*", "")

            '
            ' Ricerco se il cassetto è già presente nella lista.
            '
            cas = (From v In CassettiModuli.Values Where v.CAP_pagina = nTray).ToList

            If (cas.Count = 0) Then
                CassettiModuli.Add(CassettiModuli.Count, New CassettiPagineEntity(CassettiModuli.Count, nTray, aValue(1), Me.CassettoDefault.PRT_codice))
            End If
        Next

    End Sub

    Public Sub VerificaProcedoConStampa(ByVal lPrnOttica As Boolean)

        ProcedoConStampa = True
        If DaStampare Then
            If lPrnOttica Then
                '
                ' Se la stampa è ottica allora verifico che sia presente almeno un tipo di nome relativo al file 
                '
                ProcedoConStampa = Modulo.NomeFileArchivioOtticoSelect IsNot Nothing
                If Not ProcedoConStampa Then
                    DescrizionePossoProcedere = "Errore, occorre selezionare almeno un nome per i file PDF."
                End If
            Else
                For Each k As KeyValuePair(Of Integer, CassettoEntity) In ListaCassettiUtilizzati
                    ProcedoConStampa = ProcedoConStampa And (k.Value.PRT_codice <> 0)
                Next
                If Not ProcedoConStampa Then
                    DescrizionePossoProcedere = "Errore, non è stato selezionato un cassetto da dove prelevare i fogli."
                End If
            End If
            If ProcedoConStampa Then
                DescrizionePossoProcedere = "Posso procedere con la stampa del presente modulo."
            End If
        Else
            DescrizionePossoProcedere = "Il presente modulo non è selezionato."
        End If

    End Sub

    Public Sub Imposta_cassettiUtilizzati(ByVal lDefault As Boolean, ByVal DefTray As CassettoEntity)

        Me.ListaCassettiUtilizzati = New Dictionary(Of Integer, CassettoEntity)
        If lDefault Then
            Me.ListaCassettiUtilizzati.Add(-999, DefTray)
        Else
            For Each c As CassettiPagineEntity In Me.ListaCassettiModuli
                Me.ListaCassettiUtilizzati.Add(c.CAP_pagina, ListaCassetti(c.CAP_cassetto))
            Next
        End If

    End Sub

End Class
