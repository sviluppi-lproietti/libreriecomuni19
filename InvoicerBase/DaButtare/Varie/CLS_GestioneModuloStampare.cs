﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ISC.LibrerieComuni.GestioneAttivita;
using ISC.LibrerieComuni.InvoicerDomain.Entity;
//using ISC.Invoicer.ManageStampa;
using ISC.LibrerieComuni.ManageSessione;
using ISC.LibrerieComuni.OggettiComuni;
//using ISC.Invoicer40.Dati.Impl;
using ISC.LibrerieComuni.InvoicerService.Dati.Impl;
using ISC.LibrerieComuni.InvoicerTran.Modulo;

namespace ISC.Invoicer40.Varie
{
    public class CLS_GestioneModuloStampare
    {
        public Decimal BarcodeCodImbX { get; set; }
        public Decimal BarcodeCodImbY { get; set; }

        public Dictionary<int, CassettoEntity> CassettiStampante { get; set; }
        public Dictionary<int, CassettiPagineEntity> CassettiModuli { get; set; }

        //
        // Indica se il modulo è da stampare o meno
        //
        public Boolean DaStampare { get; set; }
        public DatiStampaDettaglio DatiPerStampa { get; set; }

        //
        //
        //
        public String DescrizionePossoProcedere { get; set; }

        public List<CassettiPagineEntity> ListaCassettiModuli
        {
            get
            {
                List<CassettiPagineEntity> l;

                l = new List<CassettiPagineEntity>();
                foreach (KeyValuePair<int, CassettiPagineEntity> k in CassettiModuli)
                    l.Add(k.Value);
                return l;
            }
        }

        public List<CassettoEntity> ListaCassetti
        {

            get
            {
                List<CassettoEntity> l;

                l = new List<CassettoEntity>();
                foreach (KeyValuePair<int, CassettoEntity> k in CassettiStampante)
                    l.Add(k.Value);
                return l;
            }
        }

        //public Boolean ArchiviazioneOttica1
        //{
        //    get
        //    {
        //        return DatiPerStampa.ArchiviazioneOttica;
        //    }
        //    set
        //    {
        //        DatiPerStampa.ArchiviazioneOttica = value;
        //    }

        //}

        public Boolean ModuloSenzaSelezioneNecessaria
        {
            get
            {
                return Modulo.ModuloSenzaSelezioneNecessaria;
            }
        }

        public Boolean CanPrintPDF
        {

            get
            {
                return Modulo.DatiModulo.CanPrintPDF;
            }
        }

        public CassettoEntity CassettoDefault { get; set; }

        public Boolean StampaConDettaglio
        {
            get
            {
                return Modulo.ConDettaglio;
            }

        }

        public Boolean EsistonoPagineGenerare
        {
            get
            {
                return (Modulo.AllegatoDaDividere != "");
            }

        }

        public FattoreCorrezioneEntity FattoreCorrezione_Cartaceo
        {
            get
            {
                return Modulo.FattoreCorrezione_Cartaceo;
            }
            set
            {
                Modulo.FattoreCorrezione_Cartaceo = value;
            }

        }

        public FattoreCorrezioneEntity FattoreCorrezione_Ottico
        {
            get
            {
                return Modulo.FattoreCorrezione_Ottico;
            }
            set
            {
                Modulo.FattoreCorrezione_Ottico = value;
            }
        }

        public Boolean RendiPaginePari
        {

            get
            {
                return DatiPerStampa.RendiPaginePari;
            }
            set
            {
                DatiPerStampa.RendiPaginePari = value;
            }

        }

        public Boolean StampaForzataDettaglio { get; set; }

        public CLS_FileFolderDic Folders
        {
            get
            {
                return DatiPerStampa.Folders;
            }
            set
            {
                DatiPerStampa.Folders = value;
            }

        }

        public Boolean FronteRetro
        {
            get
            {
                return DatiPerStampa.FronteRetro;
            }
            set
            {
                DatiPerStampa.FronteRetro = value;
                AssociaPagineCassetti();
            }
        }

        public Boolean InserisciFincatura
        {
            get
            {
                return DatiPerStampa.InserisciFincatura;
            }
            set
            {
                DatiPerStampa.InserisciFincatura = value;
            }
        }
        public Dictionary<int, CassettoEntity> ListaCassettiUtilizzati
        {

            get
            {
                return DatiPerStampa.ListaCassettiUtilizzati;
            }
            set
            {
                DatiPerStampa.ListaCassettiUtilizzati = value;
            }

        }

        public GestioneModulo Modulo { get; set; }
        public String NomeModulo { get; set; }
        public int CodiceModulo { get; set; }

        //
        // Indica il nome della stampante con la quale si stamperà il modulo.
        //
        public String NomeStampanteCartacea { get; set; }
        public String NomeStampanteOttica { get; set; }
        public Boolean NoSplitAllegati { get; set; }

        public Boolean ProcedoConStampa { get; set; }
        public Boolean ResetWrapRound { get; set; }

        public Boolean StampaEseguita
        {
            get
            {
                return DatiPerStampa.StampaEseguita;
            }
            set
            {
                DatiPerStampa.StampaEseguita = value;
            }

        }

        public Decimal WrapRound
        {

            get
            {
                return DatiPerStampa.WrapRound;
            }
            set
            {
                DatiPerStampa.WrapRound = value;
            }
        }

        public CLS_GestioneModuloStampare(int nCodice)
        {
            CodiceModulo = nCodice;
            DatiPerStampa = new DatiStampaDettaglio();
            ListaCassettiUtilizzati = new Dictionary<int, CassettoEntity>();

        }

        public void GetDatiStampante()
        {
            //System.Drawing.Printing.PaperSource PSource;
            System.Drawing.Printing.PrintDocument pd;

            Modulo.NomeStampanteCartacea = NomeStampanteCartacea;
            pd = new System.Drawing.Printing.PrintDocument();
            pd.PrinterSettings.PrinterName = this.NomeStampanteCartacea;
            CassettiStampante = new Dictionary<int, CassettoEntity>();
            CassettiStampante.Add(CassettiStampante.Count, new CassettoEntity(0, "0", 0, "----- Seleziona un cassetto -----"));

            foreach (System.Drawing.Printing.PaperSource PSource in pd.PrinterSettings.PaperSources)
            {
                if ((PSource.Kind == System.Drawing.Printing.PaperSourceKind.AutomaticFeed) || (PSource.Kind == System.Drawing.Printing.PaperSourceKind.Middle) ||
                    (PSource.Kind == System.Drawing.Printing.PaperSourceKind.Upper) || (PSource.Kind == System.Drawing.Printing.PaperSourceKind.Lower) ||
                    (PSource.Kind == System.Drawing.Printing.PaperSourceKind.FormSource) || (PSource.Kind == System.Drawing.Printing.PaperSourceKind.Custom))
                {
                    CassettiStampante.Add(CassettiStampante.Count, new CassettoEntity(CassettiStampante.Count, PSource.Kind.ToString(), PSource.RawKind, PSource.SourceName));
                    if (pd.PrinterSettings.DefaultPageSettings.PaperSource.RawKind == PSource.RawKind)
                        CassettoDefault = CassettiStampante[CassettiStampante.Count - 1];
                }
            }
            if (CassettiStampante.Count == 1)
            {
                CassettiStampante.Add(CassettiStampante.Count, new CassettoEntity(CassettiStampante.Count, "-1", 0, "Cassetto di default"));
                CassettoDefault = CassettiStampante[CassettiStampante.Count - 1];
            }

        }

        //
        // Prepara la il modulo per la stampa.
        //
        public void PreparaPerStampa()
        {
            if (ResetWrapRound)
                DatiPerStampa.WrapRound = 1;
            else
                DatiPerStampa.WrapRound = Math.Truncate(DatiPerStampa.WrapRound);
            Modulo.PreparaperStampa();
        }

        // ********************************************************************* //
        // Predisposizione del dataset per l'associazione del modulo al cassetto //
        // ********************************************************************* //
        public void AssociaPagineCassetti()
        {
            List<CassettiPagineEntity> cas;
            int nTray;

            CassettiModuli = new Dictionary<int, CassettiPagineEntity>();
            foreach (string[] aValue in this.Modulo.RecuperaModuliNecessari())
            {
                nTray = int.Parse(aValue[0].Replace("*", ""));

                //
                // Ricerco se il cassetto è già presente nella lista.
                //
                cas = CassettiModuli.Values.Where(k => k.CAP_pagina == nTray).ToList();

                if (cas.Count == 0)
                    CassettiModuli.Add(CassettiModuli.Count, new CassettiPagineEntity(CassettiModuli.Count, nTray, aValue[1], this.CassettoDefault.PRT_codice));
            }
        }

        public void VerificaProcedoConStampa(Boolean lPrnOttica)
        {
            ProcedoConStampa = true;
            if (DaStampare)
            {
                if (lPrnOttica)
                {    //
                    // Se la stampa è ottica allora verifico che sia presente almeno un tipo di nome relativo al file 
                    //
                    ProcedoConStampa = (Modulo.NomeFileArchivioOtticoSelect != null);
                    if (!ProcedoConStampa)
                        DescrizionePossoProcedere = "Errore, occorre selezionare almeno un nome per i file PDF.";

                }
                else
                {
                    foreach (KeyValuePair<int, CassettoEntity> k in ListaCassettiUtilizzati)
                        ProcedoConStampa = ProcedoConStampa && (k.Value.PRT_codice != 0);
                    if (!ProcedoConStampa)
                        DescrizionePossoProcedere = "Errore, non è stato selezionato un cassetto da dove prelevare i fogli.";

                }
                if (ProcedoConStampa)
                    DescrizionePossoProcedere = "Posso procedere con la stampa del presente modulo.";
            }
            else
                DescrizionePossoProcedere = "Il presente modulo non è selezionato.";


        }
        public void Imposta_cassettiUtilizzati(Boolean lDefault, CassettoEntity DefTray)
        {
            this.ListaCassettiUtilizzati = new Dictionary<int, CassettoEntity>();
            if (lDefault)
                this.ListaCassettiUtilizzati.Add(-999, DefTray);
            else
            {
                foreach (CassettiPagineEntity c in this.ListaCassettiModuli)
                    this.ListaCassettiUtilizzati.Add(c.CAP_pagina, ListaCassetti[c.CAP_cassetto]);
            }
        }

        public CLS_GestioneModuloStampare Clone()
        {
            return (CLS_GestioneModuloStampare)this.MemberwiseClone();
        }
    }
}
