﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;

namespace ISC.InvoicerBase.Classi
{
    public class CustomSmtpAppender : SmtpAppender
    {
        public PatternLayout SubjectLayout { get; set; }

        protected override void SendBuffer(LoggingEvent[] events)
        {
            PrepareSubject(events);
            base.SendBuffer(events);
        }

        protected virtual void PrepareSubject(IEnumerable<LoggingEvent> events)
        {
            Subject = string.Empty;

            foreach (LoggingEvent @event in events)
            {
                if (Evaluator.IsTriggeringEvent(@event))
                    Subject += SubjectLayout.Format(@event);
            }

            if (string.IsNullOrEmpty(Subject))
                throw new InvalidOperationException("Errore impostando l'oggetto della mail.");
        }
    }
}
