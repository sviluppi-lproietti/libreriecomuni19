﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.InvoicerBase.Classi
{
    public static class CLS_icone
    {
        public static System.Drawing.Bitmap IconaFolder(int nSize)
        {
            if (nSize == 16)
                return Properties.Resources.Folder_16;
            else
                return null;
        }

        public static System.Drawing.Bitmap IconaMailSettings(int nSize)
        {
            if (nSize == 16)
                return Properties.Resources.MailSetting_16;
            else
                return null;
        }
    }
}
