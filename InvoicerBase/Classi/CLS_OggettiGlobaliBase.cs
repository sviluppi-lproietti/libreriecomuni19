﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.LibrerieComuni.InvoicerDomainBase.Entity;

namespace ISC.InvoicerBase.Classi
{
    public abstract class CLS_OggettiGlobaliBase
    {

        //
        // Proprietà principali
        //
        public static UtentiEntity UtenteLoggato { get; set; }
        public static CLS_BaseConfig CFG { get; set; }

        public static log4net.ILog Applicationlogger;

        //
        // Proprietà derivate da quelle precedenti
        //
        public static CLS_FileFolderDic Folders
        {
            get
            {
                return CFG.Folders;
            }
            set
            {
                CFG.Folders = value;
            }
        }

        public static string ApplicationName
        {
            get
            {
                var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
                var applicationTitle = ((System.Reflection.AssemblyTitleAttribute)entryAssembly.GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false)[0]).Title;
                if (string.IsNullOrWhiteSpace(applicationTitle))
                {
                    applicationTitle = entryAssembly.GetName().Name;
                }
                return applicationTitle;
            }
        }

        public static string ApplicationVersion
        {
            get
            {
                var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();

                var rtnValue = entryAssembly.GetName().Version.Major.ToString("00") + ".";
                rtnValue += entryAssembly.GetName().Version.Minor.ToString("00") + ".";
                rtnValue += entryAssembly.GetName().Version.Build.ToString("00") + ".";
                rtnValue += entryAssembly.GetName().Version.Revision.ToString("00");
                return rtnValue;
            }
        }

        public static void SetUpApplicationLogger()
        {
            //
            log4net.GlobalContext.Properties["ApplNome"] = ApplicationName;
            log4net.GlobalContext.Properties["ApplVers"] = ApplicationVersion;
            System.IO.FileInfo logfile = new System.IO.FileInfo("cfg/log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(logfile);
            Applicationlogger = LogManager.GetLogger("ApplicationLogger");
        }

    }
}
