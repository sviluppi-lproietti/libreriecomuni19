﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ISC.LibrerieComuni.OggettiComuni;

namespace ISC.InvoicerBase.Classi
{
    
    public abstract class CLS_ConfigBase
    {
        public Boolean ApplicazioneAvviabile { get; set; }
        public OGC_enumeratori.etipoBitOS BitOs { get; set; }
        public CLS_FileFolderDic Files { get; set; }
        public CLS_FileFolderDic Folders { get; set; }
        public Boolean IsLoaded { get; set; }
        public ArrayList ListaFileNecessari { get; set; }
        private Dictionary< String, String> _OleDBConnStrs { get; set; }

    //Imports Microsoft.Win32
   // Imports log4net

    //    ReadOnly Property VersioneSistemaOperativo() As String

    //        Get
    //            Return String.Concat("Windows ", Me.OSVersion)
    //        End Get

    //    End Property

    
    //    Public Property OLEDB_ConnString As Dictionary(Of String, CLS_oledbconnstring)
    //    Public Property OSVersion As eTipoSistemaOperativo
    //    Public Property PlugIn_1_0_0_loadable As Boolean
    //    Public Property PlugIn_1_1_0_loadable As Boolean
    //    Public Property PlugIn_1_2_0_loadable As Boolean
    //    Public Property PlugIn_1_3_0_loadable As Boolean
    //    Public Property PlugIn_1_3_1_loadable As Boolean
    //    Public Property PlugIn_1_3_2_loadable As Boolean
    //    Public Property PlugIn_1_4_0_loadable As Boolean
    //    Public Property PlugIn_1_4_1_loadable As Boolean
    //    Public Property TipoAmbiente As eTipoApplicazione

    //    ' ************************************************************************* '
    //    ' La classe gestisce la configurazione dell'applicazione:                   '
    //    ' - carica i parametri di funzionamento dal file di configurazione;         '
    //    ' - controllando la presenza dei componenti necessari;                      '
    //    ' - fornendo i valori che vengono richiesti durante l'utilizzo dell'applica '
    //    '   zione.                                                                  '
    //    ' ************************************************************************* '


    //    ' ************************************************************************* '
    //    ' Carica il file di configurazione se presente altrimenti predispone un do- '
    //    ' cumento vuoto per il salvataggio dei dati di configurazione. Inoltre leg- '
    //    ' ge lo switch per determinare se l'applicazione è libera o vincolata.      '
    //    ' ************************************************************************* '
    //    Public Sub New(ByVal nTipoAmbiente As eTipoApplicazione, ByVal cRootApplicationFLD As String, ByVal cCFGFileName As String)

    //        Try
    //            TipoAmbiente = nTipoAmbiente

    //            '
    //            ' Popola il dizionario dei folder 
    //            '
    //            Folders = New CLS_FileFolderDic()
    //            Folders.Add("RootApplicationFLD", StandardFolderValue(cRootApplicationFLD))
    //            Folders.Add("ExchangeFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "Exchange"))
    //            Folders.Add("TempFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "Temp"))
    //            Folders.Add("LogsFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "Logs"))
    //            Folders.Add("SessioniDefaultFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "SessioniDefault"))

    //            '
    //            ' Popola il dizionario dei nomi dei file
    //            '
    //            Files = New CLS_FileFolderDic
    //            Files.Add("ConfigurazioneFIL", ConcatenaFolderFileValue(Folders("RootApplicationFLD"), cCFGFileName))

    //            '
    //            ' Inizializza l'elenco dei file necessari.
    //            '
    //            ListaFileNecessari = New ArrayList

    //            RecuperaDatiSistemaOperativo()

    //            FileCFGXml = New Xml.XmlDocument
    //            If System.IO.File.Exists(Me.Files("ConfigurazioneFIL")) Then
    //                FileCFGXml.Load(Me.Files("ConfigurazioneFIL"))
    //                Me.IsLoaded = True
    //            Else
    //                Me.IsLoaded = False
    //                FileCFGXml.AppendChild(FileCFGXml.CreateNode(Xml.XmlNodeType.Element, "configurazione", ""))
    //            End If

    //            Me.OLEDB_ConnString = New Dictionary(Of String, CLS_oledbconnstring)
    //            If BitOs = etipoBitOS.bit_32 Then
    //                Me.OLEDB_ConnString.Add("DB", New CLS_oledbconnstring("Microsoft.Jet.OLEDB.4.0", """Excel 8.0;HDR=YES;IMEX=1"""))
    //                Me.OLEDB_ConnString.Add("mdb", New CLS_oledbconnstring("Microsoft.Jet.OLEDB.4.0", """Excel 8.0;HDR=YES;IMEX=1"""))
    //                Me.OLEDB_ConnString.Add("xls", New CLS_oledbconnstring("Microsoft.Jet.OLEDB.4.0", """Excel 8.0;HDR=YES;IMEX=1"""))
    //            Else
    //                Me.OLEDB_ConnString.Add("DB", New CLS_oledbconnstring("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"))
    //                Me.OLEDB_ConnString.Add("mdb", New CLS_oledbconnstring("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"))
    //                Me.OLEDB_ConnString.Add("xls", New CLS_oledbconnstring("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"))
    //            End If
    //            Me.OLEDB_ConnString.Add("xlsx", New CLS_oledbconnstring("Microsoft.ACE.OLEDB.12.0", "Excel 12.0 XML"))
    //        Catch ex As Exception
    //            Me.IsLoaded = False
    //        End Try

    //    End Sub

    //    Property FileCFGXml As Xml.XmlDocument

    //    ' ************************************************************************ '
    //    ' Procedura per il salvataggio del file di configurazione.                 '
    //    ' ************************************************************************ '
    //    Public Sub SalvaFileConfigurazione()

    //        SafeXMLSaveFile(FileCFGXml, Me.Files("ConfigurazioneFIL"), Me.Folders("TempFLD"))

    //    End Sub

    //    Public Function GetBitOS() As etipoBitOS
    //        Dim osBit As etipoBitOS

    //        If Registry.LocalMachine.OpenSubKey("Hardware\Description\System\CentralProcessor\0").GetValue("Identifier").ToString.Contains("x86") Then
    //            osBit = etipoBitOS.bit_32
    //        Else
    //            osBit = etipoBitOS.bit_64
    //        End If
    //        Return osBit

    //    End Function

    //#Region "Elenco delle proprietà Pubblica"

    //    Public Function SetFTPPrdSviFld() As String

    //        Return String.Concat("FTP://", Me.FTPServIP, "/", TranslatePrdSviSwitch, "/")

    //    End Function

    //    Public Function TranslatePrdSviSwitch() As String

    //        If Me.SwitchPrdSvi = eSwitchPrdSvi.SPS_sviluppo Then
    //            Return "SVI"
    //        ElseIf Me.SwitchPrdSvi = eSwitchPrdSvi.SPS_produzio Then
    //            Return "PRD"
    //        ElseIf Me.SwitchPrdSvi = eSwitchPrdSvi.SPS_test Then
    //            Return "TST"
    //        Else
    //            Return "N/A"
    //        End If

    //    End Function

    //    Public ReadOnly Property DatiStatiConsegna(ByVal cFolder As String) As String

    //        Get
    //            Return String.Concat(ConcatenaFolderValue(cFolder, "Dati"), "StatiConsegna.xml")
    //        End Get

    //    End Property

    //    Public ReadOnly Property DatiDettagliStatiConsegna(ByVal cFolder As String) As String

    //        Get
    //            Return String.Concat(ConcatenaFolderValue(cFolder, "Dati"), "DettaglioStatiConsegna.xml")
    //        End Get

    //    End Property

    //    Private ReadOnly Property FTPServer() As Xml.XmlNode

    //        Get
    //            Return FileCFGXml.SelectSingleNode("configurazione/FTPServer")
    //        End Get

    //    End Property

    //    Public ReadOnly Property FTPServIP() As String

    //        Get
    //            Return FTPServer.SelectSingleNode("IPAddress").InnerText
    //        End Get

    //    End Property

    //    Public ReadOnly Property FTPPort() As String

    //        Get
    //            Return FTPServer.SelectSingleNode("PortNumber").InnerText
    //        End Get

    //    End Property

    //    Public ReadOnly Property FTPusername() As String

    //        Get
    //            Return FTPServer.SelectSingleNode("UserName").InnerText
    //        End Get

    //    End Property

    //    Public ReadOnly Property FTPpassword() As String

    //        Get
    //            Return FTPServer.SelectSingleNode("Password").InnerText
    //        End Get

    //    End Property

    //    Public ReadOnly Property SwitchPrdSvi() As eSwitchPrdSvi

    //        Get
    //            Return Me.GetConfigVariabile("SwitchPrdSvi", "1")
    //        End Get

    //    End Property

    //#End Region

    //    Public ReadOnly Property OLEDB_prefixStrs() As Dictionary(Of String, String)

    //        Get
    //            Return _OleDBConnStrs
    //        End Get

    //    End Property

    //#Region "Elenco dei metodi pubblici"

    //    ' ************************************************************************* '
    //    ' Recupera il valore della variabli di configurazione dal file di gestione. '
    //    ' ************************************************************************* '
    //    Public Function GetConfigVariabile(ByVal cItemToken As String, ByVal cDefaultValue As String) As String
    //        Dim cRtn As String

    //        cRtn = cDefaultValue
    //        If _IsLoaded Then
    //            cItemToken = String.Concat("configurazione/", cItemToken)
    //            If NodeNotNull(FileCFGXml.SelectSingleNode(cItemToken)) Then
    //                cRtn = FileCFGXml.SelectSingleNode(cItemToken).InnerText
    //            End If
    //            If cItemToken.ToLower.EndsWith("_path") Then
    //                cRtn = StandardFolderValue(cRtn)
    //            End If
    //        End If
    //        Return cRtn

    //    End Function

    //    ' ************************************************************************ '
    //    ' Imposta il valore della variabli di configurazione BOOLEANE nel file di  '
    //    ' gestione.                                                                '
    //    ' ************************************************************************ '
    //    Public Sub SetConfigVariabile(ByVal cItemToken As String, ByVal lItemValue As Boolean)
    //        Dim xnItem As Xml.XmlNode

    //        xnItem = GetItemConfigVariable(cItemToken)
    //        If lItemValue Then
    //            xnItem.InnerText = 1
    //        Else
    //            xnItem.InnerText = 0
    //        End If

    //    End Sub

    //    ' ************************************************************************ '
    //    ' Imposta il valore della variabli di configurazione INTEGER nel file di   '
    //    ' gestione.                                                                '
    //    ' ************************************************************************ '
    //    Public Sub SetConfigVariabile(ByVal cItemToken As String, ByVal lItemValue As Integer)
    //        Dim xnItem As Xml.XmlNode

    //        xnItem = GetItemConfigVariable(cItemToken)
    //        xnItem.InnerText.ToString()

    //    End Sub

    //    ' ************************************************************************ '
    //    ' Imposta il valore della variabli di configurazione STRINGA nel file di   ' 
    //    ' gestione.                                                                '
    //    ' ************************************************************************ '
    //    Public Sub SetConfigVariabile(ByVal cItemToken As String, ByVal cItemValue As String)
    //        Dim xnItem As Xml.XmlNode

    //        xnItem = GetItemConfigVariable(cItemToken)
    //        xnItem.InnerText = cItemValue

    //    End Sub

    //#End Region

    //#Region "Elenco dei metodi Privati"

    //    ' ************************************************************************ '
    //    ' Recupera il node dell'item della variabile di configurazione.            '
    //    ' ************************************************************************ '
    //    Private Function GetItemConfigVariable(ByVal cItemToken As String) As Xml.XmlNode
    //        Dim xnItem As Xml.XmlNode

    //        xnItem = FileCFGXml.SelectSingleNode(String.Concat("configurazione/", cItemToken))
    //        If NodeNull(xnItem) Then
    //            xnItem = FileCFGXml.CreateNode(Xml.XmlNodeType.Element, cItemToken, "")
    //            FileCFGXml.SelectSingleNode("configurazione").AppendChild(xnItem)
    //        End If
    //        Return xnItem

    //    End Function

    //#End Region

    //    ' ************************************************************************ '
    //    ' Questa procedura determina il sistema operativo presente sulla macchina  ' 
    //    ' dove girerà l'applicazione.                                              '
    //    ' ************************************************************************ '
    //    Private Sub RecuperaDatiSistemaOperativo()

    //        '
    //        ' Recupera la tipologia di sistema operativo
    //        '
    //        Me.OSVersion = eTipoSistemaOperativo.Sconosciuto
    //        Select Case Environment.OSVersion.Platform
    //            Case PlatformID.Win32S
    //                Me.OSVersion = eTipoSistemaOperativo.Win_3_1
    //            Case PlatformID.Win32Windows
    //                Select Case Environment.OSVersion.Version.Minor
    //                    Case 0
    //                        Me.OSVersion = eTipoSistemaOperativo.Win_95
    //                    Case 10
    //                        Me.OSVersion = eTipoSistemaOperativo.Win_98
    //                    Case 90
    //                        Me.OSVersion = eTipoSistemaOperativo.Win_ME
    //                End Select
    //            Case PlatformID.Win32NT
    //                Select Case Environment.OSVersion.Version.Major
    //                    Case 3
    //                        Me.OSVersion = eTipoSistemaOperativo.Win_NT_3_51
    //                    Case 4
    //                        Me.OSVersion = eTipoSistemaOperativo.Win_NT_4_0
    //                    Case 5
    //                        Select Case Environment.OSVersion.Version.Minor
    //                            Case 0
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_2000
    //                            Case 1
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_XP
    //                            Case 2
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_2003
    //                        End Select
    //                    Case 6
    //                        Select Case Environment.OSVersion.Version.Minor
    //                            Case Is = 0
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_2008
    //                            Case Is = 1
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_2008_R2
    //                            Case Is = 2
    //                                Me.OSVersion = eTipoSistemaOperativo.Win_2012
    //                        End Select
    //                End Select
    //            Case PlatformID.WinCE
    //                Me.OSVersion = eTipoSistemaOperativo.Win_CE
    //        End Select

    //        '
    //        ' Recupera il numero dei bit del processore del sistema operativo
    //        '
    //        If Registry.LocalMachine.OpenSubKey("Hardware\Description\System\CentralProcessor\0").GetValue("Identifier").ToString.Contains("x86") Then
    //            BitOs = etipoBitOS.bit_32
    //        Else
    //            BitOs = etipoBitOS.bit_64
    //        End If

    //    End Sub

    //    Public Sub ControllaFileApplicazione()

    //        '
    //        ' Controlla che i file necessari siano tutti presenti
    //        '
    //        ApplicazioneAvviabile = True
    //        For Each cFileName As String In ListaFileNecessari
    //            ApplicazioneAvviabile = ApplicazioneAvviabile And System.IO.File.Exists(ConcatenaFolderFileValue(Me.Folders("RootApplicationFLD"), cFileName))
    //        Next

    //    End Sub

    //    Public Sub ControllaFilePlugin()

    //        '
    //        ' Individua se sono presenti le interfacce lìper il caricamento dei plug in
    //        '
    //        Me.PlugIn_1_0_0_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_0.dll")
    //        Me.PlugIn_1_1_0_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_1.dll")
    //        Me.PlugIn_1_2_0_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_2.dll")
    //        Me.PlugIn_1_3_0_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_3.dll")
    //        Me.PlugIn_1_3_1_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_3_1.dll")
    //        Me.PlugIn_1_3_2_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_3_2.dll")
    //        Me.PlugIn_1_4_0_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_4.dll")
    //        Me.PlugIn_1_4_1_loadable = System.IO.File.Exists("PLUGIN_interfaceV1_4_1.dll")

    //    End Sub

    //#Region "da controllare utilità"

    //    Public ReadOnly Property FTPInputFLD() As String

    //        Get
    //            Return String.Concat(Me.SetFTPPrdSviFld, "INP/")
    //        End Get

    //    End Property

    //#End Region

}
}