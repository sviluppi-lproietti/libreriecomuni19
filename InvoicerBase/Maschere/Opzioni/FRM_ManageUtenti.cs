﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ISC.LibrerieComuni.InvoicerDomainBase.Entity;
using ISC.LibrerieComuni.InvoicerServiceBase;
using ISC.InvoicerBase.Classi;
using ISC.InvoicerBase.Varie;

namespace ISC.InvoicerBase.Maschere.Opzioni
{
    public partial class ManageUtenti : Form
    {
        private Dictionary<int, UtentiEntity> _dictUtenti { get; set; }
        private UtentiEntity _SelectUtente
        {
            get
            { return __SelectUtente; }
            set
            {
                __SelectUtente = value;
                _SetUtente = true;
                TXB_cognome.Text = _SelectUtente.UTE_cognome;
                TXB_nome.Text = _SelectUtente.UTE_nome;
                LBL_username.Text = _SelectUtente.UTE_username;
                CHB_isAdmin.Checked = _SelectUtente.isAdmin;
                CHB_isEnabled.Checked = _SelectUtente.isEnabled;
                _SetUtente = false;
            }
        }
        private UtentiEntity __SelectUtente { get; set; }
        private Boolean _SetUtente { get; set; }
        private Boolean _SetRiga { get; set; }
        private List<UtentiEntity> DisplaydUserList
        {
            get
            {
                return _dictUtenti.Values.Where(v => v.UTE_codice != 1).OrderBy(k => k.UTE_username).ToList();
            }
        }

        public ManageUtenti()
        {
            InitializeComponent();
        }

        private void ManageUtenti_Load(object sender, EventArgs e)
        {
            _SetUtente = false;
            _SetRiga = false;
            _dictUtenti = UtentiServices.LoadAllUsers();

            DGV_utenti.AutoGenerateColumns = false;
            DGV_utenti.DataSource = DisplaydUserList;
            GRP_utente.Enabled = false;

            if (DisplaydUserList.Count == 0)
                TSB_insert_Click(null, null);
        }

        private void DGV_utenti_SelectionChanged(object sender, EventArgs e)
        {
            if (!_SetRiga)
                _SelectUtente = ((UtentiEntity)this.DGV_utenti.CurrentRow.DataBoundItem).Clone();
        }

        private void TSB_insert_Click(object sender, EventArgs e)
        {
            _SelectUtente = new UtentiEntity();
            SettaPannelliAbilitati(true);
        }

        private void TSB_modifica_Click(object sender, EventArgs e)
        {
            SettaPannelliAbilitati(true);
        }

        private void TSB_cancella_Click(object sender, EventArgs e)
        {
            try
            {
                if (_SelectUtente.UTE_codice == CLS_OggettiGlobaliBase.UtenteLoggato.UTE_codice)
                    throw new Exception("L'utente corrente non può essere cancellato.");
                if (MessageBox.Show("Sei sicuro di voler procedere con la cancellazione dell'utente: '" + _SelectUtente.UTE_username + "'?", "Reset password", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    _dictUtenti.Remove(_SelectUtente.UTE_codice);
                    UtentiServices.SaveUsers(_dictUtenti);
                    DGV_utenti.DataSource = DisplaydUserList;
                    if (DisplaydUserList.Count == 0)
                        TSB_insert_Click(null, null);
                    else
                    {
                        DGV_utenti.CurrentCell = DGV_utenti.Rows[0].Cells[0];
                        DGV_utenti.Rows[0].Selected = true;
                    }
                    MessageBox.Show("Cancellazione correttamente eseguita.", "Cancellazione eseguita", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
        // Procedure per la gestione della modifica di un utente.
        //
        private void BTN_salva_Click(object sender, EventArgs e)
        {
            try
            {
                //
                // Controlli pre salvataggio
                //
                if (_SelectUtente.UTE_cognome.Length < 3 || _SelectUtente.UTE_nome.Length < 3)
                {
                    throw new Exception("Il nome e il cognome debbono essere lunghi almeno 3 caratteri.");
                }
                if (_SelectUtente.isNewUtente)
                {
                    if (_dictUtenti.Count > 0)
                    {
                        _SelectUtente.UTE_codice = _dictUtenti.Max(kvp => kvp.Value.UTE_codice) + 1;
                        if (_SelectUtente.UTE_codice < 1000)
                            _SelectUtente.UTE_codice = 1000;
                    }
                    else
                        _SelectUtente.UTE_codice = 1;
                    _SelectUtente.UTE_password = Utilita.HashValue("Iniziale123");
                    _dictUtenti.Add(_SelectUtente.UTE_codice, _SelectUtente);
                }
                else
                    _dictUtenti[_SelectUtente.UTE_codice] = _SelectUtente;
                UtentiServices.SaveUsers(_dictUtenti);

                _SetRiga = true;
                DGV_utenti.DataSource = DisplaydUserList;
                DataGridViewRow row = DGV_utenti.Rows.Cast<DataGridViewRow>()
                    .Where(r => ((UtentiEntity)r.DataBoundItem).UTE_codice == _SelectUtente.UTE_codice)
                    .First();
                DGV_utenti.ClearSelection();
                DGV_utenti.CurrentCell = DGV_utenti.Rows[row.Index].Cells[0];
                DGV_utenti.Rows[row.Index].Selected = true;
                _SetRiga = false;
                SettaPannelliAbilitati(false);
                MessageBox.Show("Salvataggio correttamente eseguito", "Salvataggio eseguito", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BTN_annulla_Click(object sender, EventArgs e)
        {
            if (_SelectUtente.isNewUtente)
            {
                _SetRiga = true;
                DGV_utenti.DataSource = DisplaydUserList;
                DGV_utenti.CurrentCell = DGV_utenti.Rows[0].Cells[0];
                DGV_utenti.Rows[0].Selected = true;
                _SetRiga = false;
            }
            else
            {
                _SelectUtente = _dictUtenti[_SelectUtente.UTE_codice].Clone();
            }
            SettaPannelliAbilitati(false);
        }

        private void BTN_reset_pwd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sei sicuro di voler procedere con il reset della password?", "Reset password", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _SelectUtente.UTE_password = Utilita.HashValue("Iniziale123");
                BTN_salva_Click(null, null);
            }
        }

        private void nominativo_TextChanged(object sender, EventArgs e)
        {
            if (!_SetUtente)
            {
                _SelectUtente.UTE_cognome = TXB_cognome.Text;
                _SelectUtente.UTE_nome = TXB_nome.Text;
                if (_SelectUtente.isNewUtente)
                {
                    UtentiServices.GeneraUsername(_SelectUtente);
                    while (_dictUtenti.Any(kvp => kvp.Value.UTE_username == _SelectUtente.UTE_username))
                    {
                        _SelectUtente.tiebreak += 1;
                        UtentiServices.GeneraUsername(_SelectUtente);
                    }
                    LBL_username.Text = _SelectUtente.UTE_username;
                }
            }
        }

        //
        // Procedura generale
        //
        private void SettaPannelliAbilitati(Boolean lEdit)
        {
            toolStrip1.Enabled = !lEdit;
            DGV_utenti.Enabled = !lEdit;
            GRP_utente.Enabled = lEdit;
            BTN_reset_pwd.Enabled = lEdit && !_SelectUtente.isNewUtente;
        }

        private void CHB_isEnabled_CheckedChanged(object sender, EventArgs e)
        {
            _SelectUtente.isEnabled = CHB_isEnabled.Checked;
        }

        private void CHB_isAdmin_CheckedChanged(object sender, EventArgs e)
        {
            _SelectUtente.isAdmin = CHB_isAdmin.Checked;
        }
    }
}
