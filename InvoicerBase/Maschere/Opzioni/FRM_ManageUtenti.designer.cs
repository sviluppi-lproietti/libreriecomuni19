﻿namespace ISC.InvoicerBase.Maschere.Opzioni
{
    partial class ManageUtenti
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGV_utenti = new System.Windows.Forms.DataGridView();
            this.UTE_username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTE_cognome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTE_nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRP_utente = new System.Windows.Forms.GroupBox();
            this.CHB_isEnabled = new System.Windows.Forms.CheckBox();
            this.BTN_reset_pwd = new System.Windows.Forms.Button();
            this.LBL_username = new System.Windows.Forms.Label();
            this.BTN_annulla = new System.Windows.Forms.Button();
            this.TXB_nome = new System.Windows.Forms.TextBox();
            this.TXB_cognome = new System.Windows.Forms.TextBox();
            this.BTN_salva = new System.Windows.Forms.Button();
            this.CHB_isAdmin = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TSB_insert = new System.Windows.Forms.ToolStripButton();
            this.TSB_modifica = new System.Windows.Forms.ToolStripButton();
            this.TSB_cancella = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_utenti)).BeginInit();
            this.GRP_utente.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGV_utenti
            // 
            this.DGV_utenti.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_utenti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_utenti.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UTE_username,
            this.UTE_cognome,
            this.UTE_nome});
            this.DGV_utenti.Location = new System.Drawing.Point(13, 28);
            this.DGV_utenti.Name = "DGV_utenti";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_utenti.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV_utenti.RowTemplate.Height = 24;
            this.DGV_utenti.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGV_utenti.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_utenti.Size = new System.Drawing.Size(690, 279);
            this.DGV_utenti.TabIndex = 0;
            this.DGV_utenti.SelectionChanged += new System.EventHandler(this.DGV_utenti_SelectionChanged);
            // 
            // UTE_username
            // 
            this.UTE_username.DataPropertyName = "UTE_username";
            this.UTE_username.HeaderText = "Username";
            this.UTE_username.Name = "UTE_username";
            this.UTE_username.ReadOnly = true;
            // 
            // UTE_cognome
            // 
            this.UTE_cognome.DataPropertyName = "UTE_cognome";
            this.UTE_cognome.HeaderText = "Cognome";
            this.UTE_cognome.Name = "UTE_cognome";
            this.UTE_cognome.ReadOnly = true;
            // 
            // UTE_nome
            // 
            this.UTE_nome.DataPropertyName = "UTE_nome";
            this.UTE_nome.HeaderText = "Nome";
            this.UTE_nome.Name = "UTE_nome";
            this.UTE_nome.ReadOnly = true;
            // 
            // GRP_utente
            // 
            this.GRP_utente.Controls.Add(this.CHB_isEnabled);
            this.GRP_utente.Controls.Add(this.BTN_reset_pwd);
            this.GRP_utente.Controls.Add(this.LBL_username);
            this.GRP_utente.Controls.Add(this.BTN_annulla);
            this.GRP_utente.Controls.Add(this.TXB_nome);
            this.GRP_utente.Controls.Add(this.TXB_cognome);
            this.GRP_utente.Controls.Add(this.BTN_salva);
            this.GRP_utente.Controls.Add(this.CHB_isAdmin);
            this.GRP_utente.Controls.Add(this.label3);
            this.GRP_utente.Controls.Add(this.label2);
            this.GRP_utente.Controls.Add(this.label1);
            this.GRP_utente.Location = new System.Drawing.Point(13, 313);
            this.GRP_utente.Name = "GRP_utente";
            this.GRP_utente.Size = new System.Drawing.Size(690, 120);
            this.GRP_utente.TabIndex = 2;
            this.GRP_utente.TabStop = false;
            this.GRP_utente.Text = "Dettaglio utenti";
            // 
            // CHB_isEnabled
            // 
            this.CHB_isEnabled.AutoSize = true;
            this.CHB_isEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CHB_isEnabled.Location = new System.Drawing.Point(305, 81);
            this.CHB_isEnabled.Name = "CHB_isEnabled";
            this.CHB_isEnabled.Size = new System.Drawing.Size(80, 21);
            this.CHB_isEnabled.TabIndex = 7;
            this.CHB_isEnabled.Text = "Abilitato";
            this.CHB_isEnabled.UseVisualStyleBackColor = true;
            this.CHB_isEnabled.CheckedChanged += new System.EventHandler(this.CHB_isEnabled_CheckedChanged);
            // 
            // BTN_reset_pwd
            // 
            this.BTN_reset_pwd.Image = global::ISC.InvoicerBase.Properties.Resources.Password_16;
            this.BTN_reset_pwd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_reset_pwd.Location = new System.Drawing.Point(525, 86);
            this.BTN_reset_pwd.Name = "BTN_reset_pwd";
            this.BTN_reset_pwd.Size = new System.Drawing.Size(159, 28);
            this.BTN_reset_pwd.TabIndex = 4;
            this.BTN_reset_pwd.Text = "RESET PWD";
            this.BTN_reset_pwd.UseVisualStyleBackColor = true;
            this.BTN_reset_pwd.Click += new System.EventHandler(this.BTN_reset_pwd_Click);
            // 
            // LBL_username
            // 
            this.LBL_username.AutoSize = true;
            this.LBL_username.Location = new System.Drawing.Point(106, 82);
            this.LBL_username.Name = "LBL_username";
            this.LBL_username.Size = new System.Drawing.Size(52, 17);
            this.LBL_username.TabIndex = 6;
            this.LBL_username.Text = "...........";
            // 
            // BTN_annulla
            // 
            this.BTN_annulla.Image = global::ISC.InvoicerBase.Properties.Resources.Ignora_16;
            this.BTN_annulla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_annulla.Location = new System.Drawing.Point(525, 50);
            this.BTN_annulla.Name = "BTN_annulla";
            this.BTN_annulla.Size = new System.Drawing.Size(159, 28);
            this.BTN_annulla.TabIndex = 3;
            this.BTN_annulla.Text = "ANNULLA";
            this.BTN_annulla.UseVisualStyleBackColor = true;
            this.BTN_annulla.Click += new System.EventHandler(this.BTN_annulla_Click);
            // 
            // TXB_nome
            // 
            this.TXB_nome.Location = new System.Drawing.Point(109, 54);
            this.TXB_nome.MaxLength = 20;
            this.TXB_nome.Name = "TXB_nome";
            this.TXB_nome.Size = new System.Drawing.Size(406, 22);
            this.TXB_nome.TabIndex = 5;
            this.TXB_nome.TextChanged += new System.EventHandler(this.nominativo_TextChanged);
            // 
            // TXB_cognome
            // 
            this.TXB_cognome.Location = new System.Drawing.Point(109, 25);
            this.TXB_cognome.MaxLength = 20;
            this.TXB_cognome.Name = "TXB_cognome";
            this.TXB_cognome.Size = new System.Drawing.Size(406, 22);
            this.TXB_cognome.TabIndex = 4;
            this.TXB_cognome.TextChanged += new System.EventHandler(this.nominativo_TextChanged);
            // 
            // BTN_salva
            // 
            this.BTN_salva.Image = global::ISC.InvoicerBase.Properties.Resources.Salva_16;
            this.BTN_salva.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_salva.Location = new System.Drawing.Point(525, 14);
            this.BTN_salva.Name = "BTN_salva";
            this.BTN_salva.Size = new System.Drawing.Size(159, 31);
            this.BTN_salva.TabIndex = 1;
            this.BTN_salva.Text = "SALVA";
            this.BTN_salva.UseVisualStyleBackColor = true;
            this.BTN_salva.Click += new System.EventHandler(this.BTN_salva_Click);
            // 
            // CHB_isAdmin
            // 
            this.CHB_isAdmin.AutoSize = true;
            this.CHB_isAdmin.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CHB_isAdmin.Location = new System.Drawing.Point(391, 81);
            this.CHB_isAdmin.Name = "CHB_isAdmin";
            this.CHB_isAdmin.Size = new System.Drawing.Size(124, 21);
            this.CHB_isAdmin.TabIndex = 3;
            this.CHB_isAdmin.Text = "Amministratore";
            this.CHB_isAdmin.UseVisualStyleBackColor = true;
            this.CHB_isAdmin.CheckedChanged += new System.EventHandler(this.CHB_isAdmin_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cognome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSB_insert,
            this.TSB_modifica,
            this.TSB_cancella});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(715, 27);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // TSB_insert
            // 
            this.TSB_insert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_insert.Image = global::ISC.InvoicerBase.Properties.Resources.Inserisci_32;
            this.TSB_insert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_insert.Name = "TSB_insert";
            this.TSB_insert.Size = new System.Drawing.Size(24, 24);
            this.TSB_insert.Text = "TSB_insert";
            this.TSB_insert.ToolTipText = "Aggiungi utente";
            this.TSB_insert.Click += new System.EventHandler(this.TSB_insert_Click);
            // 
            // TSB_modifica
            // 
            this.TSB_modifica.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_modifica.Image = global::ISC.InvoicerBase.Properties.Resources.Modifica_32;
            this.TSB_modifica.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_modifica.Name = "TSB_modifica";
            this.TSB_modifica.Size = new System.Drawing.Size(24, 24);
            this.TSB_modifica.Text = "TSB_modifica";
            this.TSB_modifica.ToolTipText = "Modifica utente";
            this.TSB_modifica.Click += new System.EventHandler(this.TSB_modifica_Click);
            // 
            // TSB_cancella
            // 
            this.TSB_cancella.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_cancella.Image = global::ISC.InvoicerBase.Properties.Resources.Elimina_16;
            this.TSB_cancella.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_cancella.Name = "TSB_cancella";
            this.TSB_cancella.Size = new System.Drawing.Size(24, 24);
            this.TSB_cancella.Text = "toolStripButton3";
            this.TSB_cancella.ToolTipText = "Cancella utente";
            this.TSB_cancella.Click += new System.EventHandler(this.TSB_cancella_Click);
            // 
            // ManageUtenti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 444);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.GRP_utente);
            this.Controls.Add(this.DGV_utenti);
            this.Name = "ManageUtenti";
            this.Text = "Gestione utenti";
            this.Load += new System.EventHandler(this.ManageUtenti_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_utenti)).EndInit();
            this.GRP_utente.ResumeLayout(false);
            this.GRP_utente.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGV_utenti;
        private System.Windows.Forms.Button BTN_salva;
        private System.Windows.Forms.GroupBox GRP_utente;
        private System.Windows.Forms.Button BTN_reset_pwd;
        private System.Windows.Forms.Label LBL_username;
        private System.Windows.Forms.Button BTN_annulla;
        private System.Windows.Forms.TextBox TXB_nome;
        private System.Windows.Forms.TextBox TXB_cognome;
        private System.Windows.Forms.CheckBox CHB_isAdmin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTE_username;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTE_cognome;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTE_nome;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton TSB_insert;
        private System.Windows.Forms.ToolStripButton TSB_modifica;
        private System.Windows.Forms.CheckBox CHB_isEnabled;
        private System.Windows.Forms.ToolStripButton TSB_cancella;
    }
}

