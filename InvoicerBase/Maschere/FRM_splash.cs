﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using ISC.LibrerieComuni.InvoicerServiceBase;
using ISC.InvoicerBase.Classi;
using ISC.InvoicerBase.Varie;

namespace ISC.InvoicerBase.Maschere
{
    public partial class FRM_splash : Form
    {
        public FRM_splash()
        {
            InitializeComponent();
            EsitoAutenticazione = 0;
        }

        public int EsitoAutenticazione { get; set; }
        public string CredenzialiFornite { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationVersion { get; set; }

        //private string ApplicationName
        //{
        //    get
        //    {
        //        var entryAssembly = Assembly.GetEntryAssembly();
        //        var applicationTitle = ((AssemblyTitleAttribute)entryAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title;
        //        if (string.IsNullOrWhiteSpace(applicationTitle))
        //        {
        //            applicationTitle = entryAssembly.GetName().Name;
        //        }
        //        return applicationTitle;
        //    }
        //}

        //private string ApplicationVersion
        //{
        //    get
        //    {
        //        var entryAssembly = Assembly.GetEntryAssembly();

        //        var rtnValue = LBL_version.Text + entryAssembly.GetName().Version.Major.ToString("00") + ".";
        //        rtnValue += entryAssembly.GetName().Version.Minor.ToString("00") + ".";
        //        rtnValue += entryAssembly.GetName().Version.Build.ToString("00") + ".";
        //        rtnValue += entryAssembly.GetName().Version.Revision.ToString("00");
        //        return rtnValue;
        //    }
        //}

        private int _NumeroLogin { get; set; }
        private int _NumeroMaxLogin { get { return 3; } }
        private int _StepControllo
        {
            get
            {
                return __StepControllo;
            }
            set
            {
                __StepControllo = value;

                if (__StepControllo == 1)
                {
                    //LBL_controllo_runn.Text = "Verifica versione sistema operativo"
                }
                else if (__StepControllo == 2)
                    LBL_controllo_runn.Text = "Verifica esistenza file applicazione";
                else if (__StepControllo == 3)
                {                    //LBL_controllo_runn.Text = "Presenza cartella appoggio stampe temporanee"
                }
                else if (__StepControllo == 4)
                    LBL_controllo_runn.Text = "Verifica stampanti configurate";
                else if (__StepControllo == 5)
                    LBL_controllo_runn.Text = "Verifica versione PDFCreator";
                this.Refresh();
            }
        }

        private int __StepControllo { get; set; }

        private void FRM_splash_Load(Object sender, System.EventArgs e)
        {
            LBL_app_name.Text = this.ApplicationName;
            LBL_version.Text = "Versione: " + this.ApplicationVersion;
            _StepControllo = 1;
            TMR_controlli.Enabled = true;
        }

        private void TMR_controlli_Tick(object sender, EventArgs e)
        {
            try
            {
                if (_StepControllo == 1)
                {
                    CLS_OggettiGlobaliBase.Applicationlogger.Debug("FRM_splash - TMR_controlli - Verifica esistenza file utenti");
                    if (!System.IO.File.Exists(UtentiServices.FileDatiFullPath))
                        throw new Exception("Il file dei dati utente non esiste. Non posso proseguire oltre.");
                }
                if (_StepControllo == 2)
                {
                    CLS_OggettiGlobaliBase.Applicationlogger.Debug("FRM_splash - TMR_controlli - Verifica esistenza file applicazione");
                    CLS_OggettiGlobaliBase.CFG.ControllaFileApplicazione();
                }
                else if (_StepControllo == 3)
                {
                    //If Not System.IO.Directory.Exists(ObjGlobali.CFG.Folders("PrnWrkFLD")) Then
                    //    System.IO.Directory.CreateDirectory(_cfg.PrnWrkFLD)
                    //End If
                }
                else if (_StepControllo == 4)
                {
                    CLS_OggettiGlobaliBase.Applicationlogger.Debug("FRM_start - TMR_controlli - Verifica stampanti configurate");
                    //OggettiGlobali.CFG.VerificaStampantiConfigurate();
                }
                else if (_StepControllo == 5)
                {
                    CLS_OggettiGlobaliBase.Applicationlogger.Debug("FRM_start - TMR_controlli - Verifica versione PDFCreator");
                    //OggettiGlobali.CFG.ControllaVersioneLibrerie();
                }
                else if (_StepControllo == 6)
                {
                    LBL_descr_contr.Visible = false;
                    LBL_controllo_runn.Visible = false;

                    _NumeroLogin = 1;
                    LBL_num_login.Text = _NumeroLogin.ToString();
                    LBL_num_login.Refresh();
                    TMR_controlli.Enabled = false;
                    GRP_autenticazione.Visible = true;

                    //
                    // Qualora sia inserita una coppia di credenziali con le quali forzare il login le uso.
                    //
                    if (CredenzialiFornite != "")
                    {
                        TXB_username.Text = CredenzialiFornite.Split('/')[0];
                        TXB_password.Text = CredenzialiFornite.Split('/')[1];
                        BTN_login.PerformClick();
                    }
                    else
                        TXB_username.Focus();
                }
                __StepControllo += 1;
            }
            catch (Exception ex)
            {
                TMR_controlli.Enabled = false;
                CLS_OggettiGlobaliBase.Applicationlogger.Error(ex.Message, ex);
                MessageBox.Show(ex.Message, "Controllo in errore", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                DialogResult = DialogResult.Cancel;
            }
        }

        private void BTN_login_Click(object sender, EventArgs e)
        {
            if (BTN_login.Text == "LOGIN")
            {
                try
                {
                    CLS_OggettiGlobaliBase.UtenteLoggato = UtentiServices.GetUserByUserName(TXB_username.Text, Utilita.HashValue(TXB_password.Text));

                    if (CLS_OggettiGlobaliBase.UtenteLoggato != null)
                    {
                        CLS_OggettiGlobaliBase.Applicationlogger.Info("Autenticazione riuscita per l'utente: " + TXB_username.Text);
                        EsitoAutenticazione = 1;
                        GRP_autenticazione.BackColor = Color.Green;

                        if (CLS_OggettiGlobaliBase.UtenteLoggato.isResetPwd)
                        {
                            EsitoAutenticazione = 2;
                            TXB_username.Enabled = false;
                            label4.Visible = true;
                            TXB_password.Text = "";
                            TXB_confermapwd.Visible = true;
                            BTN_login.Text = "CAMBIA";
                            label3.Visible = false;
                            LBL_num_login.Visible = false;
                            TXB_password.Focus();
                        }
                        else
                            DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        CLS_OggettiGlobaliBase.Applicationlogger.Error("Autenticazione fallita per l'utente: " + TXB_username.Text + ", tentativo " + _NumeroLogin.ToString());
                        GRP_autenticazione.BackColor = Color.Red;
                        TXB_username.Focus();
                        EsitoAutenticazione = 3;
                        if (_NumeroLogin == _NumeroMaxLogin)
                        {
                            CLS_OggettiGlobaliBase.Applicationlogger.Error("Autenticazione fallita per " + _NumeroMaxLogin.ToString() + " volte. L'accesso non è consentito.");
                            EsitoAutenticazione = 4;
                            DialogResult = DialogResult.Cancel;
                        }
                        else
                        {
                            _NumeroLogin += 1;
                            LBL_num_login.Text = _NumeroLogin.ToString();
                            LBL_num_login.Refresh();
                            TXB_username.Text = "";
                            TXB_password.Text = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    CLS_OggettiGlobaliBase.Applicationlogger.Error(ex.Message, ex);
                    MessageBox.Show(ex.Message, "Controllo in errore", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            else if (BTN_login.Text == "CAMBIA")
            {
                try
                {
                    if (TXB_password.Text != TXB_confermapwd.Text)
                        throw new Exception("Le password non coincidono");
                    if (TXB_password.Text.Length < 8)
                        throw new Exception("Le password deve essere lunga almeno 8 caratteri.");
                    if (CLS_OggettiGlobaliBase.UtenteLoggato.UTE_password == Utilita.HashValue(TXB_password.Text))
                        throw new Exception("La password non può essere uguale alla precedente.");
                    CLS_OggettiGlobaliBase.UtenteLoggato.UTE_password = Utilita.HashValue(TXB_password.Text);
                    CLS_OggettiGlobaliBase.UtenteLoggato.isResetPwd = false;
                    ISC.LibrerieComuni.InvoicerServiceBase.UtentiServices.AggiornaPassword(CLS_OggettiGlobaliBase.UtenteLoggato);
                    EsitoAutenticazione = 6;
                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    EsitoAutenticazione = 5;
                    CLS_OggettiGlobaliBase.Applicationlogger.Error(ex.Message, ex);
                    MessageBox.Show(ex.Message, "Controllo in errore", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

    }
}
