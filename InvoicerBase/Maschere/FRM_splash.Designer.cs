﻿namespace ISC.InvoicerBase.Maschere
{
    partial class FRM_splash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRM_splash));
            this.LBL_controllo_runn = new System.Windows.Forms.Label();
            this.LBL_descr_contr = new System.Windows.Forms.Label();
            this.LBL_version = new System.Windows.Forms.Label();
            this.LBL_app_name = new System.Windows.Forms.Label();
            this.PNL_Splash = new System.Windows.Forms.Panel();
            this.TMR_controlli = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.GRP_autenticazione = new System.Windows.Forms.GroupBox();
            this.TXB_confermapwd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LBL_num_login = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BTN_login = new System.Windows.Forms.Button();
            this.TXB_password = new System.Windows.Forms.TextBox();
            this.TXB_username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.GRP_autenticazione.SuspendLayout();
            this.SuspendLayout();
            // 
            // LBL_controllo_runn
            // 
            this.LBL_controllo_runn.AutoSize = true;
            this.LBL_controllo_runn.Location = new System.Drawing.Point(7, 262);
            this.LBL_controllo_runn.Name = "LBL_controllo_runn";
            this.LBL_controllo_runn.Size = new System.Drawing.Size(40, 13);
            this.LBL_controllo_runn.TabIndex = 9;
            this.LBL_controllo_runn.Text = "-----------";
            // 
            // LBL_descr_contr
            // 
            this.LBL_descr_contr.AutoSize = true;
            this.LBL_descr_contr.Location = new System.Drawing.Point(7, 240);
            this.LBL_descr_contr.Name = "LBL_descr_contr";
            this.LBL_descr_contr.Size = new System.Drawing.Size(88, 13);
            this.LBL_descr_contr.TabIndex = 8;
            this.LBL_descr_contr.Text = "Controllo in corso";
            // 
            // LBL_version
            // 
            this.LBL_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_version.Location = new System.Drawing.Point(6, 202);
            this.LBL_version.Name = "LBL_version";
            this.LBL_version.Size = new System.Drawing.Size(222, 20);
            this.LBL_version.TabIndex = 7;
            this.LBL_version.Text = "Vers. ";
            this.LBL_version.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LBL_app_name
            // 
            this.LBL_app_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_app_name.Location = new System.Drawing.Point(6, 9);
            this.LBL_app_name.Name = "LBL_app_name";
            this.LBL_app_name.Size = new System.Drawing.Size(227, 20);
            this.LBL_app_name.TabIndex = 6;
            this.LBL_app_name.Text = "......";
            // 
            // PNL_Splash
            // 
            this.PNL_Splash.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PNL_Splash.BackgroundImage")));
            this.PNL_Splash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNL_Splash.Location = new System.Drawing.Point(10, 32);
            this.PNL_Splash.Name = "PNL_Splash";
            this.PNL_Splash.Size = new System.Drawing.Size(223, 158);
            this.PNL_Splash.TabIndex = 5;
            // 
            // TMR_controlli
            // 
            this.TMR_controlli.Interval = 1000;
            this.TMR_controlli.Tick += new System.EventHandler(this.TMR_controlli_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(2, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Username:";
            // 
            // GRP_autenticazione
            // 
            this.GRP_autenticazione.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GRP_autenticazione.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GRP_autenticazione.Controls.Add(this.TXB_confermapwd);
            this.GRP_autenticazione.Controls.Add(this.label4);
            this.GRP_autenticazione.Controls.Add(this.LBL_num_login);
            this.GRP_autenticazione.Controls.Add(this.label3);
            this.GRP_autenticazione.Controls.Add(this.BTN_login);
            this.GRP_autenticazione.Controls.Add(this.TXB_password);
            this.GRP_autenticazione.Controls.Add(this.TXB_username);
            this.GRP_autenticazione.Controls.Add(this.label2);
            this.GRP_autenticazione.Controls.Add(this.label1);
            this.GRP_autenticazione.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GRP_autenticazione.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GRP_autenticazione.Location = new System.Drawing.Point(10, 288);
            this.GRP_autenticazione.Margin = new System.Windows.Forms.Padding(2);
            this.GRP_autenticazione.Name = "GRP_autenticazione";
            this.GRP_autenticazione.Padding = new System.Windows.Forms.Padding(2);
            this.GRP_autenticazione.Size = new System.Drawing.Size(223, 132);
            this.GRP_autenticazione.TabIndex = 11;
            this.GRP_autenticazione.TabStop = false;
            this.GRP_autenticazione.Text = "Autenticazione";
            this.GRP_autenticazione.Visible = false;
            // 
            // TXB_confermapwd
            // 
            this.TXB_confermapwd.Location = new System.Drawing.Point(67, 63);
            this.TXB_confermapwd.Margin = new System.Windows.Forms.Padding(2);
            this.TXB_confermapwd.Name = "TXB_confermapwd";
            this.TXB_confermapwd.PasswordChar = '*';
            this.TXB_confermapwd.Size = new System.Drawing.Size(152, 20);
            this.TXB_confermapwd.TabIndex = 14;
            this.TXB_confermapwd.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 65);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Conferma:";
            this.label4.Visible = false;
            // 
            // LBL_num_login
            // 
            this.LBL_num_login.AutoSize = true;
            this.LBL_num_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LBL_num_login.Location = new System.Drawing.Point(206, 115);
            this.LBL_num_login.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LBL_num_login.Name = "LBL_num_login";
            this.LBL_num_login.Size = new System.Drawing.Size(13, 13);
            this.LBL_num_login.TabIndex = 16;
            this.LBL_num_login.Text = "0";
            this.LBL_num_login.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(2, 115);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Tentativo di login n°:";
            // 
            // BTN_login
            // 
            this.BTN_login.Location = new System.Drawing.Point(4, 85);
            this.BTN_login.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_login.Name = "BTN_login";
            this.BTN_login.Size = new System.Drawing.Size(214, 27);
            this.BTN_login.TabIndex = 18;
            this.BTN_login.Text = "LOGIN";
            this.BTN_login.UseVisualStyleBackColor = true;
            this.BTN_login.Click += new System.EventHandler(this.BTN_login_Click);
            // 
            // TXB_password
            // 
            this.TXB_password.Location = new System.Drawing.Point(67, 40);
            this.TXB_password.Margin = new System.Windows.Forms.Padding(2);
            this.TXB_password.Name = "TXB_password";
            this.TXB_password.PasswordChar = '*';
            this.TXB_password.Size = new System.Drawing.Size(152, 20);
            this.TXB_password.TabIndex = 13;
            // 
            // TXB_username
            // 
            this.TXB_username.Location = new System.Drawing.Point(67, 17);
            this.TXB_username.Margin = new System.Windows.Forms.Padding(2);
            this.TXB_username.Name = "TXB_username";
            this.TXB_username.Size = new System.Drawing.Size(152, 20);
            this.TXB_username.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Password:";
            // 
            // FRM_splash
            // 
            this.AcceptButton = this.BTN_login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(241, 436);
            this.Controls.Add(this.GRP_autenticazione);
            this.Controls.Add(this.LBL_controllo_runn);
            this.Controls.Add(this.LBL_descr_contr);
            this.Controls.Add(this.LBL_version);
            this.Controls.Add(this.LBL_app_name);
            this.Controls.Add(this.PNL_Splash);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FRM_splash";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_splash";
            this.Load += new System.EventHandler(this.FRM_splash_Load);
            this.GRP_autenticazione.ResumeLayout(false);
            this.GRP_autenticazione.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label LBL_controllo_runn;
        internal System.Windows.Forms.Label LBL_descr_contr;
        internal System.Windows.Forms.Label LBL_version;
        internal System.Windows.Forms.Label LBL_app_name;
        internal System.Windows.Forms.Panel PNL_Splash;
        private System.Windows.Forms.Timer TMR_controlli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GRP_autenticazione;
        private System.Windows.Forms.TextBox TXB_password;
        private System.Windows.Forms.TextBox TXB_username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_login;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LBL_num_login;
        private System.Windows.Forms.TextBox TXB_confermapwd;
        private System.Windows.Forms.Label label4;
    }
}