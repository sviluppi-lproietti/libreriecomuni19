﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using log4net;
using ISC.Invoicer40.Processi.Base;
using ISC.LibrerieComuni.InvoicerService;
using ISC.LibrerieComuni.ManageSessione;
using ISC.LibrerieComuni.InvoicerService.Dati.Impl;
using ISC.LibrerieComuni.InvoicerDomain.Entity;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Base;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Impl;

namespace ISC.Invoicer40.Processi.Impl
{
    public class Processo_10_CaricamentoDati : ProcessoBase
    {
        //

        //
        public Boolean StampaCartacea_Forzata { get; set; }
        public Boolean StampaOttica_Abilitata { get; set; }
        public Decimal StampaOttica_MaxPageXParte { get; set; }
        public String StampaOttica_SuffissoXParte { get; set; }
        public Boolean FastPrint_Abilita { get; set; }

        public String PlugIn_CheckSum { get; set; }

        public String DatiFissi_CheckSum { get; set; }
        public String DatiFissi_FileName { get; set; }
        //

        //
        public GestioneSessione GSS { get; set; }
        public decimal LimiteScaricoGruppi { get; set; }
        public Dictionary<int, CLS_GestioneModuloStampare> ListaModuliDaStampare { get; set; }
        public List<GruppoBase> ListaGruppiCreati { get; set; }
        public DatiStampaGenerale DatiMaster { get; set; }
        public Decimal NumeroItemPerGruppo { get; set; }

        public Processo_10_CaricamentoDati() : base()
        {
            Codice = 10;
            Descrizione = "Preparazione e caricamento dei gruppi da gestire.";
            Logger = LogManager.GetLogger("THRLoadDBLog");
            Stato = eStatoProcesso.STP_da_settare;
            StatoForzato = eStatoProcesso.STP_nullo;
            ListaGruppiCreati = new List<GruppoBase>();
        }

        public override void EseguiAzioneProcesso()
        {
            Boolean TerminatiRecordDaElaborare;
            GruppoPerStampa tmp;
            int loopModulo;
            DataRow dr_mai_qd;
            decimal nMinRecordNo;

            Logger.Debug("INIZIO - Azione processo " + this.Descrizione);
            Stato = eStatoProcesso.STP_in_elaborazione;
            TerminatiRecordDaElaborare = false;

            //
            // Imposta a 0 l'ultimo record caricato. In questo modo ricomincio il giro.
            //
            foreach (CLS_GestioneModuloStampare m in ListaModuliDaStampare.Values)
            {
                m.PrimoRecordDaCaricare = 0;
                m.UltimoRecordCaricato = 0;
                m.TuttiRecordCaricati = false;
            }

            while (Stato != eStatoProcesso.STP_terminato)
            {
                if (Stato == eStatoProcesso.STP_in_elaborazione)
                {
                    loopModulo = 0;
                    nMinRecordNo = decimal.MaxValue;
                    foreach (CLS_GestioneModuloStampare m in ListaModuliDaStampare.Values)
                    {
                        loopModulo = loopModulo + 1;

                        if (!m.TuttiRecordCaricati)
                        {
                            //
                            // Attivazione modulo per il recupero dei parametri delle tabelle dei dati.
                            // QUESTA OPERAZIONE DEVE ESSERE LA PRIMA DEL LOOP. 
                            // TUTTE LE ATTIVITA' SEGUENTI SONO CONDIZIONATE DA QUESTA
                            // 
                            Logger.Debug("  > Attivazione del modulo: (" + m.CodiceModulo.ToString() + ") " + m.NomeModulo);
                            this.GSS.AttivaModulo(m.CodiceModulo);

                            Logger.Debug("  > Creazione del gruppo dati da gestire.");
                            //tmp = (GruppoPerStampa)GruppoDefault.Clone();

                            tmp = new GruppoPerStampa(this.DatiFissi_FileName);
                            tmp.StampaCartacea_Forzata = this.StampaCartacea_Forzata;
                            tmp.StampaOttica_Abilitata = this.StampaOttica_Abilitata;
                            tmp.StampaOttica_MaxPageXParte = this.StampaOttica_MaxPageXParte;
                            tmp.StampaOttica_SuffissoXParte = this.StampaOttica_SuffissoXParte;
                            tmp.FastPrint_Abilita = this.FastPrint_Abilita;
                            tmp.PlugIn_CheckSum = this.PlugIn_CheckSum;
                            tmp.DatiFissi_CheckSum = this.DatiFissi_CheckSum;

                            //
                            // Impostazione dei campi del modulo in stampa.
                            //
                            tmp.ModuloDaStampare = m.Clone();
                            tmp.InserisciFincatura = m.InserisciFincatura;
                            tmp.StampaDettaglio_Forzata = m.StampaForzataDettaglio;
                            tmp.FronteRetro = m.FronteRetro;
                            tmp.Raccolta = ListaModuliDaStampare.Values.Count == 1 ? 9 : 0;
                            tmp.FattoreCorrezione = m.FattoreCorrezione;
                            if (tmp.Raccolta == 0)
                            {
                                if (loopModulo == 1)
                                    tmp.Raccolta = 1;
                                else if (loopModulo == ListaModuliDaStampare.Values.Count)
                                    tmp.Raccolta = 3;
                                else
                                    tmp.Raccolta = 2;
                            }

                            //tmp.ItemToken = cItemToken;
                            // tmp = new StrutturaGruppoStampa(this.GSS.FullDataset_MainTableName, this.GSS.Files["PlugInDatiFissiFIL"], GSS.LinkField_QuickDS_FullDS, m.StampaForzataDettaglio);
                            //tmp.DatiGruppo.StampaForzataDettaglio = m.StampaForzataDettaglio;
                            tmp.DatiGruppo.AccessoDati.MainTableName = this.GSS.FullDataset_MainTableName;
                            tmp.DatiGruppo.AccessoDati.MainLinkToQD = this.GSS.LinkField_QuickDS_FullDS;
                            tmp.DatiGenerali = DatiMaster;

                            tmp.DatiGruppo.ArchiviazioneOtticaFNTipo = m.Modulo.NomeFileArchivioOtticoSelect.AOF_filename;
                            tmp.DatiGruppo.FusioneFNTipo = this.GSS.ModuloUnificato.NomeFileArchivioOtticoSelect.AOF_filename;

                            //tmp.DatiGruppo.ArchiviazioneOtticaFN = this.GSS.Files[""];

                            //tmp.DatiGruppo.DatiCore = tmp.DatiGenerali.DatiBaseStampa;


                            //
                            // Popolazione dei campi che fanno riferimento alla base dati per la stampa.
                            //
                            tmp.Database = new LibrerieComuni.ManageAccessoDati.CLS_accessodati
                            {
                                DatiFissiFileName = this.GSS.Files["PlugInDatiFissiFIL"],
                                MainTableName = this.GSS.FullDataset_MainTableName,
                                MainLinkToQD = this.GSS.LinkField_QuickDS_FullDS
                            };
                            // Logger.Info("  > Caricamento per il record: " + saTHRLD.DatiGenerali.RecordDaStampare_Primo.ToString() + " Modulo: " + saTHRLD.CodiceModulo.ToString());
                            Logger.Debug("  > INIZIO - Caricamento database");
                            try
                            {
                                Logger.Debug("      > Primo record da caricare.: " + (m.PrimoRecordDaCaricare).ToString());
                                GSS.GoToRecordNumber(decimal.ToInt32(m.PrimoRecordDaCaricare));

                                Logger.Debug("      > Numero record da caricare: " + this.NumeroItemPerGruppo.ToString());
                                GSS.Carica_FullDataset(decimal.ToInt32(this.NumeroItemPerGruppo));
                                tmp.Database.DBDati_PlugIn = GSS.FullDataset.Copy();
                                decimal nKey;
                                foreach (System.Data.DataRow dr in tmp.Database.MainTable.Rows)
                                {
                                    nKey = decimal.Parse(dr[tmp.Database.MainLinkToQD].ToString());
                                    if (!tmp.Database.ElencoPagineDocumenti.ContainsKey(nKey))
                                        tmp.Database.ElencoPagineDocumenti.Add(nKey, new ContatoriPagineEntity(nKey, tmp.StampaOttica_Abilitata, false, 1000, 1000));
                                    //AccessoDati.ElencoPagineDocumenti.Add(nKey, new ContatoriPagineEntity(nKey, this.DatiCore.Stampa_ArchiviazioneOttica, this.FronteRetro, this.DatiCore.NumeroMassimoPaginePerParte, this.MaxNumeroFogli));
                                }

                                tmp.DatiGruppo.AssegnaDatabase(GSS.FullDataset.Copy());

                                tmp.DatiGruppo.AccessoDati.DBDati_PlugIn = tmp.Database.DBDati_PlugIn;
                                Logger.Debug("      > Numero Record caricati: " + tmp.RecordFullDatasetMainTable.ToString());
                                if (!tmp.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                                {
                                    Logger.Debug("      > Verifico se i record che ho caricato sono da stampare.");

                                    //
                                    // Se stiamo stampando i docuementi cartacei e non abbiamo impostato la forzatura della stampa cartacea allora procedo a cancellare i dati dei record interessati.
                                    //
                                    if (!tmp.StampaOttica_Abilitata && !tmp.StampaCartacea_Forzata && !tmp.ModuloDaStampare.ModuloSenzaSelezioneNecessaria)
                                    {
                                        foreach (DataRow dr_fud in tmp.DatiGruppo.AccessoDati.MainTable.Rows)
                                        {
                                            // ********************************************************************* //
                                            // Per alcuni plug-in il valore della colonna MAINTABLEKEY della tabella //
                                            // QUICKDATASET è di tipo 'string' e pertanto va effettuata una select   //
                                            // con tipologia stringa quindi con apici prima e dopo il valore ricerca //
                                            // to.                                                                   //
                                            // ********************************************************************* //
                                            if (GSS.QuickDataTable.Columns[this.GSS.QuickDataset_MainTableKey.ToString()].DataType == typeof(System.String))
                                                dr_mai_qd = GSS.QuickDataTable.Select(String.Concat(GSS.QuickDataset_MainTableKey, " = '", dr_fud[GSS.LinkField_QuickDS_FullDS], "'"))[0];
                                            else
                                                dr_mai_qd = GSS.QuickDataTable.Rows.Find(dr_fud[GSS.LinkField_QuickDS_FullDS]);   //' .Select(String.Concat(GSS.QuickDataset_MainTableKey, " = ", dr_fud.Item(GSS.LinkField_QuickDS_FullDS)))(0)

                                            if (((Boolean)dr_mai_qd["DEF_onlyarchott"]) && ((Boolean)dr_mai_qd["DEF_sendbymail"]))
                                            {
                                                Logger.Debug("      > Record rimosso.: " + dr_fud[GSS.LinkField_QuickDS_FullDS].ToString());
                                                dr_fud.Delete();
                                            }
                                            tmp.DatiGruppo.AccessoDati.MainTable.AcceptChanges();
                                        }
                                        Logger.Debug("      > Numero Record depurati: " + tmp.RecordFullDatasetMainTable.ToString());
                                    }

                                    //
                                    // Se ci sono dati da stampare allora procedo ad aggiungere il gruppo a quelli creati.
                                    //
                                    if (!tmp.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                                    {
                                        Logger.Debug("      > Gruppo con dati, è necesario aggiungerlo.");
                                        tmp.Stato = eStatoGruppo.STR_caricato;
                                        ListaGruppiCreati.Add(tmp);
                                    }
                                    else
                                        Logger.Debug("      > Gruppo vuoto, non è necesario aggiungerlo.");
                                }
                                else
                                    Logger.Debug("      > Gruppo vuoto, non è necesario aggiungerlo.");
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("      > ERRORE - Caricamento database", ex);
                            }
                            finally
                            {
                                m.UltimoRecordCaricato = GSS.CurrentRecordNumber;
                                m.PrimoRecordDaCaricare = GSS.CurrentRecordNumber;
                            }
                            Logger.Debug("  > FINE - Caricamento database");

                            m.TuttiRecordCaricati = GSS.QuickDataTable.Rows.Count < m.UltimoRecordCaricato;
                            nMinRecordNo = Math.Min(nMinRecordNo, m.UltimoRecordCaricato);
                            //lock (lockAttivita)
                            //{
                            //    if (saTHRLD.DatiGruppo.AccessoDati.IsEmpty_DBDati_PlugIn)
                            //    {
                            //        OggettiGlobali.loggerTHRLoadDB.Debug("  > Il repository è vuoto lo sposto in stato PreAttesaFusione o lo metto in stato terminato.");
                            //        if (this.RichiestaFusioneModuli)
                            //            saTHRLD.StatoGruppo = eStatoGruppo.STR_pre_attesa_fusione;
                            //        else
                            //            saTHRLD.StatoGruppo = eStatoGruppo.STR_fine;
                            //    }
                            //    else
                            //    {
                            //        OggettiGlobali.loggerTHRLoadDB.Debug("  > Il repository è pieno lo aggiungo a quelli da stampare.");
                            //        saTHRLD.StatoGruppo = eStatoGruppo.STR_attesa_anteprima;
                            //    }
                            //    AggiungiStrutturaAttivita(saTHRLD);
                            //    OggettiGlobali.loggerTHRLoadDB.Debug("  > Aggiunta effettuata con successo.");
                            //}

                            //this.DatiMaster.RecordDaStampare_Primo = this.DatiMaster.RecordDaStampare_Ultimo;
                            //this.RecordStampare += 1;
                            //tmp = (StrutturaGruppoStampa)DatiGruppo.Clone();
                            //tmp = new StrutturaGruppoStampa
                            //  {
                            //    CodiceModulo = m.CodiceModulo
                            //};
                        }
                        TerminatiRecordDaElaborare = GSS.QuickDataTable.Rows.Count <= nMinRecordNo;
                    }
                    if (ListaGruppiCreati.Count > this.LimiteScaricoGruppi || TerminatiRecordDaElaborare)
                    {
                        Stato = eStatoProcesso.STP_scarica_dati;
                    }
                    Logger.Debug("  > Numero gruppi creati: " + ListaGruppiCreati.Count.ToString());
                }
                else if (Stato == eStatoProcesso.STP_riprendi_elaborazione)
                {
                    if (TerminatiRecordDaElaborare)
                        Stato = eStatoProcesso.STP_terminato;
                    else
                        Stato = eStatoProcesso.STP_in_elaborazione;
                }
                else if (Stato == eStatoProcesso.STP_in_attesa)
                {
                    Logger.Debug("  > Caricamento record in pausa.");
                    System.Threading.Thread.Sleep(1000);
                }
                if (StatoForzato != eStatoProcesso.STP_nullo && StatoForzato != eStatoProcesso.STP_terminato)
                {
                    Stato = StatoForzato;
                    StatoForzato = eStatoProcesso.STP_nullo;
                }
                else
                    StatoForzato = eStatoProcesso.STP_nullo;
            }
            Stato = eStatoProcesso.STP_terminato;
            Logger.Debug("FINE - Azione processo " + this.Descrizione);
        }

    }
}
