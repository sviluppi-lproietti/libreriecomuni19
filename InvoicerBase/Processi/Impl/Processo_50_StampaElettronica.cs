﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ISC.Invoicer.ManageStampa;
using ISC.Invoicer40.Processi.Base;

namespace ISC.Invoicer40.Processi.Impl
{
    public class Processo_50_StampaElettronica : ProcessoBase
    {
        public Processo_50_StampaElettronica()
        {
            Boolean Anteprima = false;

            Logger = LogManager.GetLogger("THRGenStaLog");
            MotoreStampa = new CLS_motore_stampa("");
            Stato = eStatoProcesso.STP_da_settare;
        }

        // ********************************************************************* //
        // Elenco delle Proprietà.                                               //
        // ********************************************************************* //
        protected Boolean Anteprima { get { return false; } }

        private CLS_motore_stampa MotoreStampa { get; set; }

        // ********************************************************************* //
        // Elenco dei metodi.                                               //
        // ********************************************************************* //
        public override void EseguiAzioneProcesso()
        {
            throw new NotImplementedException();
        }
    }
}
