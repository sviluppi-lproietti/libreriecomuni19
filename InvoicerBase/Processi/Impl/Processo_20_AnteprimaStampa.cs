﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Xml;
using System.Data;
using ISC.Invoicer.ManageStampa;
using ISC.Invoicer40.Processi.Base;
using ISC.LibrerieComuni.InvoicerDomain.Entity;
using ISC.LibrerieComuni.InvoicerService.Dati.Impl;
using ISC.LibrerieComuni.ManageSessione;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Impl;

namespace ISC.Invoicer40.Processi.Impl
{
    public class Processo_20_AnteprimaStampa : ProcessoBase
    {
        //
        // Converte una vola per tutte la struttura del gruppo da elaborare.
        //
        public GruppoPerStampa GruppoInElaborazione
        {
            get
            {
                return (GruppoPerStampa)base.DatiGruppo;
            }
            set
            {
                base.DatiGruppo = value;
            }
        }
        //public GestioneSessione GSS { get; set; }
        public string Stampante { set { MotoreStampa = new CLS_motore_stampa(value); } }
        public CLS_FileFolderDic Folders { get; set; }
        public CLS_FileFolderDic Files { get; set; }
        //public DatiStampaGenerale DatiMaster { get; set; }

        private System.Xml.XmlDocument _xdFastPrint { get; set; }

        public Processo_20_AnteprimaStampa()
        {
            Codice = 20;
            Descrizione = "Creazione anteprima di stampa.";
            Logger = LogManager.GetLogger("THRGenAntLog");

            Stato = eStatoProcesso.STP_da_settare;
            StatoForzato = eStatoProcesso.STP_nullo;
        }

        protected Boolean Anteprima { get { return true; } }

        private CLS_motore_stampa MotoreStampa { get; set; }

        public override void EseguiAzioneProcesso()
        {
            Logger.Debug("INIZIO - Azione processo " + this.Descrizione);
            Stato = eStatoProcesso.STP_in_elaborazione;
            Logger.Debug("  > Caricamento dei dati di anteprima.");
            Carica_FileDatiAnteprima();

            while (Stato != eStatoProcesso.STP_terminato)
            {
                if (GruppoInElaborazione == null)
                {
                    Stato = eStatoProcesso.STP_in_attesa;
                    Logger.Debug("  > Nessun gruppo assegnato. Processo in pausa.");
                    System.Threading.Thread.Sleep(1000);
                }
                if (Stato == eStatoProcesso.STP_in_elaborazione && GruppoInElaborazione.Stato == eStatoGruppo.STR_genera_anteprima)
                {
                    try
                    {
                        if (!GruppoInElaborazione.FastPrint_Abilita || (GruppoInElaborazione.FastPrint_Abilita & !this.Recupera_DatiAnteprima()))
                        {
                            Logger.Debug("  > Generazione anteprima di stampa NECESSARIA per il record codice: " + GruppoInElaborazione.Chiave.ToString() + " modulo: " + GruppoInElaborazione.Modulo_Codice.ToString());

                            //if (DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                            //    do
                            //    {
                            //        CancellaDocumentoGenerare(DatiGruppo.ArchiviazioneOtticaFN);
                            //        if (FileDeletingLocked)
                            //            Thread.Sleep(1000);
                            //    }
                            //    while (FileDeletingLocked);
                            //lock (lockStampante)
                            //{
                            //if (DatiMaster.DatiBaseStampa.Stampa_ArchiviazioneOttica)
                            //{
                            //    prnprn = new CLS_motore_stampa(this.saTHRGA.DatiGruppo.GMS.NomeStampanteOttica);
                            //}
                            //else
                            //    prnprn = new CLS_motore_stampa(this.saTHRGA.DatiGruppo.GMS.NomeStampanteCartacea);
                            MotoreStampa.DatiGruppo = GruppoInElaborazione;

                           // MotoreStampa.DatiGenerali = (DatiStampaGenerale)this.DatiMaster.Clone();
                            GruppoInElaborazione.ModuliFLD = this.Folders["ModuliFLD"];
                            MotoreStampa.ModuloDaStampare = GruppoInElaborazione.ModuloDaStampare;
                            MotoreStampa.WorkFLD = this.Folders["PrnWrkFLD"];
                            MotoreStampa.PrintController = new System.Drawing.Printing.StandardPrintController();
                            MotoreStampa.PrinterSettings.PrintFileName = String.Concat(this.Folders["PrnWrkFLD"], "print_temp_", OGC_utilita.Timestamp());
                            if (MotoreStampa != null)
                            {
                                MotoreStampa.Print();
                                //if (this.saTHRGA.DatiGruppo.ContatoriPagine().PartiDocumento > 1)
                                //SetUpError2QuickDataset(prnprn.StatusPrn);
                                MotoreStampa.Dispose();
                            }
                            this.Salva_DatiAnteprima();
                        }
                        else
                            Logger.Debug("  > Generazione anteprima di stampa NON NECESSARIA per il record codice: " + GruppoInElaborazione.Chiave.ToString() + " modulo: " + GruppoInElaborazione.Modulo_Codice.ToString());
                        GruppoInElaborazione.Stato = eStatoGruppo.STR_attesa_stampa;
                        Stato = eStatoProcesso.STP_in_attesa;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("      > ERRORE - Stampa anteprima. ", ex);
                    }
                }
                if (StatoForzato != eStatoProcesso.STP_nullo && StatoForzato != eStatoProcesso.STP_terminato)
                {
                    Stato = StatoForzato;
                    StatoForzato = eStatoProcesso.STP_nullo;
                }
                else
                    StatoForzato = eStatoProcesso.STP_nullo;
            }
            Stato = eStatoProcesso.STP_terminato;
            Logger.Debug("FINE - Azione processo " + this.Descrizione);
        }

        public void Carica_FileDatiAnteprima()
        {
            try
            {
                _xdFastPrint = new System.Xml.XmlDocument();
                if (System.IO.File.Exists(this.Files["FastPrintFIL"]))
                    _xdFastPrint.Load(this.Files["FastPrintFIL"]);
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(this.Files["FastPrintFIL"]);
                _xdFastPrint = null;
                Logger.Error("Caricamento file per fast print.", ex);
                throw ex;
            }
            finally
            {
                if (!_xdFastPrint.HasChildNodes)
                {
                    _xdFastPrint.LoadXml("<fastprint></fastprint>");
                    //GruppoDaElaborare.FastPrint_Abilita = false;
                }
            }
        }

        public Boolean Recupera_DatiAnteprima()
        {
            Boolean lRtn;
            ContatoriPagineEntity c;
            XmlNode xnGruppo;
            int nKey;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            lRtn = false;
            try
            {
                if (GruppoInElaborazione.DatiGruppo.AccessoDati.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(GruppoInElaborazione.DatiGruppo.AccessoDati.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GruppoInElaborazione.PlugIn_CheckSum, "' and @modulo='", GruppoInElaborazione.ModuloDaStampare.Modulo.ModuloCheckSum, "' and @datifissi='", GruppoInElaborazione.DatiFissi_CheckSum, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo != null)
                {
                    foreach (DataRow dr in GruppoInElaborazione.Database.MainTable.Rows)
                    {
                        nKey = int.Parse(dr[GruppoInElaborazione.Database.MainLinkToQD].ToString());
                        xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", nKey, "']"));
                        if (xnDocumento != null)
                        {
                            c = GruppoInElaborazione.Database.ElencoPagineDocumenti[nKey];
                            if (!c.GiaEseguitaAnteprima)
                            {
                                foreach (string cValue in xnDocumento.SelectSingleNode("parzia").InnerText.Split(';'))
                                {
                                    //c.PagineDocumento += int.Parse(cValue);
                                    c.FineStampaSubDocumento(int.Parse(cValue));
                                }
                                c.GiaEseguitaAnteprima = true;
                                //this.saTHRGA.DatiGruppo.ProgressivoPagine += c.PagineDocumento;
                                GruppoInElaborazione.FineStampaDocumento(nKey);
                            }
                            lRtn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lRtn = false;
                throw ex;
            }
            return lRtn;
        }

        public void Salva_DatiAnteprima()
        {
            XmlNode xnGruppo;
            string cPagineparziali;
            XmlNode xnDocumento;
            Boolean lDettaglio;

            try
            {
                if (GruppoInElaborazione.DatiGruppo.AccessoDati.MainTable.Columns.Contains("DEF_stampa_dettaglio"))
                    lDettaglio = Boolean.Parse(GruppoInElaborazione.DatiGruppo.AccessoDati.MainTable.Rows[0]["DEF_stampa_dettaglio"].ToString());
                else
                    lDettaglio = false;

                //
                // Ricerca presenza 3 cheksum di riferimento
                //
                xnGruppo = _xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GruppoInElaborazione.PlugIn_CheckSum, "' and @modulo='", GruppoInElaborazione.ModuloDaStampare.Modulo.ModuloCheckSum, "' and @datifissi='", GruppoInElaborazione.DatiFissi_CheckSum, "' and @dettaglio='", lDettaglio.ToString(), "']"));
                if (xnGruppo == null)
                {
                    xnGruppo = _xdFastPrint.CreateNode(XmlNodeType.Element, "anteprima", "");
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("nomemodulo", ""));
                    xnGruppo.Attributes["nomemodulo"].Value = GruppoInElaborazione.Modulo_Nome;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("plugin", ""));
                    xnGruppo.Attributes["plugin"].Value = GruppoInElaborazione.PlugIn_CheckSum;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("modulo", ""));
                    xnGruppo.Attributes["modulo"].Value = GruppoInElaborazione.ModuloDaStampare.Modulo.ModuloCheckSum;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("datifissi", ""));
                    xnGruppo.Attributes["datifissi"].Value = GruppoInElaborazione.DatiFissi_CheckSum;
                    xnGruppo.Attributes.Append(_xdFastPrint.CreateAttribute("dettaglio", ""));
                    xnGruppo.Attributes["dettaglio"].Value = lDettaglio.ToString();
                    _xdFastPrint.SelectSingleNode("fastprint").AppendChild(xnGruppo);
                }

                //
                //All'interno del gruppo ricerca la presenza del documento di cui devo salvare i dati di anteprima.
                //
                foreach (KeyValuePair<decimal, ContatoriPagineEntity> k in GruppoInElaborazione.Database.ElencoPagineDocumenti)
                {
                    xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", k.Key, "']"));
                    if (xnDocumento == null)
                    {
                        xnDocumento = _xdFastPrint.CreateNode(XmlNodeType.Element, "documento", "");
                        xnDocumento.Attributes.Append(_xdFastPrint.CreateAttribute("codice", ""));
                        xnDocumento.Attributes["codice"].Value = k.Key.ToString();
                        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "totali", ""));
                        xnDocumento.AppendChild(_xdFastPrint.CreateNode(XmlNodeType.Element, "parzia", ""));
                        xnGruppo.AppendChild(xnDocumento);
                    }

                    //
                    // Aggiorna i dati delle pagine.
                    //
                    xnDocumento.SelectSingleNode("totali").InnerText = k.Value.PagineDocumento.ToString();
                    cPagineparziali = "";
                    foreach (int nPagine in k.Value.PagineSubDocumenti)
                        cPagineparziali = String.Concat(cPagineparziali, nPagine, ";");
                    k.Value.GiaEseguitaAnteprima = true;
                    xnDocumento.SelectSingleNode("parzia").InnerText = cPagineparziali.Substring(0, cPagineparziali.Length - 1);
                }
                _xdFastPrint.Save(this.Files["FastPrintFIL"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
