﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ISC.Invoicer.ManageFusione;
using ISC.Invoicer40.Processi.Base;

namespace ISC.Invoicer40.Processi.Impl
{
    public class Processo_60_FusioneDocumenti :ProcessoBase
    {
        public Processo_60_FusioneDocumenti()
        {
            Logger = LogManager.GetLogger("THRFonDocLog");
            MotoreFusione = new CLS_motore_fusione();
            Stato = eStatoProcesso.STP_da_settare;
        }

        private CLS_motore_fusione MotoreFusione { get; set; }

        public override void EseguiAzioneProcesso()
        {
            throw new NotImplementedException();
        }
    }
}
