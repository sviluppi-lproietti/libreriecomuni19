﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Base;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;

namespace ISC.Invoicer40.Processi.Base
{
    public abstract class ProcessoBase
    {

        public enum eStatoProcesso
        {
            STP_nullo = 0,
            STP_creato = 10,
            STP_da_settare = 20,
            STP_in_settaggio = 30,
            STP_da_avviare = 30,
            STP_in_avvio = 40,
            STP_riprendi_elaborazione = 50,
            STP_in_elaborazione = 60,
            STP_scarica_dati = 70,
            STP_in_attesa = 80,
            STP_terminato = 90
        }

        public decimal Codice { get; set; }
        public string Descrizione { get; set; }
        private GruppoBase _DatiGruppo { get; set; }
        public GruppoBase DatiGruppo
        {
            get
            {
                return _DatiGruppo;
            }
            set
            {
                _DatiGruppo = value;
                _DatiGruppo.Stato = eStatoGruppo.STR_genera_anteprima;
            }
        }
        public eStatoProcesso Stato { get; set; }
        public eStatoProcesso StatoForzato { get; set; }
        public Boolean IsRunning { get { return Stato != eStatoProcesso.STP_terminato; } }

        protected log4net.ILog Logger { get; set; }

        public ProcessoBase()
        {
            Stato = eStatoProcesso.STP_creato;
            StatoForzato = eStatoProcesso.STP_nullo;
        }

        public abstract void EseguiAzioneProcesso();

    }
}
