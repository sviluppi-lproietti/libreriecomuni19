﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using log4net;
using ISC.Invoicer40.Processi.Base;
using ISC.Invoicer40.Processi.Impl;
using ISC.LibrerieComuni.InvoicerService;
using ISC.LibrerieComuni.ManageSessione;
using ISC.LibrerieComuni.InvoicerService.Dati.Impl;
using ISC.LibrerieComuni.OggettiComuni;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Base;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Enum;
using ISC.LibrerieComuni.InvoicerService.Gruppi.Impl;

namespace ISC.Invoicer40.Processi
{
    public class ProcessoOrchestrator
    {
        public enum eStatoOrchestrator
        {
            STO_creato = 10,
            STO_in_avvio = 20,
            STO_avvitato = 30,
            STO_concluso = 40
        }

        public Boolean StampaCartacea_Forzata { get; set; }
        public Boolean StampaOttica_Abilitata { get; set; }
        public Decimal StampaOttica_MaxPageXParte { get; set; }
        public String StampaOttica_SuffissoXParte { get; set; }
        public Boolean FastPrint_Abilita { get; set; }

        public String PlugIn_CheckSum { get; set; }
        public String DatiFissi_CheckSum { get; set; }
        public string DatiFissi_FileName { get; set; }

        // public String DatiFissi_CheckSum { get; set; }
        public GruppoBase GruppoDefault { get; set; }
        //public DatiStampaGenerale DatiMaster { get; set; }
        public GestioneSessione GSS { get; set; }
        public Dictionary<decimal, Base.ProcessoBase> ListaProcessiFigli { get; set; }
        public Dictionary<decimal, GruppoBase> ListaGruppiGestione { get; set; }
        public Dictionary<int, CLS_GestioneModuloStampare> ListaModuliDaStampare { get; set; }
        public DataTable QuickDataTable { get; set; }
        public eStatoOrchestrator Stato { get; set; }
        public CLS_FileFolderDic Folders { get; set; }
        public Decimal NumeroItemPerGruppo { get; set; }
        public Boolean AttesaAnteprima_ExistGruppi { get { return (ListaGruppiGestione.Where(v => v.Value.Stato == eStatoGruppo.STR_attesa_anteprima).Count() > 0); } }
        public GruppoBase AttesaAnteprima_PrimoGruppo { get { return ListaGruppiGestione.Where(v => v.Value.Stato == eStatoGruppo.STR_attesa_anteprima).First().Value; } }

        public Boolean IsRunning_Proc_10_PreparaGruppi
        {
            get { return (ListaProcessiFigli.ContainsKey(10)) ? ListaProcessiFigli[10].IsRunning : false; }
        }

        protected log4net.ILog Logger { get; set; }

        public ProcessoOrchestrator(int TipoProcessoStampa)
        {
            Logger = LogManager.GetLogger("THROrchestratorLog");
            Logger.Info("Processo Orchestrator creato");
            Stato = eStatoOrchestrator.STO_creato;

            try
            {
                Logger.Debug("INIZIO - Creazione dei processi figlio.");
                ListaProcessiFigli = new Dictionary<decimal, Base.ProcessoBase>
                {
                    { 10, new Processo_10_CaricamentoDati() },
                    { 20, new Processo_20_AnteprimaStampa() }
                };

                //if (TipoProcessoStampa == 1)
                //{
                //    Logger.Debug("Processo di stampa cartacea. Aggiungo il processo figlio.");
                //    ListaProcessiFigli.Add(40, new Processo_40_StampaCartacea());
                //}
                //else if (TipoProcessoStampa == 2 || TipoProcessoStampa == 3)
                //{
                //    Logger.Debug("Processo di stampa elettronica. Aggiungo il processo figlio.");
                //    ListaProcessiFigli.Add(50, new Processo_50_StampaElettronica());
                //    if (TipoProcessoStampa == 3)
                //    {
                //        Logger.Debug("Processo di stampa elettronica con fusione dei documenti generati. Aggiungo il processo figlio.");
                //        ListaProcessiFigli.Add(60, new Processo_60_FusioneDocumenti());
                //    }
                //}
                Logger.Debug("Numero di processi figli da gestire: " + ListaProcessiFigli.Count.ToString());
                Logger.Debug("FINE - Creazione dei processi figlio.");

                ListaGruppiGestione = new Dictionary<decimal, GruppoBase>();
            }
            catch (Exception ex)
            {
                Logger.Error("Errore nella fase di costruzione del processo ORCHESTRATOR", ex);
            }
        }

        public void OrchestraProcessi()
        {
            DateTime dAppo = System.DateTime.Now;
            Logger.Info("INIZO - Procedura OrchestraProcessi per la gestione del processo.");
            while (ListaProcessiFigli.Count > 0)
            {
                Logger.Debug("  > Numero gruppi da gesture: " + ListaGruppiGestione.Count.ToString());
                foreach (ProcessoBase p in ListaProcessiFigli.Values)
                {
                    Logger.Debug("INIZIO - Gestione del processo: (" + p.Codice.ToString() + ") " + p.Descrizione);
                    Logger.Debug("   Stato: " + p.Stato.ToString());
                    if (p.Stato == ProcessoBase.eStatoProcesso.STP_da_settare)
                    {
                        p.Stato = ProcessoBase.eStatoProcesso.STP_in_settaggio;
                        if (p.Codice == 10)
                        {
                            //((Processo_10_CaricamentoDati)p).GruppoDefault = this.GruppoDefault;
                            ((Processo_10_CaricamentoDati)p).StampaCartacea_Forzata = this.StampaCartacea_Forzata;
                            ((Processo_10_CaricamentoDati)p).StampaOttica_Abilitata = this.StampaOttica_Abilitata;
                            ((Processo_10_CaricamentoDati)p).StampaOttica_MaxPageXParte = this.StampaOttica_MaxPageXParte;
                            ((Processo_10_CaricamentoDati)p).StampaOttica_SuffissoXParte = this.StampaOttica_SuffissoXParte;
                            ((Processo_10_CaricamentoDati)p).FastPrint_Abilita = this.FastPrint_Abilita;
                            ((Processo_10_CaricamentoDati)p).PlugIn_CheckSum = this.PlugIn_CheckSum;
                            ((Processo_10_CaricamentoDati)p).DatiFissi_CheckSum = this.DatiFissi_CheckSum;
                            ((Processo_10_CaricamentoDati)p).DatiFissi_FileName = this.DatiFissi_FileName;
                            
                            ((Processo_10_CaricamentoDati)p).GSS = this.GSS;
                            //((Processo_10_CaricamentoDati)p).DatiMaster = this.DatiMaster;
                            ((Processo_10_CaricamentoDati)p).ListaModuliDaStampare = this.ListaModuliDaStampare;
                            ((Processo_10_CaricamentoDati)p).LimiteScaricoGruppi = 20;
                            ((Processo_10_CaricamentoDati)p).NumeroItemPerGruppo = this.NumeroItemPerGruppo;
                        }
                        else if (p.Codice == 20)
                        {
                            //((Processo_20_AnteprimaStampa)p).DatiMaster = DatiMaster;
                            ((Processo_20_AnteprimaStampa)p).Stampante = "Sorte_PRN_Anteprima";
                            ((Processo_20_AnteprimaStampa)p).Folders = Folders;
                            ((Processo_20_AnteprimaStampa)p).Files = GSS.Files;
                        }
                        p.Stato = ProcessoBase.eStatoProcesso.STP_da_avviare;
                    }
                    else if (p.Stato == ProcessoBase.eStatoProcesso.STP_da_avviare && p.Codice > 0)
                    {
                        System.Threading.Thread thr = new System.Threading.Thread(p.EseguiAzioneProcesso);
                        thr.Start();
                        p.Stato = ProcessoBase.eStatoProcesso.STP_in_avvio;
                    }
                    else if (p.Stato == ProcessoBase.eStatoProcesso.STP_scarica_dati)
                    {
                        if (p.Codice == 10)
                        {
                            Logger.Debug("Aggiungo i gruppi da gestire. Sono " + ((Processo_10_CaricamentoDati)p).ListaGruppiCreati.Count.ToString());
                            foreach (GruppoBase s in ((Processo_10_CaricamentoDati)p).ListaGruppiCreati)
                            {
                                while (ListaGruppiGestione.ContainsKey(s.Chiave))
                                    s.Chiave += 1;
                                s.Stato = eStatoGruppo.STR_attesa_anteprima;
                                ListaGruppiGestione.Add(s.Chiave, s);
                            }
                            ((Processo_10_CaricamentoDati)p).ListaGruppiCreati.Clear();
                            p.Stato = ProcessoBase.eStatoProcesso.STP_riprendi_elaborazione;
                            Logger.Debug("Aggiunti gruppi da gestire. Il numero dei gruppi da gestire ora è di " + ListaGruppiGestione.Count.ToString());
                        }
                        else if (p.Codice == 20)
                        {
                            p.Stato = ProcessoBase.eStatoProcesso.STP_in_attesa;
                        }
                    }
                    else if (p.Stato == ProcessoBase.eStatoProcesso.STP_in_attesa && p.StatoForzato == ProcessoBase.eStatoProcesso.STP_nullo)
                    {
                        if (p.Codice == 10)
                        {
                            if ((System.DateTime.Now - dAppo).TotalSeconds > 10)
                            {
                                //ListaGruppiGestione.Remove(ListaGruppiGestione.First().Key);
                                //ListaGruppiGestione.Remove(ListaGruppiGestione.First().Key);
                                p.StatoForzato = ProcessoBase.eStatoProcesso.STP_riprendi_elaborazione;
                            }
                        }
                        else if (p.Codice == 20)
                        {
                            if (this.AttesaAnteprima_ExistGruppi)
                            {
                                ((Processo_20_AnteprimaStampa)p).DatiGruppo = AttesaAnteprima_PrimoGruppo;
                                p.StatoForzato = ProcessoBase.eStatoProcesso.STP_in_elaborazione;
                            }
                        }
                    }
                    if (p.Codice == 10 && p.Stato != ProcessoBase.eStatoProcesso.STP_in_attesa && ListaGruppiGestione.Count > 30)
                    {
                        p.StatoForzato = ProcessoBase.eStatoProcesso.STP_in_attesa;
                        dAppo = System.DateTime.Now;
                    }

                    if (p.Codice == 20 && p.Stato == ProcessoBase.eStatoProcesso.STP_in_attesa && !IsRunning_Proc_10_PreparaGruppi)
                    {
                        p.StatoForzato = ProcessoBase.eStatoProcesso.STP_terminato;
                    }
                    Logger.Debug("FINE - Gestione del processo: (" + p.Codice.ToString() + ") " + p.Descrizione);
                }

                //
                // Rimozione di eventuali processi terminati
                //
                ListaProcessiFigli = ListaProcessiFigli.Where(kvp => kvp.Value.Stato != ProcessoBase.eStatoProcesso.STP_terminato).ToDictionary(k => k.Key, v => v.Value);
                foreach (GruppoBase g in ListaGruppiGestione.Values)
                    Logger.Debug("Codice gruppo: " + g.Chiave.ToString() + " Stato: " + g.Stato.ToString());
                System.Threading.Thread.Sleep(1000);
            }
            Logger.Info("FINE - Procedura OrchestraProcessi per la gestione del processo.");
        }
    }
}
