﻿'
' In questa classe vengono dichiarati tutti gli enumeratori utilizzati in questo progetto
'

Namespace PlugIn

    Public Enum eVersionePlugIn
        No_versione = -1
        Versione_1_0_0 = 1
        Versione_1_1_0 = 2
        Versione_1_2_0 = 3
        Versione_1_3_0 = 4
        Versione_1_3_1 = 5
        Versione_1_3_2 = 6
        Versione_1_4_0 = 7
        Versione_1_4_1 = 8
        Versione_1_4_2 = 9
        Versione_1_4_3 = 10
        Versione_1_5_0 = 11
        Versione_1_5_1 = 12
        Versione_1_6_0 = 13
        Versione_1_6_1 = 14
        Versione_2_0_0 = 15
        Versione_3_0_0 = 16
    End Enum

End Namespace