﻿Imports ISC.LibrerieComuni.ManageSessione.MSE_Costanti
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Namespace PlugIn

    Public Class CLS_plugin_v1_6_0
        Inherits CLS_PluginBase
        Implements IPlugIn

        Private ReadOnly Property _PlugIn As PLUGIN_interfaceV1_6_0.IPLG_dati_lib

            Get
                Return CType(PluginBase, PLUGIN_interfaceV1_6_0.IPLG_dati_lib)
            End Get

        End Property

        Public Sub New(ByVal p As Object, ByVal fo As CLS_FileFolderDic, ByVal fog As CLS_FileFolderDic, ByVal fi As CLS_FileFolderDic, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))
            MyBase.New(p, fo, fog, fi, ol)

            Versione = eVersionePlugIn.Versione_1_6_0

        End Sub

        Public Overrides ReadOnly Property Descrizione As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

        Public Overrides ReadOnly Property NomePlugin As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTable() As DataTable

            Get
                Return _PlugIn.FullDataset.Tables(_PlugIn.FullDataSet_MainTableName)
            End Get

        End Property

        Public Overrides ReadOnly Property LinkField_QuickDS_FullDS() As String

            Get
                Return _PlugIn.LinkField_QuickDS_FullDS()
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataset() As DataSet

            Get
                Return _PlugIn.FullDataset
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTableName() As String

            Get
                Return _PlugIn.FullDataSet_MainTableName
            End Get

        End Property

        Public Overrides Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer, ByVal cCodiceServizio As String)

            _PlugIn.LoadFullDataset(nRecordDaCaricare)

        End Sub

        Public Overrides ReadOnly Property MessaggioAvviso() As String

            Get
                Return _PlugIn.MessaggioAvviso
            End Get

        End Property

        Public Overrides ReadOnly Property CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Get
                Return _PlugIn.CheckCondPSA(nCond2Check, cValue2Check)
            End Get

        End Property

        Public Overrides Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

            _PlugIn.ExecActionPSA(nAct2Exec, cValue2Exec)

        End Sub

        Public Overrides ReadOnly Property CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Get
                Return _PlugIn.CheckCondAPI(nCond2Check, cValue2Check)
            End Get

        End Property

        Public Overrides Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

            _PlugIn.ExecActionAPI(cTipoExecAPI, nAct2Exec, cValue2Exec)

        End Sub

        Public Overrides WriteOnly Property AlimentazioneImbustatrice() As String

            Set(ByVal value As String)
                _PlugIn.FeedString = value
            End Set

        End Property

        Public Overrides Sub GoToRecordNumber(ByVal nRow As Integer)

            _PlugIn.GoToRecordNumber(nRow)

        End Sub

        Public Overrides Sub AggiornaQuickDataset()

            _PlugIn.UpdateQuickDatasetFile()

        End Sub

        Public Overrides Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean

            Return _PlugIn.CheckDatiSessione(_DatiSessione)

        End Function

        Public Overrides ReadOnly Property DatiAzioniPostStampa() As ArrayList

            Get
                Return _PlugIn.DatiAzioniPostStampa
            End Get

        End Property

        Public Overrides Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)

            _PlugIn.SetSelectable(dr, cFilter)

        End Sub

        Public Overrides Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

            _PlugIn.SetPrintable(dr, cSelectFromPlugInType, cParamSelez)

        End Sub

        Public Overrides ReadOnly Property ListaControlliFormNuovaSessione()

            Get
                Return _PlugIn.ListaControlliFormNuovaSessione
            End Get

        End Property

        Public Overrides ReadOnly Property ElementiMenuDedicati() As ArrayList

            Get
                Return _PlugIn.ElementiMenuDedicati()
            End Get

        End Property

        Public Overrides Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

            _PlugIn.ImpostaDGVDati(DGV)

        End Sub

        Public Overrides Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

            Return _PlugIn.GetSelectedRow(_TipoRic, _ValueRic)

        End Function

        Public Overrides WriteOnly Property FiltroSelezione() As String

            Set(ByVal value As String)
                _PlugIn.FiltroSelezione = value
            End Set

        End Property

        Public Overrides WriteOnly Property DBGiriConnectionString() As String

            Set(ByVal value As String)
                _PlugIn.DBGiriConnectionString = value
            End Set

        End Property

        Public Overrides Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)

            Try
                If _DatiSessione Is Nothing Then
                    _DatiSessione = New ArrayList
                    For Each xnItem As Xml.XmlNode In xdSessione.SelectNodes("sessione/dati/dati")
                        _DatiSessione.Add(New String() {xnItem.Attributes("tipo").Value, xnItem.InnerXml})
                    Next
                End If
                _PlugIn.CreaDataBaseSessione(_DatiSessione, "", "")
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Overrides WriteOnly Property ListaGiri() As ArrayList

            Set(ByVal value As ArrayList)
                _PlugIn.ListaGiri = value
            End Set

        End Property

        Public Overrides ReadOnly Property NuoviToponimi() As Boolean

            Get
                Return _PlugIn.NuoviToponimi
            End Get

        End Property

        Public Overrides Function ValidaFile(ByVal aValidaFile As ArrayList) As Boolean

            _DatiCreazioneSessione = aValidaFile
            Return _PlugIn.ValidaFileSessione(aValidaFile)

        End Function

        Public Overrides ReadOnly Property ErrorePlugIn() As String

            Get
                Return _PlugIn.ErrorePlugIn
            End Get

        End Property

        Public Overrides WriteOnly Property DatiCreazioneSessioneXML() As Xml.XmlDocument

            Set(ByVal value As Xml.XmlDocument)
                _DatiCreazioneSessione = New ArrayList
                For Each xnNode As Xml.XmlNode In value.SelectNodes("datisessione/dati")
                    _DatiCreazioneSessione.Add(New String() {xnNode.Attributes("tipo").Value, xnNode.InnerText})
                Next
            End Set

        End Property

        Public Overrides Sub CreaSessioneSmart()
            Dim xdSessioneSmart As Xml.XmlDocument
            Dim xiWork As Xml.XmlNode
            Dim lCreaNodo As Boolean
            Dim nParam As Integer
            Dim nExist As Integer
            Dim cAppo As String
            Dim cParamValue As String

            Try
                xdSessioneSmart = New Xml.XmlDocument
                xdSessioneSmart.AppendChild(xdSessioneSmart.CreateElement("datisessione"))
                _DatiCreazioneSessione.Sort(New CLS_Ordina_DatiCreazioneSessione)
                For Each aValue As String() In _DatiCreazioneSessione
                    nParam = aValue(0)
                    lCreaNodo = True
                    cParamValue = ""
                    If nParam = 1 Or nParam = 2 Or nParam = 3 Or nParam = 52 Then
                        cParamValue = aValue(1)
                    ElseIf nParam = 51 Then
                        _SessioneSmartFLD = StandardFolderValue(aValue(1))
                        cAppo = _SessioneSmartFLD.Substring(0, _SessioneSmartFLD.Length - 1)
                        nExist = 2
                        While System.IO.Directory.Exists(cAppo)
                            cAppo = String.Concat(_SessioneSmartFLD.Substring(0, _SessioneSmartFLD.Length - 1), " (", nExist, ")")
                            nExist += 1
                        End While
                        _SessioneSmartFLD = StandardFolderValue(cAppo)
                        If Not System.IO.Directory.Exists(_SessioneSmartFLD) Then
                            System.IO.Directory.CreateDirectory(_SessioneSmartFLD)
                        End If
                        lCreaNodo = False
                    ElseIf nParam = 101 Then
                        System.IO.File.Copy(aValue(1), String.Concat(_SessioneSmartFLD, System.IO.Path.GetFileName(aValue(1))))
                        aValue(1) = System.IO.Path.GetFileName(aValue(1))
                        cParamValue = aValue(1)
                    End If
                    If lCreaNodo Then
                        xiWork = xdSessioneSmart.CreateElement("dati")
                        xiWork.InnerText = cParamValue
                        xiWork.Attributes.Append(xdSessioneSmart.CreateAttribute("tipo"))
                        xiWork.Attributes("tipo").Value = aValue(0)
                        xdSessioneSmart.SelectSingleNode("datisessione").AppendChild(xiWork)
                    End If
                Next
                xdSessioneSmart.Save(String.Concat(_SessioneSmartFLD, "datisessione.xml"))
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Overrides WriteOnly Property SezioneDati() As Xml.XmlNode

            Set(ByVal value As Xml.XmlNode)

            End Set

        End Property

        'Public Property Files As OggettiComuni.CLS_FileFolderDic Implements IPlugIn.Files
        '    Get

        '    End Get
        '    Set(ByVal value As OggettiComuni.CLS_FileFolderDic)

        '    End Set
        'End Property

        'Public Property Folders As OggettiComuni.CLS_FileFolderDic Implements IPlugIn.Folders
        '    Get

        '    End Get
        '    Set(ByVal value As OggettiComuni.CLS_FileFolderDic)

        '    End Set
        'End Property

        'Public Property FoldersGlobali As OggettiComuni.CLS_FileFolderDic Implements IPlugIn.FoldersGlobali
        '    Get

        '    End Get
        '    Set(ByVal value As OggettiComuni.CLS_FileFolderDic)

        '    End Set
        'End Property

        'Public Property OleDB_ConnString As System.Collections.Generic.Dictionary(Of String, OggettiComuni.CLS_oledbconnstring) Implements IPlugIn.OleDB_ConnString
        '    Get

        '    End Get
        '    Set(ByVal value As System.Collections.Generic.Dictionary(Of String, OggettiComuni.CLS_oledbconnstring))

        '    End Set
        'End Property

        'Public ReadOnly Property SessioneSmartFLD As String Implements IPlugIn.SessioneSmartFLD
        '    Get

        '    End Get
        'End Property
    End Class

End Namespace