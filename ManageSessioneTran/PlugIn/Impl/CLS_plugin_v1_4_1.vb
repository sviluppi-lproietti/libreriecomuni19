﻿Imports ISC.LibrerieComuni.ManageSessione.MSE_Costanti
Imports ISC.LibrerieComuni.OggettiComuni
imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Namespace PlugIn

    Public Class CLS_plugin_v1_4_1
        Inherits CLS_PluginBase
        Implements IPlugIn

        Private ReadOnly Property _PlugIn As PLUGIN_interfaceV1_4_1.IPLG_dati_lib

            Get
                Return CType(PluginBase, PLUGIN_interfaceV1_4_1.IPLG_dati_lib)
            End Get

        End Property

        Public Sub New(ByVal p As Object, ByVal fo As CLS_FileFolderDic, ByVal fog As CLS_FileFolderDic, ByVal fi As CLS_FileFolderDic, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))
            MyBase.New(p, fo, fog, fi, ol)

            Versione = eVersionePlugIn.Versione_1_4_1

        End Sub

        Public Overrides ReadOnly Property Descrizione As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

        Public Overrides ReadOnly Property NomePlugin As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTable() As DataTable

            Get
                Return _PlugIn.FullDataset.Tables(_PlugIn.FullDataSet_MainTableName)
            End Get

        End Property

        Public Overrides ReadOnly Property LinkField_QuickDS_FullDS() As String

            Get
                Return _PlugIn.LinkField_QuickDS_FullDS()
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataset() As DataSet

            Get
                Return _PlugIn.FullDataset
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTableName() As String

            Get
                Return _PlugIn.FullDataSet_MainTableName
            End Get

        End Property

        Public Overrides Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer, ByVal cCodiceServizio As String)

            _PlugIn.LoadFullDataset(nRecordDaCaricare)

        End Sub

        Public Overrides ReadOnly Property MessaggioAvviso() As String

            Get
                Return _PlugIn.MessaggioAvviso
            End Get

        End Property

        Public Overrides WriteOnly Property AlimentazioneImbustatrice() As String

            Set(ByVal value As String)
                _PlugIn.FeedString = value
            End Set

        End Property

        Public Overrides Sub GoToRecordNumber(ByVal nRow As Integer)

            _PlugIn.GoToRecordNumber(nRow)

        End Sub

        Public Overrides Sub AggiornaQuickDataset()

            _PlugIn.UpdateQuickDatasetFile()

        End Sub

        Public Overrides Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean

            Return _PlugIn.CheckDatiSessione(_DatiSessione)

        End Function

        Public Overrides Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)

            _PlugIn.SetSelectable(dr, cFilter)

        End Sub

        Public Overrides Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

            _PlugIn.SetPrintable(dr, cSelectFromPlugInType)

        End Sub

        Public Overrides ReadOnly Property ListaControlliFormNuovaSessione()

            Get
                Return _PlugIn.ListaControlliFormNuovaSessione
            End Get

        End Property

        Public Overrides ReadOnly Property ElementiMenuDedicati() As ArrayList

            Get
                Return _PlugIn.ElementiMenuDedicati()
            End Get

        End Property

        Public Overrides Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

            _PlugIn.ImpostaDGVDati(DGV)

        End Sub

        Public Overrides Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

            Return _PlugIn.GetSelectedRow(_TipoRic, _ValueRic)

        End Function

        Public Overrides WriteOnly Property FiltroSelezione() As String

            Set(ByVal value As String)
                _PlugIn.FiltroSelezione = value
            End Set

        End Property

        Public Overrides Function PostStampa() As ArrayList

            Return _PlugIn.PostStampa

        End Function

        Public Overrides WriteOnly Property SezioneDati() As Xml.XmlNode

            Set(ByVal value As Xml.XmlNode)
                _PlugIn.SezioneDati = value
            End Set

        End Property

        Public Overrides Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)

            Try
                _PlugIn.CreaDataBaseSessione(_DatiSessione)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Overrides ReadOnly Property Modello As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

    End Class

End Namespace