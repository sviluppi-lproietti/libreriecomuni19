﻿Imports ISC.LibrerieComuni.ManageSessioneTran.Modulo
Imports ISC.LibrerieComuni.OggettiComuni

Namespace PlugIn

    Public Class CLS_plugin_v1_0_0
        Inherits CLS_PluginBase
        Implements Iplugin

        Public ReadOnly Property _PlugIn As PLUGIN_interfaceV1_0.IPLG_dati_lib

            Get
                Return CType(PluginBase, PLUGIN_interfaceV1_0.IPLG_dati_lib)
            End Get

        End Property

        Public Sub New(ByVal p As Object, ByVal fo As CLS_FileFolderDic, ByVal fog As CLS_FileFolderDic, ByVal fi As CLS_FileFolderDic, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))
            MyBase.New(p, fo, fog, fi, ol)

            Versione = eVersionePlugIn.Versione_1_0_0

        End Sub

        Public Overrides ReadOnly Property Descrizione As String

            Get
                Return _PlugIn.PlugInName
            End Get

        End Property

        Public Overrides ReadOnly Property NomePlugin As String

            Get
                Return _PlugIn.PlugInName
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTable() As DataTable

            Get
                Return _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo))
            End Get

        End Property

        Public Overrides ReadOnly Property LinkField_QuickDS_FullDS() As String

            Get
                Return _PlugIn.PlugInMainLinkToQD(CodiceModulo)
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataset() As DataSet

            Get
                Return _PlugIn.PrintDataset
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTableName() As String

            Get
                Return _PlugIn.PlugInMainTable(CodiceModulo)
            End Get

        End Property

        Public Overrides Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer, ByVal cCodiceServizio As String)
            Dim dr_2 As DataRow
            Dim i As Integer
            Dim j As Integer

            _PlugIn.LoadPrintDataset(nRecordDaCaricare)
            ' In questo caso dobbiamo aggiungere il campo DEF_alimimb al dataset nella tabella principale

            With _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo)).Columns
                .Add(New DataColumn("DEF_alimimb"))
                .Add(New DataColumn("DEF_raccolta", System.Type.GetType("System.Int32")))
            End With

            If _PlugIn.FeedArray.Count = 0 Then
                For Each dr As DataRow In _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo)).Rows
                    dr("DEF_raccolta") = 9
                Next
            Else
                i = 0
                j = 0
                For Each dr_1 As DataRow In _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo)).Rows
                    dr_2 = _PlugIn.QuickDataset.Tables(0).Select(String.Concat(_PlugIn.PlugInMainTableKey(0), " = '", dr_1(_PlugIn.PlugInMainTableKey(CodiceModulo)), "'"))(0)
                    If dr_2("DEF_errcode") = 0 Or dr_2("DEF_errcode") = -2 Then
                        _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo)).Rows(j).Item("DEF_alimimb") = _PlugIn.FeedArray(i)
                        _PlugIn.PrintDataset.Tables(_PlugIn.PlugInMainTable(CodiceModulo)).Rows(j).Item("DEF_raccolta") = 9
                        i += 1
                    End If
                    j += 1
                Next
            End If
            _PlugIn.PrintDataset.AcceptChanges()

        End Sub

        Public Overrides ReadOnly Property MessaggioAvviso() As String

            Get
                Dim cValue As String

                cValue = ""
                If CodiceModulo <> eCodiceModuloFix.MOD_SendEmail Then
                    cValue = _PlugIn.AlertPlugIn
                End If
                Return cValue
            End Get

        End Property

        'Public Overrides ReadOnly Property CurrentRecordNumber() As Integer

        '    Get
        '        Return _PlugIn.CurrentRecordNumber
        '    End Get

        'End Property

        Public Overrides WriteOnly Property AlimentazioneImbustatrice() As String

            Set(ByVal value As String)
                _PlugIn.FeedString = value
            End Set

        End Property

        Public Overrides Sub GoToRecordNumber(ByVal nRow As Integer)

            _PlugIn.GoToRecordNumber(nRow)

        End Sub

        Public Overrides Sub AggiornaQuickDataset()

            _PlugIn.RemovePRNDSErrorRecord()

        End Sub

        Public Overrides Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean

            Return _PlugIn.CheckDatiSessione(_DatiSessione)

        End Function

        Public Overrides Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)

            _PlugIn.SetSelectable(dr)

        End Sub

        Public Overrides Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

            _PlugIn.SetPrintable(dr, cSelectFromPlugInType)

        End Sub

        Public Overrides ReadOnly Property ListaControlliFormNuovaSessione()

            Get
                Return _PlugIn.ListaControlliForm
            End Get

        End Property

        Public Overrides ReadOnly Property ElementiMenuDedicati() As ArrayList

            Get
                Return _PlugIn.GetSessioneMenuItem
            End Get

        End Property

        Public Overrides Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

            _PlugIn.ImpostaDGVDati(DGV)

        End Sub

        Public Overrides Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

            Return _PlugIn.GetSelectedRow(_TipoRic, _ValueRic)

        End Function

        Public Overrides ReadOnly Property LogDSStampa() As DataSet

            Get
                _PlugIn.LogDS = New DataSet
                Return _PlugIn.LogDS
            End Get

        End Property

        Public Overrides WriteOnly Property SezioneDati() As Xml.XmlNode

            Set(ByVal value As Xml.XmlNode)
                _PlugIn.SessioneDati = value
            End Set

        End Property

        Public Overrides Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)

            Try
                _PlugIn.CreaDBSessioneS1(_DatiSessione)
                _PlugIn.CreaDBSessioneS2()
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Overrides ReadOnly Property Modello As String

            Get
                Return _PlugIn.PlugInName
            End Get

        End Property
    End Class

End Namespace