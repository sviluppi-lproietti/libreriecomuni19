Imports AssemblyLoader
Imports ISC.LibrerieComuni.ManageSessioneTran.Entity
Imports ISC.LibrerieComuni.ManageSessione.PlugIn
Imports ISC.LibrerieComuni.ManageSessioneTran.MSE_Costanti
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports System.ComponentModel
Imports log4net

Namespace PlugIn

    Public MustInherit Class CLS_PluginBase
        Implements IPlugIn

        Private _TipoApplicazione As eTipoApplicazione
        Private _QuickDataset As DataSet

        Public Event PlugInCaricato()

        Private _PlugInAppDomain As AppDomain
        Private _SameAppDomain As Boolean

        Public Shadows PluginBase As Object

        <Description("Elenca i nomi dei file necessari per l'accesso ai dati del plugin.")> _
        Public Property Files As CLS_FileFolderDic

        <Description("Elenca i nomi delle cartelle necessari per l'accesso ai dati del plugin.")> _
        Public Property Folders As CLS_FileFolderDic

        Public Property FoldersGlobali As CLS_FileFolderDic
        Public Property OleDB_ConnString As Dictionary(Of String, CLS_oledbconnstring)

        Protected _DatiCreazioneSessione As ArrayList
        Protected _SessioneSmartFLD As String

        Public ReadOnly Property SessioneSmartFLD() As String

            Get
                Return _SessioneSmartFLD
            End Get

        End Property

        Public Sub New(ByVal p As Object, ByVal fo As CLS_FileFolderDic, ByVal fog As CLS_FileFolderDic, ByVal fi As CLS_FileFolderDic, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))

            Me.PluginBase = p
            Me.Folders = fo
            Me.Files = fi
            Me.FoldersGlobali = fog
            Me.OleDB_ConnString = ol

        End Sub

        Public Property Versione As eVersionePlugIn
        ' Public DBGiri_CS As String

#Region "ProprietÓ da ereditare nel plugin"

        'Public MustOverride ReadOnly Property CurrentRecordNumber() As Integer
        Public ReadOnly Property CurrentRecordNumber As Integer

            Get
                Return PluginBase.CurrentRecordNumber
            End Get

        End Property

        Public MustOverride ReadOnly Property Descrizione As String
        Public MustOverride ReadOnly Property ElementiMenuDedicati() As ArrayList
        Public MustOverride ReadOnly Property FullDataSet As DataSet
        Public MustOverride ReadOnly Property FullDataSet_MainTable() As DataTable
        Public MustOverride ReadOnly Property FullDataSet_MainTableName() As String
        Public MustOverride ReadOnly Property LinkField_QuickDS_FullDS() As String
        Public MustOverride ReadOnly Property ListaControlliFormNuovaSessione()
        Public MustOverride ReadOnly Property MessaggioAvviso() As String

        Public Property MessaggiProgress As MessaggiEntity

            Get
                Dim m As MessaggiEntity

                If Me.Versione < eVersionePlugIn.Versione_2_0_0 Then
                    m = Trasformazioni.MEPlugin1_X_X_TO_MESessione(PluginBase.MessaggiAvanzamento)
                ElseIf Me.Versione = eVersionePlugIn.Versione_2_0_0 Then
                    m = Trasformazioni.MEPlugin2_X_X_TO_MESessione(PluginBase.MessaggiProgress)
                Else
                    Throw New Exception("GESE-000007: Trasformazione dei messaggi non riuscita")
                End If
                Return m
            End Get
            Set(ByVal value As MessaggiEntity)
                If Me.Versione < eVersionePlugIn.Versione_2_0_0 Then
                    PluginBase.MessaggiAvanzamento = Trasformazioni.MESessione_TO_MEPlugin1_X_X(value)
                ElseIf Me.Versione = eVersionePlugIn.Versione_2_0_0 Then
                    PluginBase.MessaggiProgress = Trasformazioni.MESessione_TO_MEPlugin2_X_X(value)
                Else
                    Throw New Exception("GESE-000008: Trasformazione dei messaggi non riuscita")
                End If
            End Set

        End Property

        Public MustOverride WriteOnly Property AlimentazioneImbustatrice() As String
        Public MustOverride Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean
        Public MustOverride Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

        Public MustOverride Sub AggiornaQuickDataset()

        Public MustOverride Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer, ByVal cCodiceServizio As String)
        Public MustOverride Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)
        Public MustOverride Sub GoToRecordNumber(ByVal nRow As Integer)
        Public MustOverride Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

        Public MustOverride Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
        Public MustOverride Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

#End Region

        '
        ' Contiene il codice del modulo attivo.
        '
        Public Overridable Property CodiceModulo As Integer

            Get
                Return PluginBase.CodiceModulo
            End Get
            Set(ByVal value As Integer)
                PluginBase.CodiceModulo = value
            End Set

        End Property

        Shadows Property TipoApplicazione As eTipoApplicazione

            Get
                Return _TipoApplicazione
            End Get
            Set(ByVal value As eTipoApplicazione)
                _TipoApplicazione = value
                If _TipoApplicazione > 2 Then
                    ' _PlugIn.TipoCaricamento = 1
                Else
                    ' _PlugIn.TipoCaricamento = _TipoApplicazione
                End If
            End Set

        End Property

        Public MustOverride WriteOnly Property SezioneDati() As Xml.XmlNode

        Public ReadOnly Property CapacitaInvioEmail As Boolean

            Get
                Dim lRtn As Boolean

                If Me.Versione >= eVersionePlugIn.Versione_1_4_2 Then
                    lRtn = PluginBase.CapacitaInvioMail
                Else
                    lRtn = Me.Versione > eVersionePlugIn.Versione_1_3_2
                End If
                Return lRtn
            End Get

        End Property

        Public Overridable ReadOnly Property CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Get
                Return False
            End Get

        End Property

        Public Overridable Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

        End Sub

        Public Overridable ReadOnly Property CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Get
                Return False
            End Get

        End Property

        Public Overridable Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

        End Sub

        Public Overridable ReadOnly Property DatiAzioniPostStampa() As ArrayList

            Get
                Return New ArrayList
            End Get

        End Property

        Public Overridable WriteOnly Property FiltroSelezione() As String

            Set(ByVal value As String)
                ' ********************************************************************* '
                ' Nella versione 1.0.0 non era prevista la possibilitÓ di filtrare i re ' 
                ' cord della tabella pricipale del quickdataset.                        '
                ' ********************************************************************* '
            End Set

        End Property

        Public Overridable Function PostStampa() As ArrayList

            Return New ArrayList

        End Function

        Public Overridable ReadOnly Property LogDSStampa() As DataSet

            Get
                Return Nothing
            End Get

        End Property

        Public Overridable WriteOnly Property DBGiriConnectionString() As String

            Set(ByVal value As String)

            End Set

        End Property

        Public Overridable WriteOnly Property ListaGiri() As ArrayList

            Set(ByVal value As ArrayList)

            End Set

        End Property

        Public MustOverride ReadOnly Property NomePlugIn() As String

        Public Overridable ReadOnly Property NuoviToponimi() As Boolean

            Get
                Return False
            End Get

        End Property

        Public Overridable ReadOnly Property TipoPlugIn As String

            Get
                Return ""
            End Get

        End Property

        Public Overridable ReadOnly Property Modello As String

            Get
                Return ""
            End Get

        End Property

        Public Overridable Function ValidaFile(ByVal aValidaFile As ArrayList) As Boolean

            Return True

        End Function

        Public Overridable ReadOnly Property ErrorePlugIn() As String
            Get
                Return ""
            End Get
        End Property

        Public Overridable WriteOnly Property DatiCreazioneSessioneXML() As Xml.XmlDocument

            Set(ByVal value As Xml.XmlDocument)

            End Set

        End Property

        Public Overridable Sub CreaSessioneSmart()

        End Sub

        Public Overridable Property FileDati As ArrayList

            Get

            End Get
            Set(ByVal value As ArrayList)

            End Set

        End Property

        Public Overridable Function DeliveryDataTranslate(ByVal cField As String) As String

            Return ""

        End Function

        Public ReadOnly Property ResultAL As ArrayList

            Get
                Return PluginBase.ResultAl
            End Get

        End Property

        Public Sub Carica_QuickDataset() Implements IPlugIn.Carica_QuickDataset

            If Versione = eVersionePlugIn.No_versione Then
                Throw New Exception("Plug in non caricato.")
            ElseIf Versione = eVersionePlugIn.Versione_1_3_2 Or Versione = eVersionePlugIn.Versione_1_4_0 Then
                PluginBase.LoadQuickDataset(Me.TipoApplicazione)
            ElseIf Versione = eVersionePlugIn.Versione_1_6_0 Or Versione = eVersionePlugIn.Versione_1_6_1 Then
                PluginBase.LoadQuickDataset(Me.Files(MSE_Costanti.FIL_QuickDataSetIndex))
            Else
                PluginBase.LoadQuickDataset()
            End If

        End Sub

        Public ReadOnly Property QuickDataset() As DataSet Implements IPlugIn.QuickDataset

            Get
                If _QuickDataset Is Nothing Then
                    If Versione = eVersionePlugIn.Versione_2_0_0 Then
                        _QuickDataset = DeserializeDataset(PluginBase.Files("QuickDatasetSerializadFIL"))
                    Else
                        _QuickDataset = PluginBase.QuickDataset
                    End If
                End If
                Return _QuickDataset
            End Get

        End Property

        Public ReadOnly Property QuickDataset_MainTable() As DataTable Implements IPlugIn.QuickDataset_MainTable

            Get
                If Versione = eVersionePlugIn.Versione_1_0_0 Then
                    Return PluginBase.QuickDataset.Tables(PluginBase.PlugInMainTable(0))
                Else
                    Return PluginBase.QuickDataset.Tables(PluginBase.QuickDataset_MainTableName)
                End If
            End Get

        End Property

        Public ReadOnly Property QuickDataset_MainTableKey() As String Implements IPlugIn.QuickDataset_MainTableKey
            Get
                If Versione = eVersionePlugIn.Versione_1_0_0 Then
                    Return PluginBase.PlugInMainTableKey(0)
                Else
                    Return PluginBase.QuickDataset_MainTableKey
                End If
            End Get
        End Property

    End Class

End Namespace