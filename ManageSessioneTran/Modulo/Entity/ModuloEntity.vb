﻿Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Namespace Modulo

    Public Class ModuloEntity

        Public Property CassettoDefault As Integer
        Public Property Codice As Integer
        Public Property DaStampare As Boolean
        Public Property DaEsportazione As Boolean
        Public Property CanPrintPDF As Boolean

        Public Property FileName As String

            Get
                Dim cTmp As String

                If _FileName = "" Then
                    cTmp = ""
                Else
                    cTmp = ConcatenaFolderFileValue(Folder, _FileName)
                End If
                Return cTmp
            End Get
            Set(ByVal value As String)
                _FileName = value
                If _FileName.ToLower.StartsWith("moduli\") Then
                    _FileName = _FileName.Substring(7)
                End If
            End Set

        End Property

        Public Property Filtro As String
        Public Property ModuloUnificato As Boolean
        Public Property Nome As String
        Public Property Tipologia As eTipologiaModulo
        Public Property Folder As String
        Public Property AlimentazioneImbustatrice As String
        Public Property StampaFronteRetro As Boolean
        Public Property DisabilitaFronteRetro As Boolean

        Private _FileName As String

        Public Sub New(ByVal cFolder As String)

            Codice = 0
            Folder = cFolder

        End Sub

    End Class

End Namespace