Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_sezione
        Inherits CLS_elemento_base

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

            '
            ' Recupera l'item per il box se esiste e lo rimuove dall'item padre
            '
            BoxItem = GetXmlNodeFromXML(XMLItem, "item[@type='Boxed']")
            If Me.IsBoxed Then
                BoxItem.SelectSingleNode("Posizione/X").InnerText = Posizione("X") + BoxItem.SelectSingleNode("Posizione/X").Attributes("OffSet").Value
                BoxItem.SelectSingleNode("Posizione/Y").InnerText = Posizione("Y") + BoxItem.SelectSingleNode("Posizione/Y").Attributes("OffSet").Value
                XMLItem.RemoveChild(BoxItem)
            End If

        End Sub

        Public Sub SetUpBoxedItem(ByVal nMaxPrintedY As Decimal)

            BoxItem.SelectSingleNode("Dimensioni/X").InnerText = Posizione("X") + BoxItem.SelectSingleNode("Dimensioni/X").Attributes("OffSet").Value - BoxItem.SelectSingleNode("Posizione/X").InnerText
            BoxItem.SelectSingleNode("Dimensioni/Y").InnerText = nMaxPrintedY + BoxItem.SelectSingleNode("Dimensioni/Y").Attributes("OffSet").Value - BoxItem.SelectSingleNode("Posizione/Y").InnerText

        End Sub

        Public Property BoxItem As Xml.XmlNode

        Public ReadOnly Property IsBoxed As Boolean

            Get
                Return BoxItem IsNot Nothing
            End Get

        End Property

        Public ReadOnly Property AltezzaMinimaRichiesta As Decimal

            Get
                Return GetValueFromXML(Me.XMLItem, "AltezzaMinimaRichiesta", 0)
            End Get

        End Property

        Public ReadOnly Property ControlloSpazioNecessario As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "ControlloMargineBasso", 0) = 1
            End Get

        End Property

        Public ReadOnly Property TroppoLunga(ByVal nMargineBasso As Decimal) As Boolean

            Get
                Return Me.AltezzaMinimaRichiesta + Me.Posizione("Y") > nMargineBasso
            End Get

        End Property

        Public Sub RipristinaBox()

            XMLItem.AppendChild(_BoxItem)

        End Sub

    End Class

End Namespace