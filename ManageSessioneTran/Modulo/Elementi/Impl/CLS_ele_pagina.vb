Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_pagina
        Inherits CLS_elemento_base

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New(xi)

            '
            ' Rimuove i commenti dalla pagina. I Commenti non sono necesari.
            '
            PulisciDaiCommenti(Me.XMLItem)

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode, ByVal lClean As Boolean)
            MyBase.New(xi)

            If lClean Then
                '
                ' Rimuove i commenti dalla pagina. I Commenti non sono necesari.
                '
                PulisciDaiCommenti(Me.XMLItem)
            End If

        End Sub

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property PosizioneBaseXML As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("PosizioneBase").SelectSingleNode("Posizione").Clone
            End Get

        End Property

        ''' <summary>
        ''' Rimuove i commenti dalla pagina che sista stampando.
        ''' </summary>
        ''' <param name="xnItem">Contiene l'elemento dal quale rimuovere i commenti</param>
        ''' <remarks></remarks>
        Private Sub PulisciDaiCommenti(ByVal xnItem As Xml.XmlNode)
            'Dim i As Integer
            Dim allComments As Xml.XmlNodeList
            'i = 0
            'While i < xnItem.ChildNodes.Count - 1
            '    If xnItem.ChildNodes(i).NodeType = Xml.XmlNodeType.Comment Then
            '        xnItem.RemoveChild(xnItem.ChildNodes(i))
            '        i -= 1
            '    Else
            '        PulisciDaiCommenti(xnItem.ChildNodes(i))
            '    End If
            '    i += 1
            'End While
            allComments = xnItem.SelectNodes("//comment()")

            For Each n As Xml.XmlNode In allComments
                n.ParentNode.RemoveChild(n)
            Next

        End Sub

        ''' <summary>
        ''' Restituisce il cassetto dal quale prelevare la carta. Utile in fase di stampa del cartaceo.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Integer</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CassettoCarta As Integer

            Get
                Dim nTrayOfPage As Integer
                Dim cTrayOfPage As String

                cTrayOfPage = Me.XMLItem.SelectSingleNode("tray").InnerText.Replace("*", "")
                If IsNumeric(cTrayOfPage) Then
                    nTrayOfPage = cTrayOfPage
                Else
                    nTrayOfPage = cTrayOfPage
                End If
                Return nTrayOfPage
            End Get

        End Property

        ''' <summary>
        ''' Indica se la pagina in questione � generata come vuota o meno.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property PaginaVuota As Boolean

            Get
                Return GetAttrFromXML(Me.XMLItem, "PaginaVuota", 0) = 1
            End Get

        End Property

        ''' <summary>
        ''' Indica se la pagina in questione deve essere stampata obbligatoriamente su pagina Dispari.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ForzaSuPaginaDispari As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "tray", "0").Contains("*")
            End Get

        End Property

        Public ReadOnly Property RiportaElementiTuttePagine As Boolean

            Get
                Return GetValueFromXML(Me.XMLItem, "NoAllPageItems", "0") = 0
            End Get

        End Property

        Public Sub ImpostaMargineBasso(ByVal nMargineBassoDef As Decimal)

            MargineBasso = GetValueFromXML(Me.XMLItem, "MaxPageLenght", nMargineBassoDef)

        End Sub

        ''' <summary>
        ''' Indca il valore del margine basso della pagina.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MargineBasso As Decimal

    End Class

End Namespace