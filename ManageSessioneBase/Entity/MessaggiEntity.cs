﻿using System;

namespace ISC.LibrerieComuni.ManageSessioneBase.Entity
{
    public class MessaggiEntity
    {
        public string TipoOperazionePrinc { get; set; }
        public int ProgressivoPrinc { get; set; }
        public int MassimoPrinc { get; set; }
        public string TipoOperazioneSecon { get; set; }
        public int ProgressivoSecon { get; set; }
        public int MassimoSecon { get; set; }

        public Boolean Abilitati { get; set; }
        public Boolean MessaggiDalPlugin { get; set; }

        public MessaggiEntity()
        {

            Abilitati = false;

        }
    }
}