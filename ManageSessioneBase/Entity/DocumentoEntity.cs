﻿namespace ISC.LibrerieComuni.ManageSessioneBase.Entity
{
    public class DocumentoEntity
    {
        public int DOC_codice { get; set; }
        public int DOC_cod_mod { get; set; }
        public string DOC_filename { get; set; }
    }
}