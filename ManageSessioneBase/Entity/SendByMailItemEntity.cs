﻿using System.Collections.Generic;

namespace ISC.LibrerieComuni.ManageSessioneBase.Entity
{
    public class SendByMailItemEntity
    {
        public SendByMailItemEntity()
        {
            ListaDocumenti = new List<DocumentoEntity>();
        }

        public int MAI_codice { get; set; }
        public List<DocumentoEntity> ListaDocumenti { get; set; }

    }
}