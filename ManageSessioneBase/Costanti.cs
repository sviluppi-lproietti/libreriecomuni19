﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.ManageSessioneBase
{
    public static class Costanti
    {
        public const string FIL_Sessione = "SessioneFIL";
        public const string FIL_PlugIn = "PlugInFIL";
        public const string FIL_PlugInDatiFissi = "PlugInDatiFissiFIL";

        public const string FLD_SessioneRoot = "SessioneRootFLD";
        public const string FLD_moduli = "ModuliFLD";
    }
}
