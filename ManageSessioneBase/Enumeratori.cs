﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.ManageSessioneBase
{
    public static class Enumeratori
    {
        public enum eTipoApplicazione
        {
            TAP_Invoicer = 1,
            TAP_InvoicerSender = 2,
            TAP_SorteCert_SRV = 5
        }

        public enum eTipoDocumenti
        {
            DocumentoUnificato = 900,
            DocumentoAllegato = 901
        }
    }
}
