﻿using System;
using System.Reflection;
using System.Collections;
using log4net;

namespace ISC.LibrerieComuni.ManageSessioneBase
{
    [Serializable()]
    public class AssemblyLoader : MarshalByRefObject
    {
        public log4net.ILog _logger = LogManager.GetLogger("GestioneSessioneLog");

        public int VersionePlugIn { get; set; }

        public object CreateInstance(string assemblyName)
        {
            ArrayList aInterface;
            int nInterface;
            
            try
            {
                VersionePlugIn = 0;
                aInterface = new ArrayList
                {
                    "PLUGIN_interfaceV1_0.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_1.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_2.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_3_0.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_3_1.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_3_2.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_4.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_4_1.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_4_2.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_4_3.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_5_0.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_5_1.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_6_0.IPLG_dati_lib",
                    "PLUGIN_interfaceV1_6_1.IPLG_dati_lib",
                    "PLUGIN_interfaceV2_0_0.IPLG_dati_lib",
                    "PlugINs.PlugIN_2_0_0"
                };
                _logger.Debug("AssemblyLoader - CreateInstance - Plugin da caricare: " + assemblyName);
                var dll = Assembly.LoadFile(assemblyName);
                _logger.Debug("AssemblyLoader - CreateInstance - Plugin da caricato: " + assemblyName);

                foreach (Type tipoInt in dll.GetExportedTypes())
                {
                    nInterface = 1;
                    foreach (string cInterfaceName in aInterface)
                    {
                        _logger.Debug("AssemblyLoader - CreateInstance - Verifica la presenza dell'interfaccia: " + cInterfaceName);
                        if (tipoInt.GetInterface(cInterfaceName) != null)
                        {
                            _logger.Debug("AssemblyLoader - CreateInstance verifica la presenza dell'interfaccia: " + cInterfaceName);
                            VersionePlugIn = nInterface;
                            return Activator.CreateInstance(tipoInt);
                        }
                        nInterface += 1;
                    }
                }
                return null;
            }
            catch (BadImageFormatException ex)
            {
                _logger.Error("AssemblyLoader - CreateInstance - BadImageFormatException: ", ex);
                //Console.WriteLine("Unable to load {0}.", filePath);
                //Console.WriteLine(e.Message.Substring(0,
                //                  e.Message.IndexOf(".") + 1));
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error("AssemblyLoader - CreateInstance", ex);
                throw ex;
            }
        }
    }
}
