﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISC.LibrerieComuni.ManageSessioneBase.Entity;

namespace ISC.LibrerieComuni.ManageSessioneBase
{
    public static class FunzioniUtili
    {
        public static string EstraiNomeSessione(string cValue)
        {
            if (cValue.EndsWith("\\"))
                cValue = cValue.Substring(0, cValue.Length - 1);
            return cValue.Substring(cValue.LastIndexOf("\\") + 1);

        }

        public static MessaggiEntity TransformMessaggiIN(ManageSessioneTran.Entity.MessaggiEntity msgTrans)
        {
            MessaggiEntity m;

            m = new MessaggiEntity();
            m.Abilitati = msgTrans.Abilitati;
            m.MassimoPrinc = msgTrans.MassimoPrinc;
            m.MassimoSecon = msgTrans.MassimoSecon;
            m.MessaggiDalPlugin = true;
            m.ProgressivoPrinc = msgTrans.ProgressivoPrinc;
            m.ProgressivoSecon = msgTrans.ProgressivoSecon; ;
            m.TipoOperazionePrinc = msgTrans.TipoOperazionePrinc;
            m.TipoOperazioneSecon = msgTrans.TipoOperazioneSecon;
            return m;
        }

        public static ManageSessioneTran.Entity.MessaggiEntity TransformMessaggiOUT(MessaggiEntity msgTrans)
        {
            ManageSessioneTran.Entity.MessaggiEntity m;

            m = new ManageSessioneTran.Entity.MessaggiEntity();
            m.Abilitati = msgTrans.Abilitati;
            m.MassimoPrinc = msgTrans.MassimoPrinc;
            m.MassimoSecon = msgTrans.MassimoSecon;
            m.MessaggiDalPlugin = true;
            m.ProgressivoPrinc = msgTrans.ProgressivoPrinc;
            m.ProgressivoSecon = msgTrans.ProgressivoSecon;
            m.TipoOperazionePrinc = msgTrans.TipoOperazionePrinc;
            m.TipoOperazioneSecon = msgTrans.TipoOperazioneSecon;
            return m;
                        }
    }
}
