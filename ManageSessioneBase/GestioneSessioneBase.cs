﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

using ISC.LibrerieComuni.ManageSessioneBase.Entity;
using ISC.LibrerieComuni.ManageSessioneTran.Modulo;
using ISC.LibrerieComuni.ManageSessioneTran.PlugIn;
using ISC.LibrerieComuni.OggettiComuni;
using log4net;

namespace ISC.LibrerieComuni.ManageSessioneBase
{
    public abstract class GestioneSessioneBase
    {

        public log4net.ILog _logger = LogManager.GetLogger("GestioneSessioneLog");

        public Boolean CapacitaInvioEmail { get { return _PlugInSessione.CapacitaInvioEmail; } }
        public Boolean CheckDatiVar { get; set; }


        // ************************************************************************** //
        // Variabile utilizzata per la gesione di una sessioe di stampa.              //
        // Contiene il puntamento alla cartella che contiene la sessione in uso.      //
        // ************************************************************************** //
        public string Descrizione
        {
            get
            {
                if (_xdSessione == null)
                    return "Cartella non esistente o non contenente una sessione valida.";
                else
                {
                    if (_xdSessione.SelectSingleNode("sessione/descrizione") == null)
                        return "Descrizione della sessione non disponibile.";
                    else
                        return _xdSessione.SelectSingleNode("sessione/descrizione").InnerText;
                }
            }
            set
            {
                _xdSessione.SelectSingleNode("sessione/descrizione").InnerText = value;
            }

        }

        public Exception Errore { get; set; }
        public Boolean ExistModuloUnificato { get; set; }
        public CLS_FileFolderDic FoldersGlobali { get; set; }
        public CLS_FileFolderDic Folders { get; set; }
        public CLS_FileFolderDic Files { get; set; }

        public DataSet FullDataset { get { return _PlugInSessione.FullDataSet; } }
        public DataTable FullDataSet_MainTable { get { return _PlugInSessione.FullDataSet_MainTable; } }
        public String FullDataset_MainTableName { get { return _PlugInSessione.FullDataSet_MainTableName; } }

        public Boolean IsEmptyFullDataset { get { return this.FullDataSet_MainTable.Rows.Count == 0; } }

        //
        // Indica che la sessione è stata caricata correttamente.
        //
        public Boolean IsLoaded { get; set; }

        public string LinkField_QuickDS_FullDS { get { return _PlugInSessione.LinkField_QuickDS_FullDS; } }

        public string MessaggioAvviso { get { return _PlugInSessione.MessaggioAvviso; } }

        public MessaggiEntity MessaggiProgress
        {
            get
            {
                MessaggiEntity m;

                if (_MessaggiProgress.MessaggiDalPlugin)
                    m = FunzioniUtili.TransformMessaggiIN(_PlugInSessione.MessaggiProgress);
                else
                    m = _MessaggiProgress;
                return m;
            }

            set
            {
                _MessaggiProgress = value;
            }
        }

        //
        // Contiene il nome della sessione.
        //
        public string Nome { get; set; }
        public Dictionary<string, CLS_oledbconnstring> OleDb_ConnString { get; set; }

        public string PlugInName { get { return _PlugInSessione.NomePlugIn; } }

        public DataTable QuickDataTable { get { return _PlugInSessione.QuickDataset_MainTable; } }
        public string QuickDataset_MainTableKey { get { return _PlugInSessione.QuickDataset_MainTableKey; } }

        public string PlugInVersion { get { return _PlugInSessione.Versione.ToString().Replace("Versione_", "").Replace("_", "-"); } }

        public int QuickDataset_Record_Caricati { get { return _PlugInSessione.QuickDataset_MainTable.Rows.Count; } }
        public int QuickDataset_Record_ErroreStampa { get; set; }
        public int QuickDataset_Record_ErroriCaricamentoGravi { get; set; }
        public int QuickDataset_Record_ErroriCaricamentoLievi { get; set; }
        public int FullDataset_Record_MainTable { get { return FullDataSet_MainTable.Rows.Count; } }
        public int QuickDataset_Record_Selezionati { get; set; }

        // Tipo di applicazione che richiede l'apertura della sessione.
        // Può essere di stampa o di invio. 
        //
        public Enumeratori.eTipoApplicazione TipoApplicazione { get; set; }

        private MessaggiEntity _MessaggiProgress { get; set; }

        protected CLS_plugin_accesso _Access2PlugIn { get; set; }
        protected ModuloEntity _DatiModuloAttivo { get; set; }
        protected CLS_PluginBase _PlugInSessione { get; set; }
        protected AppDomain _PlugInAppDomain { get; set; }
        protected String _VersioneSistemaOperativo { get; set; }

        //
        // Contiene il dile xml che definisce la sessione.
        //
        protected System.Xml.XmlDocument _xdSessione { get; set; }

        public abstract void Attiva_ModuloSpecific();

        public void AttivaModulo(int nCodiceModulo)
        {
            Attiva_ModuloSpecific();
        }

        public GestioneSessioneBase(Enumeratori.eTipoApplicazione TipoAmbiente, CLS_FileFolderDic F, string cVersioneSistemaOperativo, Dictionary<String, CLS_oledbconnstring> ol)
        {


        }

        // ****************************************************************************************** //
        // Elenco dei metodi della classe.                                                            //
        // ****************************************************************************************** //
        public string Calculate_CheckSumPlugIn()
        {
            return OGC_utilita.HashFile(this.Files[Costanti.FIL_PlugIn]);
        }

        public string Calculate_CheckSumDatiFissi()
        {
            return OGC_utilita.HashFile(this.Files[Costanti.FIL_PlugInDatiFissi]);
        }

        public abstract void Carica_FullDatasetSpecific();

        public void Carica_FullDataset(int nRecordDaCaricare)
        {
            if (_PlugInSessione.Versione != eVersionePlugIn.Versione_2_0_0)
                _Access2PlugIn.LoadFullDataset(nRecordDaCaricare);
            else if (_PlugInSessione.Versione == eVersionePlugIn.Versione_2_0_0)
                _PlugInSessione.Carica_FullDataset(nRecordDaCaricare, "");
            Carica_FullDatasetSpecific();
        }

        public void Carica_Plugin_NuovaVersione()
        {
            AssemblyLoader PlugInLoader;
            Object PluginAppoggio;

            _logger.Debug("Carica_Plugin_NuovaVersione " + String.Concat("File del plugin: ", this.Files[Costanti.FIL_PlugIn]));
            try
            {
                // ************************************************************************** //
                // Passo al caricameno del plugin della sessione.                             //
                // Se il plug in permette (versione plugin 1.6.0 e superiori allora procedo   //
                // con il caricamento in un Application Domain separato altrimenti in quello  //
                // attuale.                                                                   //
                // Tale attività è gestita caricandolo su di un nuovo Application domain e ve //
                // rificando che non si generi nessun tipo di eccezzione.                     //
                // ************************************************************************** //
                _PlugInAppDomain = AppDomain.CreateDomain("PlugInAppDomain");
                PlugInLoader = new AssemblyLoader();
                PluginAppoggio = PlugInLoader.CreateInstance(this.Files[Costanti.FIL_PlugIn]);

                //
                // Questa assegnazione è fittizia serve solo per poter verificare che sia correttamente caricato in modalità serializad
                //
                // PluginAppoggio = PlugInLoader.PlugIn
                _logger.Debug("Carica_Plugin_NuovaVersione " + "Plug in caricato in un AppDomain diverso.");
            }
            catch (System.Runtime.Serialization.SerializationException ex)
            {
                // 
                // Caricamento del plugin in maniera tradizionale. Stesso Appdomain dell'ap -
                // plicazione.                                                                
                //
                PlugInLoader = new AssemblyLoader();
                PluginAppoggio = PlugInLoader.CreateInstance(this.Files[Costanti.FIL_PlugIn]);
                _logger.Debug("Carica_Plugin_NuovaVersione " + "Plug in caricato nello stesso AppDomain.");
            }
            catch (Exception ex)
            {
                PlugInLoader = null;
                throw new Exception(String.Concat("GESE-000004: Errore nel caricamento del plugin (", this.Files[Costanti.FIL_PlugIn], ")."), null);
            }

            _logger.Debug("Carica_Plugin_NuovaVersione" + String.Concat("Versione plugin caricato:", PlugInLoader.VersionePlugIn));

            //
            // Caricamento della classe di gestione del plugin.
            //
            try
            {
                switch (PlugInLoader.VersionePlugIn)
                {
                    case (int)eVersionePlugIn.No_versione:
                        throw new Exception(String.Concat("GESE-000005: Versione del plugin non identificata."), null);
                        break;
                    case (int)eVersionePlugIn.Versione_1_0_0:
                        _PlugInSessione = new CLS_plugin_v1_0_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_1_0:
                        _PlugInSessione = new CLS_plugin_v1_1_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_2_0:
                        _PlugInSessione = new CLS_plugin_v1_2_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_3_0:
                        _PlugInSessione = new CLS_plugin_v1_3_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_3_1:
                        _PlugInSessione = new CLS_plugin_v1_3_1(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_3_2:
                        _PlugInSessione = new CLS_plugin_v1_3_2(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_4_0:
                        _PlugInSessione = new CLS_plugin_v1_4_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_4_1:
                        _PlugInSessione = new CLS_plugin_v1_4_1(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_4_2:
                        _PlugInSessione = new CLS_plugin_v1_4_2(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_4_3:
                        _PlugInSessione = new CLS_plugin_v1_4_3(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_5_0:
                        _PlugInSessione = new CLS_plugin_v1_5_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_5_1:
                        _PlugInSessione = new CLS_plugin_v1_5_1(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_6_0:
                        _PlugInSessione = new CLS_plugin_v1_6_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_1_6_1:
                        _PlugInSessione = new CLS_plugin_v1_6_1(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                    case (int)eVersionePlugIn.Versione_2_0_0:
                        _PlugInSessione = new CLS_plugin_v2_0_0(PluginAppoggio, this.Folders, this.FoldersGlobali, this.Files, this.OleDb_ConnString);
                        break;
                }
                _PlugInSessione.SezioneDati = _xdSessione.SelectSingleNode("sessione/dati");
                _logger.Debug("Carica_Plugin_NuovaVersione" + String.Concat("Plugin sessione caricato? ", (_PlugInSessione != null).ToString()));
            }
            catch (Exception ex)
            {
                _logger.Debug(String.Concat("GESE-000006: Errore nella creazione della classe del plugin."), ex);
            }
        }

        public void Carica_QuickDataset()
        {
            try
            {
                MessaggiProgress.MessaggiDalPlugin = false;
                MessaggiProgress.ProgressivoPrinc = 0;
                MessaggiProgress.MassimoPrinc = 1;
                MessaggiProgress.TipoOperazionePrinc = "Caricamento dei dati del Quick Database....";
                MessaggiProgress.ProgressivoSecon = 0;
                MessaggiProgress.MassimoSecon = -1;
                MessaggiProgress.TipoOperazioneSecon = "N/D";

                _PlugInSessione.MessaggiProgress = FunzioniUtili.TransformMessaggiOUT(MessaggiProgress);
                MessaggiProgress.MessaggiDalPlugin = true;

                //
                // Step 1 Caricamento dei dati del QuickDataset
                //
                _PlugInSessione.Carica_QuickDataset();


                // ********************************************************************* //
                // Adeguo il QuickDataset a quello che è l'ultima versione del plugin.   //
                // In particolare:                                                       //
                //                                                                       //
                // Vers. PLUGIN      Campo                       Valore default          //
                // 1.3.0             DEF_sendbymail              FALSE                   //
                // 1.4.1             DEF_onlyarchott             FALSE                   //
                // ********************************************************************* //
                if (_PlugInSessione.Versione < eVersionePlugIn.Versione_1_4_1)
                {
                    this.QuickDataTable.Columns.Add(new DataColumn("DEF_onlyarchott", System.Type.GetType("System.Boolean")));
                    if (_PlugInSessione.Versione < eVersionePlugIn.Versione_1_3_0)
                        this.QuickDataTable.Columns.Add(new DataColumn("DEF_sendbymail", System.Type.GetType("System.Boolean")));

                    foreach (DataRow dr in this.QuickDataTable.Rows)
                    {
                        dr["DEF_onlyarchott"] = false;
                        if (_PlugInSessione.Versione < eVersionePlugIn.Versione_1_3_0)
                            dr["DEF_sendbymail"] = false;

                    }
                    this.QuickDataTable.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Apertura_Sessione(String cFolder, Boolean lNuovaSessione)
        {
            try
            {
                _logger.Debug(String.Concat("Apertura. Caricamento della sessione: ", cFolder));
                this.Caricamento_Sessione(cFolder, false, lNuovaSessione);
            }
            catch (Exception ex)
            {
                _logger.Error("Errore", ex);
            }
        }

        public abstract void Caricamento_SessioneSpecific();

        public void Caricamento_Sessione(String cFolder, Boolean lAnteprimaSessione, Boolean lNuovaSessione)
        {
            try
            {
                this.IsLoaded = true;
                _logger.Debug("Caricamento " + String.Concat("Caricamento della sessione: ", cFolder));
                if (System.IO.Directory.Exists(cFolder))
                {
                    this.Nome = FunzioniUtili.EstraiNomeSessione(cFolder);
                    _logger.Debug("Caricamento " + "Nome sessione ricavato: " + this.Nome);
                    this.Folders.Add(Costanti.FLD_SessioneRoot, OGC_utilita.StandardFolderValue(cFolder));
                    this.Files.Add(Costanti.FIL_Sessione, OGC_utilita.ConcatenaFolderFileValue(this.Folders[Costanti.FLD_SessioneRoot], String.Concat(this.Nome, ".xml")));


                    _logger.Debug("Caricamento" + "File sessione: " + this.Files[Costanti.FIL_Sessione]);
                    if (System.IO.File.Exists(this.Files[Costanti.FIL_Sessione]))
                    {
                        //
                        // Popola la lista delle cartelle della sessione.
                        //
                        this.Folders.Add("AllegatiFLD", OGC_utilita.ConcatenaFolderValue(this.Folders[Costanti.FLD_SessioneRoot], "Allegati"));


                        this.Folders.Add("DatiFLD", OGC_utilita.ConcatenaFolderValue(this.Folders[Costanti.FLD_SessioneRoot], "Dati"));
                        this.Folders.Add("EsportaFLD", OGC_utilita.ConcatenaFolderValue(this.Folders[Costanti.FLD_SessioneRoot], "Esportazione"));
                        this.Folders.Add(Costanti.FLD_moduli, OGC_utilita.ConcatenaFolderValue(this.Folders[Costanti.FLD_SessioneRoot], "Moduli"));

                        this.Folders.Add("ReportsFLD", OGC_utilita.ConcatenaFolderValue(this.Folders[Costanti.FLD_SessioneRoot], "Reports"));

                        //
                        // Popola la lista dei file delle sessioni
                        //
                        _xdSessione = new System.Xml.XmlDocument();
                        _xdSessione.Load(this.Files[Costanti.FIL_Sessione]);


                        this.Files.Add("QuickDatasetSerializadFIL", "");
                        this.Files.Add("FullDatasetSerializadFIL", "");
                        this.Files.Add(Costanti.FIL_PlugIn, String.Concat(this.Folders[Costanti.FLD_SessioneRoot], _xdSessione.SelectSingleNode("sessione/plugin/filename").InnerText));
                        this.Files.Add("PlugInTrasformazioniFIL", OGC_utilita.ConcatenaFolderFileValue(this.Folders[Costanti.FLD_SessioneRoot], _xdSessione.SelectSingleNode("sessione/plugin/trasformazioni").InnerText));
                        //this.Files.Add("FastPrintFIL", OGC_utilita.ConcatenaFolderFileValue(this.Folders["DatiFLD"], "FastPrint.xml"));
                        this.Files.Add("PlugInDatiFissiFIL", String.Concat(this.Folders[Costanti.FLD_SessioneRoot], _xdSessione.SelectSingleNode("sessione/dati/DatiFissi").InnerText));
                        if (_xdSessione.SelectSingleNode("sessione/dati/SendByMail") != null)
                            this.Files.Add("DatiSpedizioneEmailFIL", String.Concat(this.Folders[Costanti.FLD_SessioneRoot], _xdSessione.SelectSingleNode("sessione/dati/SendByMail").InnerText));

                       // this.Files.Add("StatisticheStampaFIL", OGC_utilita.ConcatenaFolderFileValue(this.Folders["DatiFLD"], "StatisticheStampa.csv"));


                        if (_xdSessione.SelectSingleNode("sessione/dati/filename[@tipo=\"0\"]") != null)
                        {
                            this.Files.Add("QuickDataSetIndexNSFIL", String.Concat(this.FoldersGlobali["TempFLD"], OGC_utilita.Timestamp(), ".xml"));
                            this.Files.Add("QuickDataSetIndexFIL", String.Concat(this.Folders[Costanti.FLD_SessioneRoot], _xdSessione.SelectSingleNode("sessione/dati/filename[@tipo=\"0\"]").InnerText));
                        }

                        if (!lAnteprimaSessione)
                        {
                            Carica_Plugin_NuovaVersione();

                            Caricamento_SessioneSpecific();
                            //
                            // Caricamento Modulo Versione Vecchia. Sarà progressivamente sostituita da quella nuova Sistemata appena sopra.
                            //
                            //_logger.Debug("GESE-000003: Il plugin non può essere caricato correttamente. Non si può procedere con l'apertura della sessione di stampa.");

                            _Access2PlugIn = new CLS_plugin_accesso((int)TipoApplicazione, _VersioneSistemaOperativo);
                            _Access2PlugIn.SessioneNuova = lNuovaSessione;
                            _Access2PlugIn.Carica_Plugin(_PlugInSessione);
                            _Access2PlugIn.Folders = this.Folders;
                            _Access2PlugIn.Files = this.Files;
                            _Access2PlugIn.SezioneDati = _xdSessione.SelectSingleNode("sessione/dati");
                            //_logger.Debug("GESE-000003: Il plugin non può essere caricato correttamente. Non si può procedere con l'apertura della sessione di stampa.");

                            if (!_Access2PlugIn.IsPlugInLoaded)
                                throw new Exception("GESE-000003: Il plugin non può essere caricato correttamente. Non si può procedere con l'apertura della sessione di stampa.");
                        }
                    }
                    else
                        throw new Exception("GESE-000002: Non è stato possibile caricare il file della sessione. Non si può procedere con l'apertura della sessione di stampa.");
                }
                else
                {
                    throw new Exception(String.Concat("GESE-000001: Il percorso della sessione (", cFolder, ") non esiste."));
                }
            }
            catch (Exception ex)
            {
                if (!lAnteprimaSessione)
                {
                    _logger.Error("Caricamento della sessione", ex);
                    Reset_Sessione();
                }
                //Errore = ex;
                throw ex;
            }

        }

        public void Chiudi_Sessione()
        {
            Reset_Sessione();
        }

        public void Conta_RecordConErrori()
        {
            this.QuickDataset_Record_ErroriCaricamentoGravi = 0;
            this.QuickDataset_Record_ErroriCaricamentoLievi = 0;
            this.QuickDataset_Record_ErroreStampa = 0;
            foreach (DataRow dr in this.QuickDataTable.Rows)
            {
                switch (int.Parse(dr["DEF_errcode"].ToString()))
                {
                    case -1:
                        this.QuickDataset_Record_ErroriCaricamentoGravi += 1;
                        break;
                    case -2:
                        this.QuickDataset_Record_ErroriCaricamentoLievi += 1;
                        break;
                    case -10:
                        this.QuickDataset_Record_ErroreStampa += 1;
                        break;
                }
            }
        }

        public void Conta_RecordSelezionati(DataRow dr)
        {
            if (Boolean.Parse(dr["DEF_toprint"].ToString()))
                this.QuickDataset_Record_Selezionati += 1;
        }

        public void GoToRecordNumber(int nRow  )
        {
            _PlugInSessione.GoToRecordNumber(nRow);
        }

        public void ImpostaDGVDati(DataGridView DGV)
        {
            _PlugInSessione.ImpostaDGVDati(DGV);

        }
        public void Reset_Sessione()
        {
            this.IsLoaded = false;
            this.Folders = new CLS_FileFolderDic();
            this.Files = new CLS_FileFolderDic();
            this._xdSessione = null;
            this.QuickDataset_Record_Selezionati = 0;
            this.MessaggiProgress = new MessaggiEntity();
        }

        public void Salvataggio_Sessione()
        {
            _xdSessione.Save(this.Files[Costanti.FIL_Sessione]);
        }

        public void SetSelectable(DataRow dr)
        {
            _PlugInSessione.SetSelectable(dr, _DatiModuloAttivo.Filtro);
        }
    }

}
