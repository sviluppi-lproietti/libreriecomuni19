﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomainBase.Entity
{
    /// <summary>
    /// Gestisce le proprietà di una sessione recente
    /// </summary>
    public class SessioniRecentiEntity
    {
        public SessioniRecentiEntity()
        {

        }

        public SessioniRecentiEntity(int id, string nome, string percorso)
        {
            SER_codice = id;
            SER_nome = nome;
            SER_percorso = percorso;
        }

        public int SER_codice { get; set; }
        public string SER_nome { get; set; }
        public string SER_percorso { get; set; }
    }
}
