﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomainBase.Entity
{
    public class UtentiEntity
    {
        public UtentiEntity()
        {
            UTE_codice = 0;
            tiebreak = 0;
            UTE_cognome = "";
            UTE_nome = "";
            UTE_password = "";
            isAdmin = false;
            isEnabled = true;
            isResetPwd = true;
        }

        public int UTE_codice { get; set; }
        public string UTE_cognome { get; set; }
        public string UTE_nome { get; set; }
        public string UTE_username { get; set; }
        public string UTE_password { get; set; }
        public int UTE_admin { get; set; }
        public int UTE_enabled { get; set; }
        public int UTE_resetpwd { get; set; }
        public int UTE_canprint { get; set; }
        public int UTE_cansend { get; set; }
        public int tiebreak { get; set; }

        public Boolean CanPrint
        {
            get
            {
                return UTE_canprint == 1;
            }
            set { UTE_canprint = value ? 1 : 0; }
        }

        public Boolean CanSend
        {
            get
            {
                return UTE_cansend == 1;
            }
            set { UTE_cansend = value ? 1 : 0; }
        }

        public Boolean isAdmin
        {
            get
            {
                return UTE_admin == 1;
            }
            set
            {
                UTE_admin = value ? 1 : 0;
            }
        }
        public Boolean isNewUtente
        {
            get
            {
                return UTE_codice == 0;
            }
        }
        public Boolean isEnabled
        {
            get
            {
                return UTE_enabled == 1;
            }
            set
            {
                UTE_enabled = value ? 1 : 0;
            }
        }
        public Boolean isResetPwd
        {
            get
            {
                return UTE_resetpwd == 1;
            }
            set { UTE_resetpwd = value ? 1 : 0; }
        }

        // ***************************************************************** //
        // Metodi Pubblici                                                   //
        // ***************************************************************** //
        public UtentiEntity Clone()
        {
            return (UtentiEntity)this.MemberwiseClone();
        }

    }
}
